﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.App_Code.DTO.ShipexAPI
{
    public class CalculadoraDTO
    {
        public float alto { get; set; }
        public float ancho { get; set; }
        public float largo { get; set; }
        public float peso { get; set; }
        public int idComuna { get; set; }
        public int idCliente { get; set; }
    }
}