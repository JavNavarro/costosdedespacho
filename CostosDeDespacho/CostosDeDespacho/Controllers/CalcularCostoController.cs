﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http.Results;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using CostosDeDespacho.App_Code.BE;

namespace CostosDeDespacho.Controllers
{
    public class CalcularCostoController : ApiController
    {
        BDconn conBD = new BDconn();
        
        
        [System.Web.Mvc.HttpPost]
        public async Task<IHttpActionResult> Post()
        {
            var requestContent = Request.Content;
            var jsonContent = await requestContent.ReadAsStringAsync();
            App_Code.BL.CostoLogic logica = new App_Code.BL.CostoLogic();
            Costo cost = new Costo();            
            cost = logica.costoDirecto(jsonContent);
            List<Retorno> retorns = new List<Retorno>();
            Retorno ret = new Retorno(); 
            ret.currency = "CLP";
            ret.service_code = "NRML";
            ret.service_name = "Shipex entrega";
            ret.total_price = cost.precio;
            // DateTimeOffset dateOff = DateTimeOffset.Parse(DateTime.Now.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss"));
            ret.min_delivery_date = (DateTimeOffset.Parse(DateTime.Now.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss"))).ToOffset(new TimeSpan(-4, 0, 0)).ToString();//.ToString("yyyy -MM-dd HH:mm:ss GMT"));
            //ret.min_delivery_date = (DateTimeOffset.Now.AddDays(1)).ToOffset(new TimeSpan(-4, 0, 0)).ToString();//.ToString("yyyy -MM-dd HH:mm:ss GMT"));
            ret.max_delivery_date = (DateTimeOffset.Parse(DateTime.Now.AddDays(3).ToString("yyyy-MM-dd HH:mm:ss"))).ToOffset(new TimeSpan(-4, 0, 0)).ToString();//.ToString("yyyy-MM-dd HH:mm:ss GMT"));
            if (ret.total_price > 0)
            {
                retorns.Add(ret);
            }

            return Get(retorns);
        }
            

        
        [System.Web.Mvc.HttpGet]
        //GET api/values
        public IHttpActionResult Get(List<Retorno> retorns)
        {

            try
            {
                var rates = new { rates = retorns };//, status =  System.Net.HttpStatusCode.OK };
                return Ok(rates);
               
            }
            catch(Exception ex)
            {
                var rates = new { rates = ex.ToString(), status = System.Net.HttpStatusCode.InternalServerError };
                return NotFound();
            }
            
            //var json = JsonConvert.SerializeObject(cost);
            //return json;
          

        }

        private JsonResult Json(object res, JsonRequestBehavior allowGet)
        {
            throw new NotImplementedException();
        }
    }
}
