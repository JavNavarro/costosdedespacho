﻿using CostosDeDespacho.App_Code.CORS;
using CostosDeDespacho.App_Code.DTO.ShipexAPI;
using CostosDeDespacho.App_Code.Utils;
using CostosDeDespacho.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CostosDeDespacho.Controllers
{
    public class ShipexController : Controller
    {
        // GET: Shipex
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AllowCrossSite]
        public JsonResult calcularPrecioEnvio(CalculadoraDTO objCalculadora)
        {
            db_shipexEntities objConexion = new db_shipexEntities();
            try
            {
                string procAlmacenado = "exec obtener_precio_despacho @alto, @ancho, @largo, @peso, @idComuna, @idCliente";
                procAlmacenado = procAlmacenado.Replace("@alto", objCalculadora.alto.ToString());
                procAlmacenado = procAlmacenado.Replace("@ancho", objCalculadora.ancho.ToString());
                procAlmacenado = procAlmacenado.Replace("@largo", objCalculadora.largo.ToString());
                procAlmacenado = procAlmacenado.Replace("@peso", objCalculadora.peso.ToString());
                procAlmacenado = procAlmacenado.Replace("@idComuna", objCalculadora.idComuna.ToString());
                procAlmacenado = procAlmacenado.Replace("@idCliente", objCalculadora.idCliente.ToString());

                int precio = objConexion.Database.SqlQuery<int>(procAlmacenado).FirstOrDefault();
                var resultado = new { precio_despacho = precio, status = System.Net.HttpStatusCode.OK };
                return Json(resultado, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Log.Fatal(e.ToString());
                var resultado = new { precio_despacho = 0, status = System.Net.HttpStatusCode.InternalServerError };
                return Json(resultado, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [AllowCrossSiteAttribute]
        public JsonResult obtenerRegionesEnvio()
        {
            try
            {
                db_shipexEntities objConexion = new db_shipexEntities();
                List<region> lRegion = objConexion.region.ToList();
                List<RegionViewModel> lRegionesVM = new List<RegionViewModel>();
                foreach (region reg in lRegion)
                {
                    RegionViewModel r = new RegionViewModel();
                    r.region_id = reg.region_id;
                    r.region_nombre = reg.region_nombre;
                    lRegionesVM.Add(r);
                }
                var resultado = new { regiones = lRegionesVM, status = System.Net.HttpStatusCode.OK };
                return Json(resultado, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Log.Fatal(e.ToString());
                var resultado = new { precio_despacho = 0, status = System.Net.HttpStatusCode.InternalServerError };
                return Json(resultado, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [AllowCrossSiteAttribute]
        public JsonResult obtenerComunasEnvio(int idRegion)
        {
            try
            {
                db_shipexEntities objConexion = new db_shipexEntities();
                List<provincia> lProvincia = objConexion.provincia.Where(x => x.region_id == idRegion).ToList();
                List<comuna> lCom = new List<comuna>();
                foreach (provincia p in lProvincia)
                {
                    lCom.AddRange(objConexion.comuna.Where(x => x.provincia_id == p.provincia_id).ToList());
                }
                var resultado = new { comunas = lCom, status = System.Net.HttpStatusCode.OK };
                return Json(resultado, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Log.Fatal(e.ToString());
                var resultado = new { precio_despacho = 0, status = System.Net.HttpStatusCode.InternalServerError };
                return Json(resultado, JsonRequestBehavior.AllowGet);
            }
        }
    }
}