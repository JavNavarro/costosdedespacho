﻿using CostosDeDespacho.App_Code.BE;
using CostosDeDespacho.App_Code.Utils;
using CostosDeDespacho.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace CostosDeDespacho
{
    public class BDconn
    {
        db_shipexEntities context = new db_shipexEntities();
        public Costo calcularEnvio(string comuna, string cliente, List<Productos> productos)
        {
            Costo newCosto;
            newCosto = new Costo();
            decimal pesoEfectivo = 0;
            int precio = 0;
            int horasEstimadas = 0;
            try
            {
                foreach (var prod in productos)
                {
                    pesoEfectivo = pesoEfectivo + obtenerPrecio(prod.sku, prod.cantidad, cliente);
                }

                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONEXION"].ToString()))
                {
                    string nombreSP;
                    if (pesoEfectivo >= 100)
                    {
                        nombreSP = "sp_calcularPrecioSobre20k";
                    }
                    else
                    {
                        nombreSP = "calcularPrecioDespacho";
                    }
                    SqlCommand cmd2 = new SqlCommand(nombreSP, cn);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    cmd2.Parameters.AddWithValue("comuna", comuna);
                    cmd2.Parameters.AddWithValue("cliente", cliente);
                    cmd2.Parameters.AddWithValue("peso", pesoEfectivo);
                    SqlDataReader resultado = cmd2.ExecuteReader();
                    resultado.Read();
                    precio = Int32.Parse(resultado["precio"].ToString());
                    horasEstimadas = Int32.Parse(resultado["estimadoHoras"].ToString());
                    cn.Close();
                }
            }
            catch (Exception exx)
            {
                string err = exx.ToString();
            }
            newCosto.precio = precio;
            newCosto.estimadoHoras = horasEstimadas;
            return newCosto;
        }

        public int GetIdCliente(string nombre_cliente)
        {
            Int32 id_cliente = 0;
            try
            {

                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONEXION"].ToString()))
                {
                    SqlCommand cmd2 = new SqlCommand("obtenerIdClienteCotizacionEnvio", cn);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    cmd2.Parameters.AddWithValue("nombre_cliente", nombre_cliente);
                    SqlDataReader resultado2 = cmd2.ExecuteReader();
                    if (resultado2.HasRows)
                    {
                        resultado2.Read();
                        id_cliente = resultado2.GetInt32(0);
                    }
                    cn.Close();
                }
            }
            catch (Exception exx)
            {
                string err = exx.ToString();
                return id_cliente;
            }
            return id_cliente;
        }

        public bool getObtenerClienteCustomer(string email, string nombre_tienda)
        {
            string result = String.Empty;
            
            try
            {
                configuracion_cliente_customer _Cliente_Customer = context.configuracion_cliente_customer.FirstOrDefault(x => x.email == email);

                var lResultados = (from tc in context.tienda_cliente
                                   join sp in context.configuracion_cliente_customer on tc.idCliente equals sp.id_cliente
                                   where tc.nom_Tienda == nombre_tienda && sp.email == email
                                   select new
                                   {

                                   }).ToList();

                if (lResultados.Count == 0)
                {

                    string url = System.Configuration.ConfigurationManager.AppSettings["api.customer.cliente.cotizacion"].ToString();
                    url = url.Replace("{email}", email);

                    using (WebClient client = new WebClient())
                    {
                        System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                        client.Headers[HttpRequestHeader.ContentType] = "application/json";
                        result = client.DownloadString(url);
                    }

                    JObject jsonResponse = JObject.Parse(result);

                    string existe_customer = jsonResponse["role"].ToString() == null || jsonResponse["role"].ToString() == "" ? "" : jsonResponse["role"].ToString();

                    if (result == "")
                    {
                        return false;
                    }
                    else if (existe_customer == "customer")
                    {
                        var cliente = context.tienda_cliente.FirstOrDefault(x => x.nom_Tienda == nombre_tienda);

                        configuracion_cliente_customer customer = new configuracion_cliente_customer();

                        customer.email = email;
                        customer.id_cliente = cliente.idCliente;
                        context.configuracion_cliente_customer.Add(customer);
                        context.SaveChanges();

                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }
            catch (WebException e)
            {
                string response = String.Empty;
                using (StreamReader r = new StreamReader(e.Response.GetResponseStream()))
                {
                    response = r.ReadToEnd();
                }
                JObject jsonResponse = JObject.Parse(response);
                Log.Error("BDconn.getObtenerClienteCustomer: " + response.ToString());
                return false;
            }
            catch (Exception ex)
            {
                string res = ex.ToString();
                return false;
            }
        }

        public List<EstadosDespacho> traerEstadosDetracking(string orderId, string cliente)
        {
            List<EstadosDespacho> estados = new List<EstadosDespacho>();

          
            try
            {
              
                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONEXION"].ToString()))
                {
                    SqlCommand cmd2 = new SqlCommand("obtenerTrackingPorOrderID", cn);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    cmd2.Parameters.AddWithValue("orderID", orderId);
                    cmd2.Parameters.AddWithValue("cliente", cliente);
                    SqlDataReader resultado = cmd2.ExecuteReader();
                    while (resultado.Read()){
                        EstadosDespacho estado = new EstadosDespacho();
                        estado.estado = resultado["estado"].ToString();
                        estado.fecha_estado = resultado["FechaEstado"].ToString();
                        estados.Add(estado);
                    }

                    cn.Close();
                }
            }
            catch (Exception exx)
            {
                string err = exx.ToString();
            }
      
            return estados;
        }

        public DataSet GetDatosRespuesta(string cliente, string comuna)
        {
            DataSet datos = new DataSet();

            try
            {
                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONEXION"].ToString()))
                {
                    SqlCommand cmd2 = new SqlCommand("sp_shipex_envio", cn);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    cmd2.Parameters.AddWithValue("cliente", cliente);
                    cmd2.Parameters.AddWithValue("comuna", comuna);
                    SqlDataAdapter result = new SqlDataAdapter(cmd2);

                    result.Fill(datos);
                    cn.Close();
                }
            }
            catch (Exception exx)
            {
                string err = exx.ToString();
            }
            return datos;
        }

        public int buscarComuna(string comuna)
        {
            Int32 id_comuna = 0;
            try
            {

                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONEXION"].ToString()))
                {
                    SqlCommand cmd2 = new SqlCommand("obtenerIdComunaCotizador", cn);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    cmd2.Parameters.AddWithValue("comuna", comuna);
                    SqlDataReader resultado2 = cmd2.ExecuteReader();
                    resultado2.Read();
                    id_comuna = Convert.ToInt32(resultado2["id_comuna"]);
                    cn.Close();
                }
            }
            catch (Exception exx)
            {
                string err = exx.ToString();
                return id_comuna;
            }
            return id_comuna;
        }

        public string GetPrefijoCliente(int cod_cliente)
        {
            Int32 despachoEncontrado = 0;
            try
            {

                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONEXION"].ToString()))
                {
                    SqlCommand cmd2 = new SqlCommand("Shipex_ObtenerPrefijo", cn);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    cmd2.Parameters.AddWithValue("id", cod_cliente);
                    SqlDataReader resultado2 = cmd2.ExecuteReader();
                    if (resultado2.HasRows)
                    {
                        resultado2.Read();
                        despachoEncontrado = resultado2.GetInt32(0);
                    }
                    cn.Close();
                }
            }
            catch (Exception exx)
            {
                string err = exx.ToString();
            }
            return despachoEncontrado.ToString();
        }

        public string Get_Correlativo(int cod_cliente)
        {
            Int32 correlativo = 0;
            try
            {

                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONEXION"].ToString()))
                {
                    SqlCommand cmd2 = new SqlCommand("Shipex_UsarCorrelativousuario", cn);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    cmd2.Parameters.AddWithValue("idUser", cod_cliente);
                    SqlDataReader resultado2 = cmd2.ExecuteReader();
                    resultado2.Read();
                    correlativo = resultado2.GetInt32(0);
                    cn.Close();
                }
            }
            catch (Exception exx)
            {
                string err = exx.ToString();
            }
            return correlativo.ToString();
        }

        public ProductoPromocionResponse ObtieneDespachoGratisPromocionCampana(ProductosPromocion productosPromocions)
        {
            Boolean respuesta = false;
            ProductoPromocionResponse objEtiquetaResponseDTO = new ProductoPromocionResponse();
            try
            {
                string url = ConfigurationManager.AppSettings["api.farmex.valida.campania"].ToString();

                using (WebClient client = new WebClient())
                {
                    System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    client.Headers[HttpRequestHeader.ContentType] = "application/json";
                    string jsonstr = JsonConvert.SerializeObject(productosPromocions);
                    string jsonResult = client.UploadString(new Uri(url), "POST", jsonstr);
                    Log.Debug("Resultado llamada Campaña " + jsonResult);
                    ProductoPromocionResponse _listPromocionResponseDTO = JsonConvert.DeserializeObject<ProductoPromocionResponse>(jsonResult);
                    objEtiquetaResponseDTO = _listPromocionResponseDTO;
                    return objEtiquetaResponseDTO;
                }
            }
            catch (Exception e)
            {
                string err = e.ToString();
                objEtiquetaResponseDTO.exist_in_campaign = false;
                return objEtiquetaResponseDTO;
            }
        }

        public void skuNoEncontrado(string sku, string cliente)
        {
            try
            {

                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONEXION"].ToString()))
                {
                    SqlCommand cmd = new SqlCommand("insertarSkuNoEncontrado", cn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    if (sku == null)
                    {
                        sku = "";
                    }
                    cmd.Parameters.AddWithValue("@sku", sku);
                    cmd.Parameters.AddWithValue("@cliente", cliente);

                    cmd.ExecuteNonQuery();
                    cn.Close();

                }

            }
            catch (Exception ex)
            {
                String err;
                err = ex.ToString();
            }
        }

        public bool getCumplePesoClienteComuna(string cliente, string comuna, decimal peso_producto)
        {
            string result = String.Empty;
            Int32 cantidad;

            try
            {
                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONEXION"].ToString()))
                {
                    SqlCommand cmd2 = new SqlCommand("Shipex_CumplePesoClienteComuna", cn);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    cmd2.Parameters.AddWithValue("cliente", cliente);
                    cmd2.Parameters.AddWithValue("comuna", comuna);
                    cmd2.Parameters.AddWithValue("peso", peso_producto);
                    SqlDataReader resultado2 = cmd2.ExecuteReader();
                    resultado2.Read();
                    cantidad = Convert.ToInt32(resultado2["resultado"]);
                    cn.Close();

                    if (cantidad > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                string res = ex.ToString();
                return false;
            }
        }

        public int Get_codCliente(string cliente)
        {
            Int32 codCliente = 0;
            try
            {

                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONEXION"].ToString()))
                {
                    SqlCommand cmd2 = new SqlCommand("Shipex_ObtenerCodCiente", cn);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    cmd2.Parameters.AddWithValue("cliente", cliente);
                    SqlDataReader resultado2 = cmd2.ExecuteReader();
                    resultado2.Read();
                    codCliente = Convert.ToInt32(resultado2["idCliente"]);
                    cn.Close();
                }
            }
            catch (Exception exx)
            {
                string err = exx.ToString();
            }
            return codCliente;
        }

        public void insertarOrdenWarehouse(int cod_cliente, long awb, long codDespacho, string sku, int cantidad, string NomCli1, string NomCli2, string ApatCli, string AmatCli, string comuna)
        {
            try
            {

                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONEXION"].ToString()))
                {
                    SqlCommand cmd = new SqlCommand("insertarOrdenTrabajo", cn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    cmd.Parameters.AddWithValue("@id_cliente", cod_cliente);
                    if (sku == null)
                    {
                        sku = "";
                    }
                    cmd.Parameters.AddWithValue("@sku", sku);
                    cmd.Parameters.AddWithValue("@cantidad", cantidad);
                    cmd.Parameters.AddWithValue("@NomCli1", NomCli1);
                    cmd.Parameters.AddWithValue("@NomCli2", NomCli2);
                    cmd.Parameters.AddWithValue("@ApatCli", ApatCli);
                    cmd.Parameters.AddWithValue("@AmatCli", AmatCli);
                    cmd.Parameters.AddWithValue("@comuna", comuna);
                    cmd.Parameters.AddWithValue("@Tracking", codDespacho);
                    cmd.Parameters.AddWithValue("@awb", awb);

                    cmd.ExecuteNonQuery();
                    cn.Close();

                }

            }
            catch (Exception ex)
            {
                String err;
                err = ex.ToString();
            }
        }

        public string GetCodComuna(string comuna)
        {
            Int32 codComuna = 0;
            try
            {

                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONEXION"].ToString()))
                {
                    SqlCommand cmd2 = new SqlCommand("Shipex_ObtenerCodDomuna", cn);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    cmd2.Parameters.AddWithValue("comuna", comuna);
                    SqlDataReader resultado2 = cmd2.ExecuteReader();
                    while (resultado2.Read())
                    {
                        codComuna = resultado2.GetInt32(0);
                    }
                    cn.Close();
                }
            }
            catch (Exception exx)
            {
                string err = exx.ToString();
            }
            return codComuna.ToString();
        }

        public DatosDespacho getDespacho(string orderId, string cliente)
        {
            DatosDespacho despacho;
            despacho = new DatosDespacho();
          
            try
            {               
                    using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONEXION"].ToString()))
                {
                    SqlCommand cmd2 = new SqlCommand("ObtenerDatosDespacho", cn);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    cmd2.Parameters.AddWithValue("orderId", orderId);
                    cmd2.Parameters.AddWithValue("cliente", cliente);
                    SqlDataReader resultado = cmd2.ExecuteReader();
                    resultado.Read();
                    despacho.codigoDespacho= Int64.Parse(resultado["codDespacho"].ToString());
                    despacho.nombreDestinatario = resultado["nombreCliente"].ToString();
                    despacho.direccion = resultado["direccion"].ToString();
                    despacho.comuna = resultado["comuna"].ToString();
                    despacho.pesoEfectivo = Decimal.Parse(resultado["pesoEfectivo"].ToString());
                    despacho.cliente = resultado["cliente"].ToString();
                    despacho.phone = resultado["phone"].ToString();
                    despacho.orderId = resultado["orderId"].ToString();
                    cn.Close();
                }                
            }            
            catch (Exception exx)
            {
              
            }
         
            return despacho;
        }
      



        public Decimal obtenerPrecio(string sku, Int32 cantidad, string cliente)
        {           
            Decimal peso = 0;
            try
            {                
                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONEXION"].ToString()))
                {
                    SqlCommand cmd2 = new SqlCommand("Peso_por_sku", cn);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    cmd2.Parameters.AddWithValue("sku", sku);
                    //cmd2.Parameters.AddWithValue("cantidad", cantidad);
                    cmd2.Parameters.AddWithValue("cliente", cliente);
                    SqlDataReader resultado = cmd2.ExecuteReader();
                    resultado.Read();
                    peso = Decimal.Parse(resultado["peso"].ToString());
                    cn.Close();
                }
            }
            catch (Exception exx)
            {
                string err = exx.ToString();
            }
            return peso*cantidad;
        }
        public Int64 obtenerCodDespacho( string cliente, string order_id)
        {           
            Int64 codDesp = 0;
            try
            {                
                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONEXION"].ToString()))
                {
                    SqlCommand cmd2 = new SqlCommand("ObtenerCodDespachoPorOrder", cn);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                 
                    cmd2.Parameters.AddWithValue("cliente", cliente);
                    cmd2.Parameters.AddWithValue("order_id", order_id);
                    SqlDataReader resultado = cmd2.ExecuteReader();
                    resultado.Read();
                    codDesp = Int64.Parse(resultado["codDesp"].ToString());
                    cn.Close();
                }
            } 
            catch (Exception exx)
            {
                string err = exx.ToString();
            }
            return codDesp;
        }
         public Int32 validarToken(string token,  string cliente)
        {           
            Int32 cantidad = 0;
            try
            {                
                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONEXION"].ToString()))
                {
                    SqlCommand cmd2 = new SqlCommand("validarTokenSegunCliente", cn);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    cmd2.Parameters.AddWithValue("cliente", cliente);
                    cmd2.Parameters.AddWithValue("token", token);
                    SqlDataReader resultado = cmd2.ExecuteReader();
                    resultado.Read();
                    cantidad = Int32.Parse(resultado["TokenValidos"].ToString());

                    cn.Close();

                }
            }
            catch (Exception exx)
            {
                string err = exx.ToString();
            }
            return cantidad;
        }
          public Int32 validarTokenPublico(string token)
        {           
            Int32 cantidad = 0;
            try
            {                
                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONEXION"].ToString()))
                {
                    SqlCommand cmd2 = new SqlCommand("validarToken_Publicos", cn);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cn.Open();                   
                    cmd2.Parameters.AddWithValue("token", token);
                    SqlDataReader resultado = cmd2.ExecuteReader();
                    resultado.Read();
                    cantidad = Int32.Parse(resultado["TokenValidos"].ToString());

                    cn.Close();
                }
            }
            catch (Exception exx)
            {
                string err = exx.ToString();
            }
            return cantidad;
        }


        public Int32 validarEstadoToken(string token, string cliente)
        {
            Int32 estado = 0;
            try
            {
                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONEXION"].ToString()))
                {
                    SqlCommand cmd2 = new SqlCommand("ApiKeyInfo", cn);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    cmd2.Parameters.AddWithValue("cliente", cliente);
                    cmd2.Parameters.AddWithValue("token", token);
                    SqlDataReader resultado = cmd2.ExecuteReader();
                  
                    if (resultado.Read())
                    {
                        estado = Int32.Parse(resultado["activo"].ToString());
                    }
                    else
                    {
                        estado = 2;
                    }
                   

                    cn.Close();
                }
            }
            catch (Exception exx)
            {
                string err = exx.ToString();
            }
            return estado;
        }

         public Int32 calcularNoToken(string token, string comuna, List<Productos> products,string cliente)
        {
            Int32 precio = 100000;
            decimal pesoVuelo;
            decimal peso = 0;
            foreach (var prod in products)
            {
                pesoVuelo = obtenerPrecio(prod.sku, prod.cantidad, cliente);
                if (pesoVuelo > 0)
                {
                    peso = peso + pesoVuelo;
                }
                else
                {
                    peso = peso + 1;
                }
               
            }
            try
            {
                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONEXION"].ToString()))
                {
                    SqlCommand cmd2 = new SqlCommand("TarifaNoToken", cn);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    cmd2.Parameters.AddWithValue("comuna", comuna);
                    cmd2.Parameters.AddWithValue("token", token);
                    cmd2.Parameters.AddWithValue("peso", peso);
                    SqlDataReader resultado = cmd2.ExecuteReader();
                  
                    resultado.Read();
                    {
                        precio = Int32.Parse(resultado["costo"].ToString());
                    }
                   

                    cn.Close();
                }
            }
            catch (Exception exx)
            {
                return 0;
            }
            return precio;
        }


        public Boolean validarSku(string sku,  string cliente)
        {           
            Int32 cantidad = 0;
            Boolean valido = false;
            try
            {                
                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONEXION"].ToString()))
                {
                    SqlCommand cmd2 = new SqlCommand("validarSKUSegunCliente", cn);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    cmd2.Parameters.AddWithValue("cliente", cliente);
                    cmd2.Parameters.AddWithValue("sku", sku);
                    SqlDataReader resultado = cmd2.ExecuteReader();
                    resultado.Read();
                    cantidad = Int32.Parse(resultado["skuValidos"].ToString());
                    if (cantidad == 1)
                    {
                        valido = true;
                    }
                    cn.Close();
                }
            }
            catch (Exception exx)
            {
                string err = exx.ToString();
            }
            return valido;
        }
         public Boolean guardarLog(string palabra,string momento, string cliente, string accion)
        {           
            Int32 cantidad = 0;
            Boolean valido = false;
            try
            {                
                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONEXION"].ToString()))
                {
                    SqlCommand cmd2 = new SqlCommand("guardarLog_API", cn);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    cmd2.Parameters.AddWithValue("str", palabra);
                    cmd2.Parameters.AddWithValue("cuando", momento);
                    cmd2.Parameters.AddWithValue("cliente", cliente);
                    cmd2.Parameters.AddWithValue("accion", accion);
                    cantidad = cmd2.ExecuteNonQuery();

                    if (cantidad == 1)
                    {
                        valido = true;
                    }
                    cn.Close();
                }
            }
            catch (Exception exx)
            {
                string err = exx.ToString();
            }
            return valido;
        }


        public Boolean EliminarCarga(string order_id, string cliente)
        {
            Int32 cantidad = 0;
            Boolean valido = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONEXION"].ToString()))
                {
                    SqlCommand cmd2 = new SqlCommand("eliminarCarga", cn);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    cmd2.Parameters.AddWithValue("cliente", cliente);
                    cmd2.Parameters.AddWithValue("order_id", order_id);
                    cantidad = cmd2.ExecuteNonQuery();
                   
                    if (cantidad > 0)
                    {
                        valido = true;
                    }
                    cn.Close();
                }
            }
            catch (Exception exx)
            {
                string err = exx.ToString();
            }
            return valido;
        }
         public Boolean editarDespacho(string nombreDestinatario,string  cliente,string  phone,string  email,string  order_id,string  direccion, Int64 codDespacho, Int32 status)
        {
            String nomcli1, nomcli2, apatcli, amatcli;
            nomcli1 = " ";
            nomcli2 = " ";
            apatcli = " ";
            amatcli = " ";
            Int32 cantidad = 0;
            Boolean valido = false;
            String[] nombreCompleto = nombreDestinatario.Split(' '); 
            if (nombreCompleto.Length > 3)
            {
               nomcli1= nombreCompleto[0].ToString();
               nomcli2= nombreCompleto[1].ToString();
               apatcli= nombreCompleto[2].ToString();
               amatcli= nombreCompleto[3].ToString();
            }else if (nombreCompleto.Length== 3)
            {
                nomcli1 = nombreCompleto[0].ToString();
                nomcli2 = nombreCompleto[1].ToString();
                apatcli = nombreCompleto[2].ToString();
              
            }else if (nombreCompleto.Length == 2)
            {
                nomcli1 = nombreCompleto[0].ToString();
                nomcli2 = nombreCompleto[1].ToString();
             
            }
            else if (nombreCompleto.Length == 1)
            {
                nomcli1 = nombreCompleto[0].ToString();
             
            }
                try
            {
                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONEXION"].ToString()))
                {
                    SqlCommand cmd2 = new SqlCommand("editarCarga", cn);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    cmd2.Parameters.AddWithValue("cliente", cliente);
                    cmd2.Parameters.AddWithValue("order_id", order_id);
                    cmd2.Parameters.AddWithValue("nomcli1", nomcli1);
                    cmd2.Parameters.AddWithValue("nomcli2", nomcli2);
                    cmd2.Parameters.AddWithValue("apatcli", apatcli);
                    cmd2.Parameters.AddWithValue("amatcli", amatcli);
                    cmd2.Parameters.AddWithValue("phone", phone);
                    cmd2.Parameters.AddWithValue("email", email);
                    cmd2.Parameters.AddWithValue("direccion", direccion);
                    cmd2.Parameters.AddWithValue("codDespacho", codDespacho);
                    cmd2.Parameters.AddWithValue("nombreDestinatario", nombreDestinatario);
                    cmd2.Parameters.AddWithValue("status", status);
                    cantidad = cmd2.ExecuteNonQuery();
                   
                    if (cantidad > 0)
                    {
                        valido = true;
                    }
                    cn.Close();
                }
            }
            catch (Exception exx)
            {
                string err = exx.ToString();
            }
            return valido;
        }


        public Boolean validarComuna(string comuna)
        {           
            Int32 cantidad = 0;
            Boolean valido = false;
            try
            {                
                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONEXION"].ToString()))
                {
                    SqlCommand cmd2 = new SqlCommand("validarComuna", cn);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    cmd2.Parameters.AddWithValue("comuna", comuna);
                    SqlDataReader resultado = cmd2.ExecuteReader();
                    resultado.Read();
                    cantidad = Int32.Parse(resultado["validas"].ToString());
                    if (cantidad > 0)
                    {
                        valido = true;
                    }
                    cn.Close();
                }
            }
            catch (Exception exx)
            {
                string err = exx.ToString();
            }
            return valido;
        }

        public string registrarDespacho(string comuna, string direccion, string NomCli1, string NomCli2, string ApatCli, string AmatCli, List<Productos> products, string cliente, string phone, string plataforma, string email, string order_id, Int32 status, string lreferencias, string comentario_envio)
        {
            string CodDespacho = "";
            Decimal peso = 0;
            //   long codDespacho = 0;

            if (phone == "")
            {
                phone = "null";
            }
            try
            {
                foreach (var prod in products)
                {
                    peso = peso + obtenerPrecio(prod.sku, prod.cantidad, cliente);
                }

                int cantPro = products.Count;

                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONEXION"].ToString()))
                {
                    SqlCommand cmd2 = new SqlCommand("Registrar_pedidoDespacho", cn);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    cmd2.Parameters.AddWithValue("comuna", comuna);
                    cmd2.Parameters.AddWithValue("direccion", direccion);
                    cmd2.Parameters.AddWithValue("peso", peso);
                    cmd2.Parameters.AddWithValue("Referencia", lreferencias);
                    cmd2.Parameters.AddWithValue("NomCli1", NomCli1);
                    cmd2.Parameters.AddWithValue("NomCli2", NomCli2);
                    cmd2.Parameters.AddWithValue("ApatCli", ApatCli);
                    cmd2.Parameters.AddWithValue("AmatCli", AmatCli);
                    cmd2.Parameters.AddWithValue("plataforma", plataforma);
                    cmd2.Parameters.AddWithValue("cliente", cliente);
                    cmd2.Parameters.AddWithValue("phone", phone);
                    cmd2.Parameters.AddWithValue("email", email);
                    cmd2.Parameters.AddWithValue("refCliente", order_id);
                    cmd2.Parameters.AddWithValue("statusId", status);
                    //cmd2.Parameters.AddWithValue("sku", products);
                    cmd2.Parameters.AddWithValue("cantidad", cantPro);
                    cmd2.Parameters.AddWithValue("comentario_envio", comentario_envio);
                    SqlDataReader resultado2 = cmd2.ExecuteReader();
                    resultado2.Read();
                    CodDespacho = (resultado2["CODDESPACHO"].ToString());
                    cn.Close();
                }
            }
            catch (Exception exx)
            {
                string err = exx.ToString();
            }
            return CodDespacho;
            //  return codDespacho;
        }

        public string ObtenerCodDespachoPorOrder(string cliente, string order_id)
        {
            string codDespacho = "";
            try
            {
                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONEXION"].ToString()))
                {
                    SqlCommand cmd2 = new SqlCommand("ObtenerCodDespachoPorOrder", cn);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cn.Open();

                    cmd2.Parameters.AddWithValue("cliente", cliente);
                    cmd2.Parameters.AddWithValue("order_id", order_id);
                    SqlDataReader resultado = cmd2.ExecuteReader();
                    resultado.Read();
                    codDespacho = resultado["codDesp"].ToString();
                    cn.Close();
                }
            }
            catch (Exception ex)
            {
                string err = ex.ToString();
                return codDespacho = "";
            }
            return codDespacho;
        }

        public void AlimentarDetalles(Int64 codDespacho, string rutCli, string order_id, string itemReferenciasGuia, string TipoServicio, DateTime fecha, int batch, int estado, int cliente, Int64 codDespacho2, string url)
        {
            try
            {
                float peso = 1;
                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONEXION"].ToString()))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("Shipex_AgregarDespacho3", cn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@codigodespacho", codDespacho);
                    cmd.Parameters.AddWithValue("@RutCli", rutCli);
                    cmd.Parameters.AddWithValue("@ReferenciaCliente", order_id);
                    cmd.Parameters.AddWithValue("@Referencia", itemReferenciasGuia.Trim());
                    cmd.Parameters.AddWithValue("@TipoServicio", TipoServicio);
                    cmd.Parameters.AddWithValue("@peso", peso);
                    cmd.Parameters.AddWithValue("@fecha", fecha);
                    cmd.Parameters.AddWithValue("@Batch", batch);
                    cmd.Parameters.AddWithValue("@Estado", estado);
                    cmd.Parameters.AddWithValue("@Cliente", cliente);
                    cmd.Parameters.AddWithValue("@codDespacho2", codDespacho2);
                    cmd.Parameters.AddWithValue("@url", url);
                    cmd.ExecuteNonQuery();
                    cn.Close();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void insertarRegistro2(Int64 awb, Int64 codDespacho, string url_etiqueta, string sku, string cliente, string order_id)
        {
            DataSet dt = new DataSet();

            try
            {
                dt = obtenerDimensiones(sku, cliente);

                decimal peso;
                int alto;
                int largo;
                int ancho;

                if (dt.Tables[0].Rows.Count > 0)
                {
                    string strPeso = dt.Tables[0].Rows[0]["peso"].ToString().Replace(".",",");
                    CultureInfo cult = new CultureInfo("es-CL");
                    decimal dValue = Convert.ToDecimal(strPeso, cult);
                    peso = dValue;
                    try
                    {
                        decimal alt = Convert.ToDecimal(dt.Tables[0].Rows[0]["alto"].ToString());
                        alto = Convert.ToInt32(Math.Ceiling(alt));
                        //alto = Convert.ToInt32(dt.Tables[0].Rows[0]["alto"].ToString());

                        decimal lar = Convert.ToDecimal(dt.Tables[0].Rows[0]["largo"].ToString());
                        largo = Convert.ToInt32(Math.Ceiling(lar));
                        //largo = Convert.ToInt32(dt.Tables[0].Rows[0]["largo"].ToString());

                        decimal anc = Convert.ToDecimal(dt.Tables[0].Rows[0]["ancho"].ToString());
                        ancho = Convert.ToInt32(Math.Ceiling(anc));
                        //ancho = Convert.ToInt32(dt.Tables[0].Rows[0]["ancho"].ToString());
                    }
                    catch (Exception)
                    {
                        alto = 1;
                        largo = 1;
                        ancho = 1;
                    }
                }
                else
                {
                    peso = 1;
                    alto = 1;
                    largo = 1;
                    ancho = 1;
                }

                string servicio = "Registro API"; 

                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONEXION"].ToString()))
                {
                    //SqlCommand cmd2 = new SqlCommand("insertarAwb", cn);
                    SqlCommand cmd2 = new SqlCommand("Registrar_pedidoDespacho_2", cn);
                    
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    cmd2.Parameters.AddWithValue("awb", awb);
                    cmd2.Parameters.AddWithValue("coddespacho", codDespacho);
                    cmd2.Parameters.AddWithValue("coddespacho2", codDespacho);
                    cmd2.Parameters.AddWithValue("url", url_etiqueta);
                    cmd2.Parameters.AddWithValue("servicio", servicio);
                    cmd2.Parameters.AddWithValue("peso", peso);
                    cmd2.Parameters.AddWithValue("alto", alto);
                    cmd2.Parameters.AddWithValue("largo", largo);
                    cmd2.Parameters.AddWithValue("ancho", ancho);
                    cmd2.Parameters.AddWithValue("refCliente", order_id);
                    //cmd2.Parameters.AddWithValue("awb", awb);
                    //cmd2.Parameters.AddWithValue("codDespacho", codDespacho);
                    //cmd2.Parameters.AddWithValue("peso", peso);
                    //cmd2.Parameters.AddWithValue("alto", alto);
                    //cmd2.Parameters.AddWithValue("largo", largo);
                    //cmd2.Parameters.AddWithValue("ancho", ancho);
                    //cmd2.Parameters.AddWithValue("cliente", cliente);
                    //cmd2.Parameters.AddWithValue("NomCli1", NomCli1);
                    //cmd2.Parameters.AddWithValue("NomCli2", NomCli2);
                    //cmd2.Parameters.AddWithValue("ApatCli", ApatCli);
                    //cmd2.Parameters.AddWithValue("AmatCli", AmatCli);
                    //cmd2.Parameters.AddWithValue("direccion", direccion);
                    //cmd2.Parameters.AddWithValue("cantidad", cantidad);
                    //cmd2.Parameters.AddWithValue("sku", sku);
                    SqlDataReader resultado2 = cmd2.ExecuteReader();
                    resultado2.Read();
                    cn.Close();
                }
            }
            catch (Exception ex)
            {
                string err = ex.ToString();
            }

        }

        private DataSet obtenerDimensiones(string sku, string cliente)
        {
            DataSet datos = new DataSet();
            try
            {
                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONEXION"].ToString()))
                {
                    SqlCommand cmd2 = new SqlCommand("dimensiones_por_sku", cn);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    cmd2.Parameters.AddWithValue("sku", sku);
                    cmd2.Parameters.AddWithValue("cliente", cliente);
                    SqlDataAdapter result = new SqlDataAdapter(cmd2);

                    result.Fill(datos);
                    cn.Close();
                }
            }
            catch (Exception exx)
            {
                string err = exx.ToString();
            }
            return datos;
        }

        public Int32 despachoGratis(string cliente, string comuna, int precio, string tags, string vendor)
        {
            Int32 costDesp = 0;
            try
            {
                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONEXION"].ToString()))
                {
                    SqlCommand cmd2 = new SqlCommand("obtenerCostoDespacho", cn);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cn.Open();

                    cmd2.Parameters.AddWithValue("cliente", cliente);
                    cmd2.Parameters.AddWithValue("comuna", comuna);
                    cmd2.Parameters.AddWithValue("precio", precio);
                    cmd2.Parameters.AddWithValue("tags", tags.Trim());
                    cmd2.Parameters.AddWithValue("vendor", vendor.Trim());
                    SqlDataReader resultado = cmd2.ExecuteReader();
                    resultado.Read();
                    costDesp = Int32.Parse(resultado["costo_despacho"].ToString());
                    cn.Close();
                }
            }
            catch (Exception exx)
            {
                string err = exx.ToString();
                return 1;
            }
            return costDesp;
        }

        public DataSet getClienteCredenciales(string cliente)
        {
            DataSet datos = new DataSet();

            try
            {
                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONEXION"].ToString()))
                {
                    SqlCommand cmd2 = new SqlCommand("cliente_credenciales", cn);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    cmd2.Parameters.AddWithValue("cliente", cliente);
                    SqlDataAdapter result = new SqlDataAdapter(cmd2);

                    result.Fill(datos);
                    cn.Close();
                }
            }
            catch (Exception exx)
            {
                string err = exx.ToString();
            }
            return datos;
        }

        public string verificarExisteTagsCliente(string cliente, string comuna, string vendor_compania)
        {
            string tags = "";
            try
            {
                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONEXION"].ToString()))
                {
                    SqlCommand cmd2 = new SqlCommand("verificarExisteTagsCliente", cn);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    cmd2.Parameters.AddWithValue("cliente", cliente);
                    cmd2.Parameters.AddWithValue("comuna", comuna);
                    cmd2.Parameters.AddWithValue("vendor_compania", vendor_compania);
                    SqlDataReader resultado = cmd2.ExecuteReader();
                    if (resultado.HasRows)
                    {
                        while (resultado.Read())
                        {
                            tags = resultado["tags_products"].ToString() + "," + tags;
                        }
                    }


                    cn.Close();
                }
            }
            catch (Exception exx)
            {
                string err = exx.ToString();
                return tags = "";
            }
            return tags.TrimEnd(',');
        }

        public string obtieneSKU(string cliente, string tags)
        {
            string sku = "";
            try
            {
                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONEXION"].ToString()))
                {
                    SqlCommand cmd2 = new SqlCommand("obtenerSKUPromocionTags", cn);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    cmd2.Parameters.AddWithValue("cliente", cliente);
                    cmd2.Parameters.AddWithValue("tags", tags);
                    SqlDataReader resultado = cmd2.ExecuteReader();
                    if (resultado.HasRows)
                    {
                        while (resultado.Read())
                        {
                            sku = resultado["sku_promocion"].ToString() + "," + sku;
                        }
                    }


                    cn.Close();
                }
            }
            catch (Exception exx)
            {
                string err = exx.ToString();
                return sku = "";
            }
            return sku.TrimEnd(',');
        }

        public Costo calcularEnvioChileParcels(string comuna, string cliente, List<Productos> productos)
        {
            Costo newCosto;
            newCosto = new Costo();
            decimal pesoEfectivo = 0;
            int precio = 0;
            int horasEstimadas = 0;
            try
            {
                foreach (var prod in productos)
                {
                    pesoEfectivo = pesoEfectivo + obtenerPrecio(prod.sku, prod.cantidad, cliente);
                }

                // Definir tarifas con `int` en lugar de `decimal`
                List<TarifaEnvio> tarifas = new List<TarifaEnvio>
                {
                    new TarifaEnvio(0m, 3m, 3390),   // 0kg - 3kg → $3000
                    new TarifaEnvio(3m, 6m, 4990),   // 3kg - 6kg → $5000
                    new TarifaEnvio(6m, 10m, 6190),  // 6kg - 10kg → $7000
                    new TarifaEnvio(10m, 15m, 9490), // 10kg - 15kg → $9000
                    new TarifaEnvio(15m, 25m, 13990) // 15kg - 25kg → $12000
                };

                foreach (var tarifa in tarifas)
                {
                    if (pesoEfectivo >= tarifa.MinPeso && pesoEfectivo <= tarifa.MaxPeso)
                    {
                        precio = tarifa.Costo;
                        break; // No es necesario seguir buscando después de encontrar el rango correcto
                    }
                }
            }
            catch (Exception exx)
            {
                string err = exx.ToString();
            }
            newCosto.precio = precio;
            return newCosto;
        }

        public int buscarComunaChileParcels(string comuna)
        {
            Int32 id_comuna = 0;
            try
            {

                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONEXION"].ToString()))
                {
                    SqlCommand cmd2 = new SqlCommand("obtenerIdComunaCotizadorChileParcels", cn);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    cmd2.Parameters.AddWithValue("comuna", comuna);
                    SqlDataReader resultado2 = cmd2.ExecuteReader();
                    resultado2.Read();
                    id_comuna = Convert.ToInt32(resultado2["id_comuna"]);
                    cn.Close();
                }
            }
            catch (Exception exx)
            {
                string err = exx.ToString();
                return id_comuna;
            }
            return id_comuna;
        }
    }
}