//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CostosDeDespacho.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Registro
    {
        public long Tracking { get; set; }
        public string digito { get; set; }
        public string NomCli1 { get; set; }
        public string NomCli2 { get; set; }
        public string ApatCli { get; set; }
        public string AmatCli { get; set; }
        public string ReferenciaCliente { get; set; }
        public string Referencia { get; set; }
        public string calle { get; set; }
        public string numero { get; set; }
        public string comuna { get; set; }
        public string region { get; set; }
        public string ciudad { get; set; }
        public string comentario { get; set; }
        public string postaBlue { get; set; }
        public string telefono { get; set; }
        public string TipoServicio { get; set; }
        public Nullable<decimal> peso { get; set; }
        public string producto { get; set; }
        public string respuesta { get; set; }
        public System.DateTime fecha { get; set; }
        public int Batch { get; set; }
        public Nullable<int> Pre_salida { get; set; }
    }
}
