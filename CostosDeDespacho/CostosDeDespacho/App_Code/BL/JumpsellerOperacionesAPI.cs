﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using CostosDeDespacho.App_Code.DTO.Jumpseller;
using CostosDeDespacho.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using CostosDeDespacho.App_Code.Utils;
using System.Collections.Specialized;

namespace CostosDeDespacho.App_Code.BL
{
    public class JumpsellerOperacionesAPI
    {
        public string tokenType { get; set; }
        public string token { get; set; }

        /// <summary>
        /// Al crear la llamada a jumpseller vía api, es necesario tener token y el tipo para agregar a los headers
        /// </summary>
        /// <param name="tokenType">Tipo de token</param>
        /// <param name="token">Token para llamada</param>
        public JumpsellerOperacionesAPI(string tokenType, string token)
        {
            this.token = token;
            this.tokenType = tokenType;
        }

        /// <summary>
        /// Obtiene la información de la tienda y la guarda en la bd de Shipex
        /// </summary>
        /// <returns></returns>
        public JUMPSELLER_STORES saveInfoStore()
        {
            try
            {
                using (WebClient wc = new WebClient())
                {
                    Log.Debug("Guardando info de tienda");
                    wc.Headers[HttpRequestHeader.ContentType] = "application/json";
                    wc.Headers[HttpRequestHeader.Authorization] = this.tokenType + " " + this.token;
                    string urlStoreInfo = ConfigurationManager.AppSettings["api.jumpseller.url.store.info"];
                    string strInfo = wc.DownloadString(urlStoreInfo);
                    Log.Debug("res store.info " + strInfo);
                    var item = JObject.Parse(strInfo);


                    db_shipexEntities objShipex = new db_shipexEntities();
                    string codigoTienda = item["store"]["code"].ToString();
                    Log.Debug("Buscando datos de tienda " + codigoTienda);
                    JUMPSELLER_STORES objJumSellerStore = objShipex.JUMPSELLER_STORES.Where(x => x.codigo_tienda.Equals(codigoTienda)).FirstOrDefault();
                    if (objJumSellerStore == null)
                    {
                        Log.Debug("Guardando datos de tienda " + codigoTienda);
                        objJumSellerStore = new JUMPSELLER_STORES();
                        objJumSellerStore.activo = true;
                        objJumSellerStore.codigo_tienda = item["store"]["code"].ToString();
                        objJumSellerStore.email_tienda = item["store"]["email"].ToString();
                        objJumSellerStore.fecha_creacion = DateTime.Now;
                        objJumSellerStore.hooks_token = item["store"]["hooks_token"].ToString();
                        objJumSellerStore.nombre_tienda = item["store"]["name"].ToString();
                        objJumSellerStore.pais = item["store"]["country"].ToString();
                        objJumSellerStore.plan_tienda = item["store"]["subscription_plan"].ToString();
                        objJumSellerStore.time_zone = item["store"]["timezone"].ToString();
                        objJumSellerStore.unidad_peso = item["store"]["weight_unit"].ToString();
                        objJumSellerStore.url_tienda = item["store"]["url"].ToString();
                        objShipex.JUMPSELLER_STORES.Add(objJumSellerStore);
                        objShipex.SaveChanges();
                    }
                    else
                    {
                        Log.Debug("JumpsellerOperacionesAPI.saveInfoStore - Tienda ya registrada");
                    }
                    return objJumSellerStore;
                }
            }
            catch (Exception e)
            {
                Log.Error("Error App_Code.BL.Jumpseller.saveInfoStore: " + e.ToString());
                return null;
            }
        }

        public JUMPSELLER_STORES getInfoStore()
        {
            try
            {
                using (WebClient wc = new WebClient())
                {
                    wc.Headers[HttpRequestHeader.ContentType] = "application/json";
                    wc.Headers[HttpRequestHeader.Authorization] = this.tokenType + " " + this.token;
                    string urlStoreInfo = ConfigurationManager.AppSettings["api.jumpseller.url.store.info"];
                    string strInfo = wc.DownloadString(urlStoreInfo);
                    var item = JObject.Parse(strInfo);
                    db_shipexEntities objShipex = new db_shipexEntities();
                    string codigoTienda = item["store"]["code"].ToString();
                    JUMPSELLER_STORES objJumSellerStore = objShipex.JUMPSELLER_STORES.Where(x => x.codigo_tienda.Equals(codigoTienda)).FirstOrDefault();
                    return objJumSellerStore;
                }
            }
            catch (Exception e)
            {
                Log.Error("Error App_Code.BL.Jumpseller.getInfoStore: " + e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Obtener orden por id de orden jumpseller
        /// </summary>
        /// <param name="orderId">Id orden jumpseller</param>
        /// <returns></returns>
        public OrderJumpsellerWrapper getOrderByOrderId(string orderId)
        {
            try
            {
                string urlRetriveOrder = ConfigurationManager.AppSettings["api.jumpseller.url.retrive.order"];
                urlRetriveOrder = urlRetriveOrder.Replace("{id}", orderId);
                using (WebClient wc = new WebClient())
                {
                    wc.Headers[HttpRequestHeader.ContentType] = "application/json";
                    wc.Headers[HttpRequestHeader.Authorization] = this.tokenType + " " + this.token;
                    string jsonResult = wc.DownloadString(urlRetriveOrder);
                    OrderJumpsellerWrapper order = JsonConvert.DeserializeObject<OrderJumpsellerWrapper>(jsonResult);
                    return order;
                }   
            }
            catch (Exception e)
            {
                Log.Error("Error App_Code.BL.Jumpseller.getOrderByOrderId: " + e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Obtiene el listado de ordenes según su estado
        /// </summary>
        /// <param name="status">Estado de la orden (Ver CONSTANTES.JumpsellerOrders)</param>
        /// <returns></returns>
        public List<OrderJumpsellerWrapper> getOrdersByStatus(string status)
        {
            string appShippingName = ConfigurationManager.AppSettings["app.jumpseller.shipping_name"];
            try
            {
                using (WebClient wc = new WebClient())
                {
                    wc.Headers[HttpRequestHeader.ContentType] = "application/json";
                    wc.Headers[HttpRequestHeader.Authorization] = this.tokenType + " " + this.token;
                    string urlOrders = ConfigurationManager.AppSettings["api.jumpseller.url.orders"];
                    urlOrders = urlOrders.Replace("[[order_status]]", status);
                    string srtOrders = wc.DownloadString(urlOrders);
                    List<OrderJumpsellerWrapper> lOderJumpSellerWrapper = new List<OrderJumpsellerWrapper>();
                    List<OrderJumpsellerWrapper> values = JsonConvert.DeserializeObject<List<OrderJumpsellerWrapper>>(srtOrders);
                    foreach (OrderJumpsellerWrapper orderWrapper in values)
                    {
                        if (orderWrapper.order.shipping_method_name.Equals(appShippingName))
                        {
                            lOderJumpSellerWrapper.Add(orderWrapper);
                        }
                    }
                    return lOderJumpSellerWrapper;
                }
            }
            catch (Exception e)
            {
                Log.Error("Error App_Code.BL.Jumpseller.getOrdersByStatus: " + e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Metodo para crear shipping Shipex en Jumpseller
        /// </summary>
        /// <param name="tokenShipex">Token del cliente shipex</param>
        /// <returns>True en caso que se cree el shipping correctamente</returns>
        public ShippingMethodWrapper createShippingMethod(string tokenShipex)
        {
            try
            {
                string urlCreateShipping = ConfigurationManager.AppSettings["api.jumpseller.url.createShipping"];
                string shippingName = ConfigurationManager.AppSettings["app.jumpseller.shipping_name"];
                //TODO: QUITAR COMENTARIO y cambiar en clase anonima
                string callBackurl = ConfigurationManager.AppSettings["app.jumpseller.url_callback"];
                string fetchServicesUrl = ConfigurationManager.AppSettings["app.jumpseller.fetch_services_url"];
                //TODO: Se debe sacar el token segun la tienda por ahora se realiza en duro

                string stateValue = ConfigurationManager.AppSettings["app.jumpseller.state"];
                string cityValue = ConfigurationManager.AppSettings["app.jumpseller.city"];
                string postalCode = ConfigurationManager.AppSettings["app.jumpseller.postalcode"];
                var parameters = new
                {
                    name = shippingName,
                    callback_url = callBackurl,
                    fetch_services_url = fetchServicesUrl,
                    token = tokenShipex,
                    state = stateValue,
                    city = cityValue,
                    postal = postalCode
                };
                var clasePadre = new { shipping_method = parameters };
                using (WebClient wc = new WebClient())
                {
                    wc.Headers[HttpRequestHeader.ContentType] = "application/json";
                    wc.Headers[HttpRequestHeader.Authorization] = this.tokenType + " " + this.token;
                    string jsonstr = JsonConvert.SerializeObject(clasePadre);
                    string jsonResult = wc.UploadString(new Uri(urlCreateShipping), "POST", jsonstr);
                    Log.Debug("Response de shipping_methods.json" + jsonResult);
                    ShippingMethodWrapper objShipping = JsonConvert.DeserializeObject<ShippingMethodWrapper>(jsonResult);
                    return objShipping;
                }
            }
            catch (Exception e)
            {
                Log.Error("Error App_Code.BL.Jumpseller.createShippingMethod: " + e.ToString());
                return null;
            }
            return null;
        }

        /// <summary>
        /// Registra Webhook en Jumpseller
        /// </summary>
        /// <param name="eventName">Nombre del evento Jumpseller</param>
        /// <param name="urlEnvioWebHook">Url donde se notificará cuando ocurra el evento</param>
        /// <returns>True en caso de crear correctamente el registro de la notificación</returns>
        public bool createWebhook(string eventName, string urlEnvioWebHook)
        {
            string urlCreateShipping = ConfigurationManager.AppSettings["api.jumpseller.url.createWebhook"];
            var parameters = new
            {
                @event = eventName,
                url = urlEnvioWebHook
            };
            var clasePadre = new
            {
                hook = parameters
            };
            try
            {
                using (WebClient wc = new WebClient())
                {
                    wc.Headers[HttpRequestHeader.ContentType] = "application/json";
                    wc.Headers[HttpRequestHeader.Authorization] = this.tokenType + " " + this.token;
                    string jsonstr = JsonConvert.SerializeObject(clasePadre);
                    string jsonResult = wc.UploadString(new Uri(urlCreateShipping), "POST", jsonstr);
                }
                return true;
            }
            catch (Exception e)
            {
                Log.Error("Error App_Code.BL.Jumpseller.createWebhook: " + e.ToString());
                return false;
            }
        }


        public bool createFulfillment(FulfillmentWrapper objFulfillmentWrapper)
        {
            try
            {
                string urlcreateFulfillment = ConfigurationManager.AppSettings["api.jumpseller.url.createFulfillment"];
                using (WebClient wc = new WebClient())
                {
                    wc.Headers[HttpRequestHeader.ContentType] = "application/json";
                    wc.Headers[HttpRequestHeader.Authorization] = this.tokenType + " " + this.token;
                    string jsonstr = JsonConvert.SerializeObject(objFulfillmentWrapper);
                    string jsonResult = wc.UploadString(new Uri(urlcreateFulfillment), "POST", jsonstr);
                    Fulfillment values = JsonConvert.DeserializeObject<Fulfillment>(jsonResult);
                    return values != null ? true : false;
                }
            }
            catch (Exception e)
            {
                Log.Error("Error App_Code.BL.Jumpseller.createFulfillment: " + e.ToString());
                return false;
            }
        }

        /// <summary>
        /// Actualizacion de orden 
        /// </summary>
        /// <param name="orderId">ID orden (requerido)</param>
        /// <param name="status">estado de la orden</param>
        /// <param name="shipment_status"></param>
        /// <param name="tracking_number"></param>
        /// <param name="tracking_company"></param>
        /// <param name="tracking_url"></param>
        /// <param name="additional_information"></param>
        /// <returns></returns>
        public bool updateOrder(string orderId, string status, string shipment_status, string tracking_number, string tracking_company, string tracking_url, string additional_information)
        {
            try
            {
                string urlcreateFulfillment = ConfigurationManager.AppSettings["api.jumpseller.url.update.order"];
                urlcreateFulfillment = urlcreateFulfillment.Replace("{id}", orderId);
                using (WebClient wc = new WebClient())
                {
                    wc.Headers[HttpRequestHeader.ContentType] = "application/json";
                    wc.Headers[HttpRequestHeader.Authorization] = this.tokenType + " " + this.token;
                    var parameters = new
                    {
                        status = status,
                        shipment_status = shipment_status,
                        tracking_number = tracking_number,
                        tracking_company = tracking_company,
                        tracking_url = tracking_url,
                        additional_information = additional_information
                    };
                    var clasePadre = new
                    {
                        order = parameters
                    };
                    string jsonstr = JsonConvert.SerializeObject(clasePadre);
                    string jsonResult = wc.UploadString(new Uri(urlcreateFulfillment), "PUT", jsonstr);
                    OrderJumpsellerWrapper orderJumpsellerW = JsonConvert.DeserializeObject<OrderJumpsellerWrapper>(jsonResult);
                    return true;
                    //TODO: A LA ESPERA DEL FIX JUMPSELLER, DESCOMENTAR LA LINEA
                    //return (orderJumpsellerW != null && (orderJumpsellerW.order.tracking_url != null || orderJumpsellerW.order.tracking_url.Trim() != "")) ? true : false;
                }
            }
            catch (Exception e)
            {
                Log.Error("Error App_Code.BL.Jumpseller.updateOrder: " + e.ToString());
                return false;
            }
        }

        /// <summary>
        /// Obtener a que tienda corresponde a un token obtenido
        /// </summary>
        /// <returns>Objeto con información del token</returns>
        public TokenInfo obtenerInfoTiendaPorToken()
        {
            try
            {
                string urlTokenInfo = ConfigurationManager.AppSettings["api.jumpseller.url.token.info"];
                using (WebClient wc = new WebClient())
                {
                    wc.Headers[HttpRequestHeader.ContentType] = "application/json";
                    wc.Headers[HttpRequestHeader.Authorization] = this.tokenType + " " + this.token;
                    string jsonResult = wc.DownloadString(urlTokenInfo);
                    TokenInfo objTokenInfo = JsonConvert.DeserializeObject<TokenInfo>(jsonResult);
                    return objTokenInfo;
                }
            }
            catch (Exception e)
            {
                Log.Error("Error App_Code.BL.Jumpseller.obtenerInfoTiendaPorToken: " + e.ToString());
                return null;
            }
        }


        public string deteleShippingMethod(int idShippingMethod)
        {
            try
            {
                string urlShippingMethod = ConfigurationManager.AppSettings["api.jumpseller.url.shipping.methods"];
                urlShippingMethod = urlShippingMethod.Replace("{id}", idShippingMethod.ToString());
                Log.Debug("url eliminar shiping " + urlShippingMethod);
                using (WebClient wc = new WebClient())
                {
                    wc.Headers[HttpRequestHeader.ContentType] = "application/json";
                    wc.Headers[HttpRequestHeader.Authorization] = this.tokenType + " " + this.token;
                    string jsonResult = wc.UploadString(new Uri(urlShippingMethod), "DELETE", "");
                    Log.Debug("Resultado eliminar shipping method :" + jsonResult);
                    return jsonResult;
                }
            }
            catch (Exception e)
            {
                Log.Error("Error App_Code.BL.Jumpseller.deleteShippingMethod: " + e.ToString());
                return null;
            }
        }

        public string getShippingMethods()
        {
            try
            {
                string urlShippingMethod = ConfigurationManager.AppSettings["api.jumpseller.url.getShipping"];
                using (WebClient wc = new WebClient())
                {
                    wc.Headers[HttpRequestHeader.ContentType] = "application/json";
                    wc.Headers[HttpRequestHeader.Authorization] = this.tokenType + " " + this.token;
                    string jsonResult = wc.DownloadString(urlShippingMethod);
                    List<ShippingMethodWrapper> objTokenInfo = JsonConvert.DeserializeObject<List<ShippingMethodWrapper>>(jsonResult);
                    Log.Debug("Resultado eliminar shipping method :" + jsonResult);
                    return jsonResult;
                }
                return null;
            }
            catch(Exception e)
            {
                Log.Error("Error App_Code.BL.Jumpseller.obtenerInfoTiendaPorToken: " + e.ToString());
                return null;
            }
        }
    }
}