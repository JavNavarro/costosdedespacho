﻿using CostosDeDespacho.App_Code.DTO.ShipexAPI;
using CostosDeDespacho.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.Controllers
{
    public class Rate
    {
        public App_Code.DTO.ShipexAPI.Destination destination { get; set; } 
        public List<ShopifyItem> items { get; set; } 
    }
}