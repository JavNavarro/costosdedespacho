﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.App_Code.BE
{
    public class DatosDespacho
    {
        public string direccion { get; set; }
        public string nombreDestinatario { set; get; }
        public string comuna { set; get; }
        public decimal pesoEfectivo { set; get; }
        public string cliente { set; get; }
        public string phone { set; get; }
        public string orderId { set; get; }
        public Int64 codigoDespacho { set; get; }


    }
}