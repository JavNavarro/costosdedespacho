﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.App_Code.BL
{
    public class CostoLogic
    {
        BDconn conBD = new BDconn();

        public void revisarCotizacion()
        {

        }

        public Costo costoDirecto(string jsonContent)
        {
            
            List<DatosEnvio> dat = JsonConvert.DeserializeObject<List<DatosEnvio>>(jsonContent);
            Costo cost = new Costo();
            Costo cost2 = new Costo();           

            foreach (var item in dat)
            {
                cost2 = conBD.calcularEnvio(item.comuna, item.cliente, item.products);// item.sku);
                cost.estimadoHoras = cost2.estimadoHoras;
                cost.precio = cost.precio + cost2.precio;
            }
            return cost;
       
        }

        public Costo costoDirectoChileParcels(string jsonContent)
        {

            List<DatosEnvio> dat = JsonConvert.DeserializeObject<List<DatosEnvio>>(jsonContent);
            Costo cost = new Costo();
            Costo cost2 = new Costo();

            foreach (var item in dat)
            {
                cost2 = conBD.calcularEnvioChileParcels(item.comuna, item.cliente, item.products);// item.sku);
                cost.precio = cost.precio + cost2.precio;
            }
            return cost;

        }
    }
}