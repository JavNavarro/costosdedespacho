﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.App_Code.DTO.Jumpseller
{
    public class Fulfillment
    {
        public string shipment_status { get; set; }
        public string order_id { get; set; }
        public string type { get; set; }
        public string tracking_number { get; set; }
        public string tracking_company { get; set; }
        public string external_id { get; set; }
        public string service_type { get; set; }
        public string expected_arrival_from { get; set; }
        public string expected_arrival_to { get; set; }
    }
}