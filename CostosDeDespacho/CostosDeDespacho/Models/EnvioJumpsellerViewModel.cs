﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.Models
{
    public class EnvioJumpsellerViewModel
    {
        public string name { get; set; }
        public string surname { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string postal { get; set; }
        public string region { get; set; }
        public string country { get; set; }
        public string country_code { get; set; }
        public string region_code { get; set; }
        public string street_number { get; set; }
        public float? latitude { get; set; }
        public float? longitude { get; set; }
        public string municipality { get; set; }
        public string complement { get; set; }
    }
}