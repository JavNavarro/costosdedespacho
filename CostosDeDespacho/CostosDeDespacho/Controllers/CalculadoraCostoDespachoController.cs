﻿using CostosDeDespacho.App_Code.DTO.CalculadoraCostosDespacho;
using CostosDeDespacho.App_Code.Utils;
using CostosDeDespacho.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CostosDeDespacho.Controllers
{
    public class CalculadoraCostoDespachoController : Controller
    {
        // GET: CalculadoraCostoDespacho
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult obtenerPrecioDespacho(CalculadoraCostoDespachoDTO objCalculadoraCostoDespachoDTO)
        {
            try
            {
                db_shipexEntities context = new db_shipexEntities();
                //comuna objComuna = context.comuna.Where(x => x.comuna_nombre.Equals(objCalculadoraCostoDespachoDTO.comuna)).FirstOrDefault();
                comuna objComuna = context.comuna.Where(x => x.comuna_nombre.Replace("ñ", "n").Replace("á", "a").Replace("é", "e").Replace("í", "i").Replace("ó", "o").Replace("ú", "u") == objCalculadoraCostoDespachoDTO.comuna.Replace("ñ", "n").Replace("á", "a").Replace("é", "e").Replace("í", "i").Replace("ó", "o").Replace("ú", "u")).FirstOrDefault();

                if (objComuna == null)
                {
                    //Diccionario_comunas objDiccionarioComuna = context.Diccionario_comunas.Where(x => x.nombreComuna.Equals(objCalculadoraCostoDespachoDTO.comuna)).FirstOrDefault();
                    Diccionario_comunas objDiccionarioComuna = context.Diccionario_comunas.Where(x => x.nombreComuna.Replace("ñ", "n").Replace("á", "a").Replace("é", "e").Replace("í", "i").Replace("ó", "o").Replace("ú", "u") == objCalculadoraCostoDespachoDTO.comuna.Replace("ñ", "n").Replace("á", "a").Replace("é", "e").Replace("í", "i").Replace("ó", "o").Replace("ú", "u")).FirstOrDefault();
                    if (objDiccionarioComuna == null)
                    {
                        //no encuentra en diccionario
                        var resultado = new
                        {
                            res = "Error: No se encuentra la comuna enviada",
                            status = System.Net.HttpStatusCode.InternalServerError
                        };
                        return Json(resultado, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        //se encuentra en diccionario, se consulta nuevamente a la bd
                        //objComuna = context.comuna.Where(x => x.comuna_nombre.Equals(objDiccionarioComuna.nombreReal)).FirstOrDefault();
                        objComuna = context.comuna.Where(x => x.comuna_nombre.Replace("ñ", "n").Replace("á", "a").Replace("é", "e").Replace("í", "i").Replace("ó", "o").Replace("ú", "u") == objDiccionarioComuna.nombreReal.Replace("ñ", "n").Replace("á", "a").Replace("é", "e").Replace("í", "i").Replace("ó", "o").Replace("ú", "u")).FirstOrDefault();
                        if (objComuna == null)
                        {
                            //no encontramos la comuna en la tabla definida (caso extremo)
                            var resultado = new
                            {
                                res = "Error: No se encuentra la comuna enviada",
                                status = System.Net.HttpStatusCode.InternalServerError
                            };
                            return Json(resultado, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                double pesoEfectivo = objCalculadoraCostoDespachoDTO.peso_efectivo;
                int idCliente = objCalculadoraCostoDespachoDTO.id_cliente;
                int idComuna = objComuna.comuna_id;
                var lResultados = (from cd in context.costo_despacho
                                   join rt in context.rangosTarifas on cd.id_rango equals rt.idRango
                                   where cd.id_cliente == idCliente && cd.estado == 1 && pesoEfectivo >= rt.cotaInf && (pesoEfectivo <= rt.cotaSup || pesoEfectivo > rt.cotaSup) && cd.id_comuna == idComuna
                                   select new
                                   {
                                       precio = cd.precio,
                                       id_comuna = cd.id_comuna,
                                       id_cliente = cd.id_cliente,
                                       cotaSuperior = rt.cotaSup,
                                       cotaInferior = rt.cotaInf,
                                       rango = rt.rango
                                   }).ToList();
                //obtenemos el maximo rango configurado
                var maxRangos = (from cd in context.costo_despacho
                                 join rt in context.rangosTarifas on cd.id_rango equals rt.idRango
                                 where cd.id_cliente == idCliente && cd.estado == 1 && cd.id_comuna == idComuna
                                 select new
                                 {
                                     cotaSuperior = rt.cotaSup,
                                     cotaInferior = rt.cotaInf
                                 }).ToList();

                var despacho = (from pd in context.promociones_despacho
                                where pd.id_cliente == idCliente && pd.id_comuna == idComuna && pd.activo == true && pd.despacho_gratis != null
                                select new { id_promocion = pd.id, id_cliente = pd.id_cliente, id_comuna = pd.id_comuna, despacho_gratis = pd.despacho_gratis, costo_tarifa = pd.precio_min }).OrderByDescending(x => x.id_promocion).FirstOrDefault();

                //Revision peso restante
                var ultimoRango = maxRangos[maxRangos.Count - 1];
                double maxPeso = ultimoRango.cotaSuperior;
                int costoPrecioKiloExtra = 0;
                if (objCalculadoraCostoDespachoDTO.peso_efectivo >= maxPeso)
                {
                    //obtengo precio por kilo extra
                    precioPorKilo_comuna objPrecioPorKiloComuna = context.precioPorKilo_comuna.Where(x => x.id_comuna == objComuna.comuna_id && x.id_cliente == objCalculadoraCostoDespachoDTO.id_cliente).FirstOrDefault();
                    double pesoRestante = 0;
                    pesoRestante = pesoEfectivo - maxPeso;
                    costoPrecioKiloExtra = Convert.ToInt32(Math.Ceiling(pesoRestante)) * objPrecioPorKiloComuna.precio;
                }
                //Fin peso restante

                Boolean? despacho_gratis;
                Int32? costo_tarifa;
                //obtenemos ultimo rango 
                var lRe = lResultados.Where(lr => lr.id_cliente == idCliente).ToList();
                var objResultado = lRe.LastOrDefault();
                int precioPagarCliente = objResultado.precio + costoPrecioKiloExtra;

                if (despacho == null)
                {
                    despacho_gratis = false;
                    costo_tarifa = precioPagarCliente;

                }
                else
                {
                    despacho_gratis = despacho.despacho_gratis == null ? false : despacho.despacho_gratis;
                    costo_tarifa = despacho.costo_tarifa == null ? precioPagarCliente : despacho.costo_tarifa;
                }

                var response = new
                {
                    data = precioPagarCliente,
                    despacho_gratis = despacho_gratis,
                    costo_tarifa_gratis = costo_tarifa,
                    status = System.Net.HttpStatusCode.OK
                };
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.Error("Error CalculadoraCostoDespacho.obtenerPrecioDespacho: " + ex.ToString());
                var resultado = new
                {
                    res = "Error: " + ex.ToString(),
                    status = System.Net.HttpStatusCode.InternalServerError
                };
                return Json(resultado, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult obtenerComunasShipexRegion()
        {
            try
            {
                if (Request.Headers["Authorization"] != null)
                {
                    string token = Request.Headers["Authorization"].ToString();
                    db_shipexEntities context = new db_shipexEntities();
                    var objApiKey = context.Api_Key.Where(x => x.token == token).FirstOrDefault();
                    if (objApiKey != null)
                    {
                        var lResultados = (from r in context.region
                                           join p in context.provincia on r.region_id equals p.region_id
                                           join c in context.comuna on p.provincia_id equals c.provincia_id
                                           select new
                                           {
                                               id_comuna = c.comuna_id,
                                               id_region = r.region_id,
                                               nombre_comuna = c.comuna_nombre
                                           }).ToList();
                        return Json(lResultados, JsonRequestBehavior.AllowGet);
                    }
                }
                var resultado = new
                {
                    res = "Error sin autorización",
                    status = System.Net.HttpStatusCode.InternalServerError
                };
                return Json(resultado, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.Error("Error CalculadoraCostoDespacho.obtenerRegionesShipex: " + ex.ToString());
                var resultado = new
                {
                    res = "Error: " + ex.ToString(),
                    status = System.Net.HttpStatusCode.InternalServerError
                };
                return Json(resultado, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult obtenerRegionesShipex()
        {
            try
            {
                if (Request.Headers["Authorization"] != null)
                {
                    string token = Request.Headers["Authorization"].ToString();
                    db_shipexEntities context = new db_shipexEntities();
                    var objApiKey = context.Api_Key.Where(x => x.token == token).FirstOrDefault();
                    if (objApiKey != null)
                    {
                        var lRegiones = context.region.ToList();
                        //Se crea lista debido a que si conectan relaciones quedan referencias y falla
                        List<RegionDTO> lRegionesDTO = new List<RegionDTO>();
                        foreach (var region in lRegiones)
                        {
                            RegionDTO _regionDTO = new RegionDTO
                            {
                                id_region = region.region_id,
                                nombre_region = region.region_nombre
                            };
                            lRegionesDTO.Add(_regionDTO);
                        }
                        return Json(lRegionesDTO, JsonRequestBehavior.AllowGet);
                    }
                }
                var resultado = new
                {
                    res = "Error sin autorización",
                    status = System.Net.HttpStatusCode.InternalServerError
                };
                return Json(resultado, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.Error("Error CalculadoraCostoDespacho.obtenerRegionesShipex: " + ex.ToString());
                var resultado = new
                {
                    res = "Error: " + ex.ToString(),
                    status = System.Net.HttpStatusCode.InternalServerError
                };
                return Json(resultado, JsonRequestBehavior.AllowGet);
            }
        }
    }
}