﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.App_Code.DTO.Jumpseller
{
    public class FulfillmentWrapper
    {
        public Fulfillment fulfillment { get; set; }
    }
}