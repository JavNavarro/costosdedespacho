﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.Models
{
    public class ComunaViewModel
    {
        public int comuna_id { get; set; }
        public string comuna_nombre { get; set; }
        public int provincia_id { get; set; }
    }
}