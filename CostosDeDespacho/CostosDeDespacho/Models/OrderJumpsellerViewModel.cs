﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.Models
{
    public class OrderJumpsellerViewModel
    {
        public int id { get; set; }
        public string created_at { get; set; }
        public string completed_at { get; set; }
        public string status { get; set; }
        public string currency { get; set; }
        public float subtotal { get; set; }
        /// <summary>
        /// Subtotal + shipping
        /// </summary>
        public float total { get; set; }
        public bool shipping_required { get; set; }
        public string payment_method_name { get; set; }
        public string additional_information { get; set; }
        public float shipping_discount { get; set; }
        //public ClienteJumpsellerViewModel Cliente { get; set; }
        //public EnvioJumpsellerViewModel Envio { get; set; }
        //public List<ProductoJumpsellerViewModel> lProducto { get; set; }
        public string tracking_number { get; set; }
        public string tracking_company { get; set; }
        public string tracking_url { get; set; }
        public string shipment_status { get; set; }
        public string shipping_method_name { get; set; }
        public string shipping_option { get; set; }
        public string shipping { get; set; }
        public ClienteJumpsellerViewModel customer { get; set; }
        public EnvioJumpsellerViewModel shipping_address { get; set; }
        public List<ProductoJumpsellerViewModel> products { get; set; }
    }
}