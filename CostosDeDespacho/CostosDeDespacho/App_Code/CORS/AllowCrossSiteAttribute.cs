﻿using CostosDeDespacho.App_Code.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CostosDeDespacho.App_Code.CORS
{
    public class AllowCrossSiteAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Origin","*");
            filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Headers","*");
            filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Credentials","true");
            filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Methods", "*");
            base.OnActionExecuting(filterContext);
        }
    }
}