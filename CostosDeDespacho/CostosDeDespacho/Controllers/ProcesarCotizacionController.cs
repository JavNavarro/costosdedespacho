﻿using CostosDeDespacho.App_Code.BE;
using CostosDeDespacho.App_Code.Utils;
using CostosDeDespacho.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;
using RouteAttribute = System.Web.Mvc.RouteAttribute;

namespace CostosDeDespacho.Controllers
{
    public class ProcesarCotizacionController : ApiController
    {
        db_shipexEntities context = new db_shipexEntities();
        BDconn conexion = new BDconn();
        private string myQueueItem;
        [System.Web.Mvc.HttpPost]
        public async Task<IHttpActionResult> Post()
        {
            List<Retorno> retorns = new List<Retorno>();
            bool Autorizado = false;
            Boolean SKUValidado = true;
            String token = String.Empty;
            RespuestaSKU respuestaSKU = new RespuestaSKU();
            Boolean validarComuna = true;
            string cliente = String.Empty;
            string comuna = String.Empty;
            string email = String.Empty;
            Int32 promocion_valida = 0;
            decimal peso_producto = 0;
            Int32 meses_promocion = 1;
            try
            {
                var requestContent = Request.Content;
                var jsonContent = await requestContent.ReadAsStringAsync();

                token = Request.Headers.Authorization.ToString();
                Log.Debug("Token cliente: " + token);

                List<DatosEnvio> data = new List<DatosEnvio>();
                DatosEnvio disEnvio = new DatosEnvio();
                List<Productos> products = new List<Productos>();
                ProductosPromocion productosPromocion = new ProductosPromocion();
                List<ProductosPromocion> LproductosPromocions = new List<ProductosPromocion>();
                int precio = 0;
                JObject json = JObject.Parse(jsonContent);
                disEnvio.comuna = json["rate"]["destination"]["city"].ToString();
                disEnvio.cliente = json["rate"]["origin"]["company_name"].ToString();
                cliente = disEnvio.cliente;
                comuna = disEnvio.comuna;
                email = json["rate"]["destination"]["email"].ToString() == null || json["rate"]["destination"]["email"].ToString() == "" ? "" : json["rate"]["destination"]["email"].ToString();

                //Boolean cliente_customer = conexion.getObtenerClienteCustomer(email);

                int id_cliente = conexion.GetIdCliente(disEnvio.cliente);

                if (id_cliente == 3)
                {
                    disEnvio.comuna = json["rate"]["destination"]["address2"].ToString();
                }
                else
                {
                    disEnvio.comuna = json["rate"]["destination"]["city"].ToString();
                }

                try
                {
                    string jsonStringLog = jsonContent.ToString();
                    conexion.guardarLog(jsonStringLog, "jsonContent de entrada", disEnvio.cliente, "Procesar Cotizacion Token");
                }
                catch (Exception ex)
                {

                }

                DataSet dtCliente = conexion.getClienteCredenciales(disEnvio.cliente);

                string usuario = "";
                string contrasenia = "";
                string url_tienda = "";

                if (dtCliente.Tables.Count != 0)
                {
                    if (dtCliente.Tables[0].Rows.Count > 0)
                    {
                        usuario = dtCliente.Tables[0].Rows[0]["usuario"].ToString();
                        contrasenia = dtCliente.Tables[0].Rows[0]["contraseña"].ToString();
                        url_tienda = dtCliente.Tables[0].Rows[0]["url_tienda"].ToString();
                    }
                }

                List<string> list_productID = new List<string>();
                var lst = "";

                foreach (JObject item in json["rate"]["items"])
                {
                    Productos product = new Productos();
                    product.sku = item["sku"].ToString();
                    product.cantidad = Int32.Parse(item["quantity"].ToString());
                    product.precio = Convert.ToInt32(item["price"]) * Int32.Parse(item["quantity"].ToString());
                    if (disEnvio.cliente == "Baby Steps")
                    {
                        product.price = Convert.ToInt32(item["price"]);
                    }
                    else
                    {
                        product.price = Convert.ToInt32(item["price"]) / 100;
                    }                        
                    product.product_id = item["product_id"].ToString() == null || item["product_id"].ToString() == "" ? 0 : Convert.ToInt64(item["product_id"]);
                    product.variant_id = item["variant_id"].ToString() == null || item["variant_id"].ToString() == "" ? 0 : Convert.ToInt64(item["variant_id"]);
                    //product.peso = Convert.ToDecimal(item["grams"]) * Int32.Parse(item["quantity"].ToString());
                    lst = Convert.ToString(product.product_id + "," + lst);
                    //precio = precio + product.precio / 100;
                    products.Add(product);
                }
                disEnvio.products = products;
                productosPromocion.products = products;



                Int32 tokenValidos = conexion.validarToken(token, json["rate"]["origin"]["company_name"].ToString());
                if (tokenValidos > 0)
                {
                    Autorizado = true;
                }



                List<string> skuNoValidos = new List<string>();
                Boolean validacionEnCaliente;

                foreach (Productos a in products)
                {
                    validacionEnCaliente = conexion.validarSku(a.sku, disEnvio.cliente);
                    if (!validacionEnCaliente)
                    {
                        SKUValidado = validacionEnCaliente;

                        skuNoValidos.Add(a.sku);
                    }

                }
                respuestaSKU.cliente = disEnvio.cliente;
                respuestaSKU.sku_faltantes = skuNoValidos;
                try
                {
                    if (respuestaSKU.sku_faltantes.Count > 0)
                    {
                        respuestaSKU.respuesta = "Problema con uno o muchos SKU no encontrados en nuestra base.";
                    }
                }
                catch(Exception ex)
                {

                }


                data.Add(disEnvio);
                var jsonContentStr = JsonConvert.SerializeObject(data);

                App_Code.BL.CostoLogic logica = new App_Code.BL.CostoLogic();
                Costo cost = new Costo();
                Retorno ret = new Retorno();

                DataSet dsEntrega = conexion.GetDatosRespuesta(disEnvio.cliente, disEnvio.comuna);

                string entrega = "";
                string descripcion = "";
                DateTime dMin = DateTime.Now.AddDays(1);
                DateTime dMax = DateTime.Now.AddDays(3);

                validarComuna = conexion.validarComuna(disEnvio.comuna);

                if (disEnvio.cliente == "Farmex")
                {
                    int existe_comuna = conexion.buscarComuna(disEnvio.comuna);

                    if (existe_comuna == 0)
                    {
                        ret.service_name = "Error Costo De Envío";
                        ret.description = "ESTE PEDIDO NO SE PUEDE ENVÍAR A LA COMUNA QUE INDICASTE. Revisa la comuna en el paso anterior para garantizar el correcto costo de envío";
                        ret.currency = "CLP";
                        ret.service_code = "NRML";
                        //ret.min_delivery_date = dMin.ToUniversalTime().ToString("u").Replace("Z", "").Replace("/", "-") + " " + ("-04:00");
                        //ret.max_delivery_date = dMax.ToUniversalTime().ToString("u").Replace("Z", "").Replace("/", "-") + " " + ("-04:00");
                        ret.total_price = 49990 * 100;
                        retorns.Add(ret);
                        return Get(retorns, Autorizado, token, disEnvio.cliente, respuestaSKU, validarComuna, disEnvio.comuna);
                    }
                }

                if (dsEntrega.Tables.Count != 0)
                {
                    if (dsEntrega.Tables[0].Rows.Count > 0)
                    {
                        entrega = dsEntrega.Tables[0].Rows[0]["entrega"].ToString();
                        descripcion = dsEntrega.Tables[0].Rows[0]["descripcion"].ToString();
                        ret.service_name = entrega;
                        ret.description = descripcion;
                        ret.currency = "CLP";
                        ret.service_code = "NRML";
                    }
                    else
                    {
                        ret.service_name = "Envío Shipex";
                        ret.description = "Los pedidos realizados después de las 2 p.m serán procesados como realizados el día hábil siguiente";
                        ret.currency = "CLP";
                        ret.service_code = "NRML";
                        ret.min_delivery_date = dMin.ToUniversalTime().ToString("u").Replace("Z", "").Replace("/", "-") + " " + ("-04:00");
                        ret.max_delivery_date = dMax.ToUniversalTime().ToString("u").Replace("Z", "").Replace("/", "-") + " " + ("-04:00");
                    }
                }
                else
                {
                    ret.service_name = "Envío Shipex";
                    ret.description = "Los pedidos realizados después de las 2 p.m serán procesados como realizados el día hábil siguiente";
                    ret.currency = "CLP";
                    ret.service_code = "NRML";
                    ret.min_delivery_date = dMin.ToUniversalTime().ToString("u").Replace("Z", "").Replace("/", "-") + " " + ("-04:00");
                    ret.max_delivery_date = dMax.ToUniversalTime().ToString("u").Replace("Z", "").Replace("/", "-") + " " + ("-04:00");
                }

                if (validarComuna)
                {
                    if (!Autorizado)
                    {
                        Int32 costo;
                        costo = conexion.calcularNoToken(token,disEnvio.comuna,products,disEnvio.cliente);
                        //Retorno ret = new Retorno();
                        ret.currency = "CLP";
                        ret.service_code = "NRML NO TOKEN";
                        ret.service_name = "Shipex entrega";
                        ret.total_price = costo;

                        ret.min_delivery_date = dMin.ToUniversalTime().ToString("u").Replace("Z", "").Replace("/", "-") + " " + ("-04:00");
                        ret.max_delivery_date = dMax.ToUniversalTime().ToString("u").Replace("Z", "").Replace("/", "-") + " " + ("-04:00");

                        if(costo > 0)
                        {
                            Autorizado = true;
                        }
                        if (ret.total_price > 0)
                        {
                            retorns.Add(ret);
                        }
                    }
                    else
                    {
                        foreach (Productos a in products)
                        {
                            validacionEnCaliente = conexion.validarSku(a.sku, disEnvio.cliente);
                            if (!validacionEnCaliente)
                            {
                                conexion.skuNoEncontrado(a.sku, disEnvio.cliente);
                                //SKUValidado = validacionEnCaliente;
                            }
                        }

                        if (disEnvio.cliente == "Baby Steps")
                        {
                            foreach (var item_prod in products)
                            {
                                precio = precio + item_prod.precio;
                                peso_producto = Convert.ToDecimal(peso_producto + item_prod.peso);
                            }
                        }
                        else
                        {
                            foreach (var item_prod in products)
                            {
                                precio = precio + item_prod.precio / 100;
                                peso_producto = Convert.ToDecimal(peso_producto + item_prod.peso);
                            }
                        }
                            

                        string obtiene_tagsDB = String.Empty;
                        string vendor_campania = String.Empty;

                        if (disEnvio.cliente == "Farmex" && json["rate"]["items"][0]["vendor"].ToString().Contains("-"))
                        {
                            vendor_campania = json["rate"]["items"][0]["vendor"].ToString().Split('-')[1];

                            obtiene_tagsDB = conexion.verificarExisteTagsCliente(disEnvio.cliente, disEnvio.comuna, vendor_campania);

                            if (obtiene_tagsDB == "")
                            {
                                vendor_campania = "";
                            }
                        }
                        else
                        {
                            obtiene_tagsDB = conexion.verificarExisteTagsCliente(disEnvio.cliente, disEnvio.comuna, "");
                        }
                        if (obtiene_tagsDB != "2")
                        {
                            Int32 costo_despacho = 999;

                            if (vendor_campania == "")
                            {
                                if (disEnvio.cliente == "Baby Steps")
                                {
                                    Boolean cliente_customer = conexion.getObtenerClienteCustomer(email, disEnvio.cliente);

                                    if (cliente_customer)
                                    {
                                        costo_despacho = conexion.despachoGratis(disEnvio.cliente, disEnvio.comuna, precio, "", "");

                                    }
                                    else
                                    {
                                        costo_despacho = conexion.despachoGratis(disEnvio.cliente, disEnvio.comuna, precio, "NOREGISTRADO", "Baby Steps");
                                        //costo_despacho = 1;
                                    }
                                }
                                else
                                {
                                    costo_despacho = conexion.despachoGratis(disEnvio.cliente, disEnvio.comuna, precio, "", "");
                                }
                                

                                //VALIDACION POR PESO CLIENTE DENTRO DEL PEDIDO
                                //if (cliente_customer)
                                //{
                                //    Boolean cumple_peso = conexion.getCumplePesoClienteComuna(disEnvio.cliente, disEnvio.comuna, peso_producto);

                                //    if (cumple_peso)
                                //    {
                                //        costo_despacho = conexion.despachoGratis(disEnvio.cliente, disEnvio.comuna, precio, "", "");
                                //    }
                                //    else
                                //    {
                                //        costo_despacho = 1;
                                //    }
                                //}
                                //else
                                //{
                                //    costo_despacho = conexion.despachoGratis(disEnvio.cliente, disEnvio.comuna, precio, "", "");
                                //}

                                //VALIDACION POR SKU EN PEDIDO
                                //if (cliente_customer)
                                //{
                                //    foreach (var item_prod in products)
                                //    {
                                //        var lResultados = (from tc in context.tienda_cliente
                                //                           join sp in context.sku_promocion_despacho on tc.idCliente equals sp.id_cliente
                                //                           where tc.nom_Tienda == disEnvio.cliente && sp.sku_promocion == item_prod.sku
                                //                           select new
                                //                           {

                                //                           }).ToList();

                                //        if (lResultados.Count > 0)
                                //        {
                                //            promocion_valida = 1;
                                //            break;
                                //        }
                                //    }

                                //    if (promocion_valida == 1)
                                //    {
                                //        costo_despacho = conexion.despachoGratis(disEnvio.cliente, disEnvio.comuna, precio, "", "");
                                //    }
                                //    else
                                //    {
                                //        costo_despacho = 1;
                                //    }


                                //}
                                //else
                                //{
                                //    costo_despacho = conexion.despachoGratis(disEnvio.cliente, disEnvio.comuna, precio, "", "");
                                //}
                            }
                            else
                            {
                                productosPromocion.campaign = vendor_campania;

                                ProductoPromocionResponse response = conexion.ObtieneDespachoGratisPromocionCampana(productosPromocion);

                                if (response.exist_in_campaign == true)
                                {
                                    costo_despacho = conexion.despachoGratis(disEnvio.cliente, disEnvio.comuna, precio, productosPromocion.campaign, "Farmex");
                                }
                                else
                                {
                                    costo_despacho = conexion.despachoGratis(disEnvio.cliente, disEnvio.comuna, precio, "", "");
                                }
                            }


                            if (costo_despacho == 1)
                            {
                                id_cliente = conexion.Get_codCliente(disEnvio.cliente);

                                cost = logica.costoDirecto(jsonContentStr);
                                ret.total_price = cost.precio * 100;

                                foreach (var sku_pro in products)
                                {
                                    promocion_pagos_adelantado promocion_Pagos_ = context.promocion_pagos_adelantado.Where(x => x.sku_promocion == sku_pro.sku && x.id_cliente == id_cliente && x.activo == true).FirstOrDefault();

                                    if (promocion_Pagos_ != null)
                                    {
                                        meses_promocion = Convert.ToInt32(promocion_Pagos_.cantidad_meses_pago);
                                        break;
                                    }
                                }

                                if (meses_promocion > 1)
                                {
                                    ret.total_price = ret.total_price * meses_promocion;
                                }

                                if (ret.total_price > 0)
                                {
                                    retorns.Add(ret);
                                }
                            }
                            else
                            {
                                ret.total_price = costo_despacho;

                                if (ret.total_price >= 0)
                                {
                                    retorns.Add(ret);
                                }
                            }
                        }
                        else
                        {
                            id_cliente = conexion.Get_codCliente(disEnvio.cliente);

                            cost = logica.costoDirecto(jsonContentStr);
                            ret.total_price = cost.precio * 100;

                            foreach (var sku_pro in products)
                            {
                                promocion_pagos_adelantado promocion_Pagos_ = context.promocion_pagos_adelantado.Where(x => x.sku_promocion == sku_pro.sku && x.id_cliente == id_cliente && x.activo == true).FirstOrDefault();

                                if (promocion_Pagos_ != null)
                                {
                                    meses_promocion = Convert.ToInt32(promocion_Pagos_.cantidad_meses_pago);
                                    break;
                                }
                            }

                            if (meses_promocion > 1)
                            {
                                ret.total_price = ret.total_price * meses_promocion;
                            }

                            if (ret.total_price > 0)
                            {
                                retorns.Add(ret);
                            }
                        }
                    }
                }
            }
            catch(Exception e)
            {
                retorns.Add(new Retorno(e.ToString()));
            }
           
            if (SKUValidado)
            {
                return Get(retorns, Autorizado,token, cliente, respuestaSKU, validarComuna,comuna);
            }
            else
            {
                List<Retorno> retornoNull = new List<Retorno>();
                return Get(retornoNull, Autorizado, token, cliente, respuestaSKU,validarComuna,comuna);
            }
        }

        [System.Web.Mvc.HttpGet]
        //GET api/values
        public IHttpActionResult Get(List<Retorno> retorns, bool autorizado, string token, string cliente, RespuestaSKU respuestasku, Boolean validarComuna, string comuna)
        {
            if (autorizado)
            {
                if (validarComuna)
                {
                    if (retorns.Count == 0)
                    {
                        conexion.guardarLog("Problema con uno o muchos SKU no encontrados en nuestra base.", "salida", cliente, "Procesar Cotizacion");
                        var rates = new { rates = "Problema con uno o muchos SKU no encontrados en nuestra base.", respuestasku.sku_faltantes, status = -2 };
                        return Ok(rates);
                    }
                    else
                    {
                        try
                        {
                            var rates = new { rates = retorns };//, status = System.Net.HttpStatusCode.OK };
                            return Ok(rates);

                        }
                        catch (Exception ex)
                        {
                            var rates = new { rates = ex.ToString(), status = System.Net.HttpStatusCode.InternalServerError };
                            return NotFound();
                        }
                    }
                }
                else
                {
                    conexion.guardarLog("Error al ingresar comuna, favor verifique que este escrita correctamente.", "salida", cliente, "Procesar Cotizacion");
                    var rates = new { rates = "Error al ingresar comuna, favor verifique que este escrita correctamente.",Comuna_ingresada = comuna, status = -3 };
                    return Ok(rates);
                }
                
            }
            else
            {
               int estadoToken = conexion.validarEstadoToken(token, cliente);
                token_key respuesta_token = new token_key();
                respuesta_token.token = token;
                respuesta_token.cliente = cliente;
                
                if (estadoToken == 0)
                {
                    conexion.guardarLog("Token fue caducado", "salida", cliente, "Procesar Cotizacion");
                    respuesta_token.respuesta = "Token fue caducado";
                    var rates = new { rates = respuesta_token, status = System.Net.HttpStatusCode.NonAuthoritativeInformation };
                    return Ok(rates);
                }else if (estadoToken == 2)
                {
                    conexion.guardarLog("Token no existe", "salida", cliente, "Procesar Cotizacion");
                    respuesta_token.respuesta = "Token no existe";
                    var rates = new { rates = respuesta_token, status = System.Net.HttpStatusCode.NonAuthoritativeInformation };
                    return Ok(rates);
                }
                else
                {
                    conexion.guardarLog("Token no válido", "salida", cliente, "Procesar Cotizacion");
                    var rates = new { rates = "Token no válido", status = System.Net.HttpStatusCode.NonAuthoritativeInformation };
                    return Ok(rates);
                }

                
            }
            
        }

        private JsonResult Json(object res, JsonRequestBehavior allowGet)
        {
            throw new NotImplementedException();
        }

        private string getTagsByID(string usuario, string contrasenia, string url_tienda, string lst)
        {
            string result = "";
            try
            {
                lst = lst.TrimEnd(',');
                var url = "https://" + usuario + ":" + contrasenia + "@" + url_tienda + "/admin/api/2022-04/products.json?ids=" + lst;
                WebClient client = new WebClient();
                string userName = usuario;
                string passWord = contrasenia;
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                string credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes(userName + ":" + passWord));
                client.Headers[HttpRequestHeader.Authorization] = "Basic " + credentials;
                result = client.DownloadString(url);
            }
            catch (Exception ex)
            {
                string err = ex.ToString();
            }
            return result;
        }

    }
}
