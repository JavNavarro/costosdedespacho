﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.App_Code.DTO.ShipexAPI
{
    public class DespachoAPI
    {
        public string coddespacho { get; set; }
        public string url_tracking { get; set; }
        public string respuesta { get; set; }
    }
}