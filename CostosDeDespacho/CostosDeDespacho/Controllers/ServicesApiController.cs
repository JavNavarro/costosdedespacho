﻿using CostosDeDespacho.App_Code.BE;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;

namespace CostosDeDespacho.Controllers
{
    public class ServicesApiController : ApiController
    {
        BDconn conexion = new BDconn();
        private string myQueueItem;

        [System.Web.Mvc.HttpPost]
        public async Task<IHttpActionResult> Post()
        {
            
            return Get();
        }

        // GET api/<controller>/5
        [System.Web.Mvc.HttpGet]
        //GET api/values
        public IHttpActionResult Get()
        {
            Service service = new Service();
            service.service_code = "NRML";
            service.service_name = "Shipex normal";
            List<Service> arrServ = new List<Service>();

            Service service2 = new Service();
            service2.service_code = "NRML2";
            service2.service_name = "Shipex no normal";
            arrServ.Add(service);
            arrServ.Add(service2);


            var services = new { services = arrServ };//, status = System.Net.HttpStatusCode.OK };
            return Ok(services);
        }
    }
}