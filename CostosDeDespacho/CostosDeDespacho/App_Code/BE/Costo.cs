﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho
{
    public class Costo
    {
        public int precio { set; get; }
        public int estimadoHoras { set; get; }
    }
}