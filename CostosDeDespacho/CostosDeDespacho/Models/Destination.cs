﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.Models
{
    public class Destination
    {
        public string country { get; set; }
        public string city { get; set; }  // Aquí recibimos la comuna/ciudad
        public string postal_code { get; set; }
    }
}