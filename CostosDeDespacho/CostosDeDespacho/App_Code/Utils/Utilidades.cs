﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace CostosDeDespacho.App_Code.Utils
{
    public class Utilidades
    {
        // Función para quitar los acentos y caracteres especiales
        public string RemoveAccents(string str)
        {
            if (str == null)
                return null;

            string normalizedStr = str.Normalize(NormalizationForm.FormD);
            StringBuilder sb = new StringBuilder();

            foreach (char c in normalizedStr)
            {
                if (CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark)
                {
                    sb.Append(c);
                }
            }

            return sb.ToString().Normalize(NormalizationForm.FormC);
        }
    }
}