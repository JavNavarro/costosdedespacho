﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.App_Code.BE
{
    public class ProductoPromocionResponse
    {
        public string campaign { get; set; }
        public bool exist_in_campaign { get; set; }
    }
}