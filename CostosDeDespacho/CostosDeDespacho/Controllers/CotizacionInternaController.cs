﻿using CostosDeDespacho.App_Code.BE;
using CostosDeDespacho.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;

namespace CostosDeDespacho.Controllers
{
    public class CotizacionInternaController : ApiController
    {
        BDconn conexion = new BDconn();
        private string myQueueItem;
        db_shipexEntities context = new db_shipexEntities();
        [System.Web.Mvc.HttpPost]
        public async Task<IHttpActionResult> Post()
        {
            List<Retorno> retorns = new List<Retorno>();
            Boolean SKUValidado = true;
            try
            {

                var requestContent = Request.Content;
                var jsonContent = await requestContent.ReadAsStringAsync();

                List<DatosEnvio> data = new List<DatosEnvio>();
                DatosEnvio disEnvio = new DatosEnvio();
                List<Productos> products = new List<Productos>();
                ProductosPromocion productosPromocion = new ProductosPromocion();
                List<ProductosPromocion> LproductosPromocions = new List<ProductosPromocion>();
                int precio = 0;
                JObject json = JObject.Parse(jsonContent);
                Int32 promocion_valida = 0;
                costo_despacho_sku_comuna costo_Despacho_Sku = null;
                comuna _comuna = null;
                Int32 existe_comuna = 0;

                disEnvio.cliente = json["rate"]["origin"]["company_name"].ToString();

                int id_cliente = conexion.GetIdCliente(disEnvio.cliente);

                if (id_cliente == 3)
                {
                    disEnvio.comuna = json["rate"]["destination"]["address2"].ToString();
                }
                else
                {
                    disEnvio.comuna = json["rate"]["destination"]["city"].ToString();
                }

                try
                {
                    string jsonStringLog = jsonContent.ToString();
                    conexion.guardarLog(jsonStringLog, "jsonContent de entrada", disEnvio.cliente, "Cotizacion Interna");
                }
                catch (Exception ex)
                {

                }

                //String token = Request.Headers.Authorization.ToString();

                //List<DatosEnvio> data = new List<DatosEnvio>();
                //DatosEnvio disEnvio = new DatosEnvio();
                //List<Productos> products = new List<Productos>();
                //JObject json = JObject.Parse(jsonContent);

                //disEnvio.comuna = json["rate"]["destination"]["city"].ToString();
                //disEnvio.cliente = json["rate"]["origin"]["company_name"].ToString();

                DataSet dtCliente = conexion.getClienteCredenciales(disEnvio.cliente);

                string usuario = "";
                string contrasenia = "";
                string url_tienda = "";

                if (dtCliente.Tables.Count != 0)
                {
                    if (dtCliente.Tables[0].Rows.Count > 0)
                    {
                        usuario = dtCliente.Tables[0].Rows[0]["usuario"].ToString();
                        contrasenia = dtCliente.Tables[0].Rows[0]["contraseña"].ToString();
                        url_tienda = dtCliente.Tables[0].Rows[0]["url_tienda"].ToString();
                    }
                }



                List<string> list_productID = new List<string>();
                var lst = "";

                foreach (JObject item in json["rate"]["items"])
                {

                    Productos product = new Productos();
                    product.sku = item["sku"].ToString();
                    product.cantidad = Int32.Parse(item["quantity"].ToString());
                    product.precio = Convert.ToInt32(item["price"]) * Int32.Parse(item["quantity"].ToString());
                    product.price = Convert.ToInt32(item["price"]) / 100;
                    product.product_id = item["product_id"] != null && !string.IsNullOrEmpty(item["product_id"].ToString()) ? Convert.ToInt64(item["product_id"]) : 0;
                    product.variant_id = item["variant_id"] != null && !string.IsNullOrEmpty(item["variant_id"].ToString()) ? Convert.ToInt64(item["variant_id"]) : 0;
                    lst = Convert.ToString(product.product_id + "," + lst);
                    //precio = precio + product.precio / 100;
                    products.Add(product);
                }
                disEnvio.products = products;
                productosPromocion.products = products;

                Boolean validacionEnCaliente;

                data.Add(disEnvio);
                var jsonContentStr = JsonConvert.SerializeObject(data);

                App_Code.BL.CostoLogic logica = new App_Code.BL.CostoLogic();
                Costo cost = new Costo();
                Retorno ret = new Retorno();

                DataSet dsEntrega = conexion.GetDatosRespuesta(disEnvio.cliente, disEnvio.comuna);

                string entrega = "";
                string descripcion = "";
                DateTime dMin = DateTime.Now.AddDays(1);
                DateTime dMax = DateTime.Now.AddDays(3);

                if (disEnvio.comuna == "")
                {
                    ret.service_name = "Formulario incompleto";
                    ret.description = "FAVOR DE COMPLETAR LA DIRECCIÓN, indicar la comuna para calcular el costo de envío a su domicilio";
                    ret.currency = "CLP";
                    ret.service_code = "NRML";
                    ret.total_price = 1000 * 100;
                    retorns.Add(ret);
                    return Get(retorns);
                }

                existe_comuna = conexion.buscarComuna(disEnvio.comuna);

                if (disEnvio.cliente == "Farmex")
                {
                    if (existe_comuna == 0)
                    {
                        ret.service_name = "Error Costo De Envío";
                        ret.description = "ESTE PEDIDO NO SE PUEDE ENVÍAR A LA COMUNA QUE INDICASTE. Revisa la comuna en el paso anterior para garantizar el correcto costo de envío";
                        ret.currency = "CLP";
                        ret.service_code = "NRML";
                        //ret.min_delivery_date = dMin.ToUniversalTime().ToString("u").Replace("Z", "").Replace("/", "-") + " " + ("-04:00");
                        //ret.max_delivery_date = dMax.ToUniversalTime().ToString("u").Replace("Z", "").Replace("/", "-") + " " + ("-04:00");
                        ret.total_price = 49990 * 100;
                        retorns.Add(ret);
                        return Get(retorns);
                    }
                }

                if (dsEntrega.Tables.Count != 0)
                {
                    if (dsEntrega.Tables[0].Rows.Count > 0)
                    {
                        entrega = dsEntrega.Tables[0].Rows[0]["entrega"].ToString();
                        descripcion = dsEntrega.Tables[0].Rows[0]["descripcion"].ToString();
                        ret.service_name = entrega;
                        ret.description = descripcion;
                        ret.currency = "CLP";
                        ret.service_code = "NRML";
                    }
                    else
                    {
                        ret.service_name = "Envío Shipex";
                        ret.description = "Los pedidos realizados después de las 2 p.m serán procesados como realizados el día hábil siguiente";
                        ret.currency = "CLP";
                        ret.service_code = "NRML";
                        ret.min_delivery_date = dMin.ToUniversalTime().ToString("u").Replace("Z", "").Replace("/", "-") + " " + ("-04:00");
                        ret.max_delivery_date = dMax.ToUniversalTime().ToString("u").Replace("Z", "").Replace("/", "-") + " " + ("-04:00");
                    }
                }
                else
                {
                    ret.service_name = "Envío Shipex";
                    ret.description = "Los pedidos realizados después de las 2 p.m serán procesados como realizados el día hábil siguiente";
                    ret.currency = "CLP";
                    ret.service_code = "NRML";
                    ret.min_delivery_date = dMin.ToUniversalTime().ToString("u").Replace("Z", "").Replace("/", "-") + " " + ("-04:00");
                    ret.max_delivery_date = dMax.ToUniversalTime().ToString("u").Replace("Z", "").Replace("/", "-") + " " + ("-04:00");
                }

                foreach (Productos a in products)
                {
                    validacionEnCaliente = conexion.validarSku(a.sku, disEnvio.cliente);
                    if (!validacionEnCaliente)
                    {
                        conexion.skuNoEncontrado(a.sku, disEnvio.cliente);
                        //SKUValidado = validacionEnCaliente;
                    }
                }

                foreach (var item_prod in products)
                {
                    precio = precio + item_prod.precio / 100;
                }

                string obtiene_tagsDB = String.Empty;
                string vendor_campania = String.Empty;

                if (disEnvio.cliente == "Farmex" && json["rate"]["items"][0]["vendor"].ToString().Contains("-"))
                {
                    vendor_campania = json["rate"]["items"][0]["vendor"].ToString().Split('-')[1];

                    obtiene_tagsDB = conexion.verificarExisteTagsCliente(disEnvio.cliente, disEnvio.comuna, vendor_campania);

                    if (obtiene_tagsDB == "")
                    {
                        vendor_campania = "";
                    }
                }
                else
                {
                    obtiene_tagsDB = conexion.verificarExisteTagsCliente(disEnvio.cliente, disEnvio.comuna, "");
                }

                Int32 costo_despacho = 999;

                //VALIDACION POR SKU EN PEDIDO
                foreach (var item_prod in products)
                {
                    var lResultados = (from tc in context.tienda_cliente
                                       join sp in context.sku_promocion_despacho on tc.idCliente equals sp.id_cliente
                                       join pd in context.promociones_despacho on tc.idCliente equals pd.id_cliente
                                       join co in context.comuna on pd.id_comuna equals co.comuna_id
                                       where tc.nom_Tienda == disEnvio.cliente && sp.sku_promocion == item_prod.sku
                                       && co.comuna_nombre.Replace("ñ","n").Replace("á", "a").Replace("é", "e").Replace("í", "i").Replace("ó", "o").Replace("ú", "u") == disEnvio.comuna.Replace("ñ", "n").Replace("á", "a").Replace("é", "e").Replace("í", "i").Replace("ó", "o").Replace("ú", "u")
                                       select new
                                       {

                                       }).ToList();

                    if (lResultados.Count > 0)
                    {
                        promocion_valida = 1;
                        break;
                    }
                }

                foreach (var item_prod in products)
                {
                    costo_Despacho_Sku = context.costo_despacho_sku_comuna.FirstOrDefault(x => x.sku == item_prod.sku);

                    if (costo_Despacho_Sku != null)
                    {
                        //string comuna_reset = disEnvio.comuna.Replace('á', 'a').Replace('é', 'e').Replace('í', 'i').Replace('ó', 'o').Replace('ú', 'u').Replace('ñ', 'n').ToLower();

                        int id_comuna = existe_comuna;

                        //_comuna = context.comuna.FirstOrDefault(x => x.comuna_nombre == comuna_reset);

                        //if (_comuna == null)
                        //{
                        //    Diccionario_comunas _diccionario_comuna = context.Diccionario_comunas.FirstOrDefault(dc => dc.nombreComuna == comuna_reset);

                        //    if (_diccionario_comuna != null)
                        //    {
                        //        _comuna = context.comuna.FirstOrDefault(x => x.comuna_nombre == _diccionario_comuna.nombreReal.Replace('á', 'a').Replace('é', 'e').Replace('í', 'i').Replace('ó', 'o').Replace('ú', 'u').Replace('ñ', 'n').ToLower());

                        //        if (_comuna != null)
                        //        {
                        //            id_comuna = _comuna.comuna_id;
                        //        }
                        //    }
                        //    else
                        //    {
                        //        _comuna = context.comuna.FirstOrDefault(x => x.comuna_nombre == disEnvio.comuna);

                        //        if (_comuna != null)
                        //        {
                        //            id_comuna = _comuna.comuna_id;
                        //        }
                        //      }
                        //}
                        //else
                        //{
                        //    id_comuna = _comuna.comuna_id;
                        //}

                        int id_cliente_envio = 0;
                        tienda_cliente _tienda_cliente = context.tienda_cliente.FirstOrDefault(x => x.nom_Tienda == disEnvio.cliente);

                        if (_tienda_cliente != null)
                        {
                            id_cliente_envio = _tienda_cliente.idCliente;
                        }

                        costo_Despacho_Sku = context.costo_despacho_sku_comuna.FirstOrDefault(x => x.id_cliente == id_cliente_envio && x.id_comuna == id_comuna && x.sku == item_prod.sku);

                        if (costo_Despacho_Sku != null)
                        {
                            if (costo_Despacho_Sku.activo == true)
                            {
                                ret.service_name = "Servicio no disponible de envío para la comuna ingresada";
                                ret.description = "ESTE PEDIDO NO SE PUEDE ENVÍAR A LA COMUNA QUE INDICASTE";
                                ret.total_price = 99999 * 100;
                                ret.currency = "CLP";
                                ret.service_code = "NRML";
                            }
                            else
                            {
                                ret.service_name = ret.service_name.Replace("Envío Shipex", "Envío Refrigerado");
                                ret.total_price = Convert.ToInt32(costo_Despacho_Sku.precio_despacho) * 100;
                            }

                            if (ret.total_price >= 0)
                            {
                                retorns.Add(ret);
                                return Get(retorns);
                            }
                        }
                    }
                }

                ////CREAR CONFIGURACION CHILE PARCELS

                //if (disEnvio.cliente == "Farmex")
                //{
                //    Int32 existe_comuna_ChileParcels = conexion.buscarComunaChileParcels(disEnvio.comuna);

                //    if (existe_comuna_ChileParcels > 0)
                //    {
                //        Retorno ret_chileparcels = new Retorno();
                //        App_Code.BL.CostoLogic logicaChileParcels = new App_Code.BL.CostoLogic();
                //        Costo costChileParcels = logicaChileParcels.costoDirectoChileParcels(jsonContentStr);


                //        if (costChileParcels.precio > 0)
                //        {
                //            ret_chileparcels.service_name = "Shipex - Entrega mismo día";
                //            ret_chileparcels.service_code = "FAST";
                //            ret_chileparcels.total_price = costChileParcels.precio * 100; // En centavos (10.00 USD)
                //            ret_chileparcels.currency = "CLP";
                //            ret_chileparcels.description = "Los pedidos realizados después de las 11 a.m serán procesados como realizados el día hábil siguiente";

                //            retorns.Add(ret_chileparcels);
                //        }
                //    }
                //}


                if (promocion_valida == 1)
                {
                    ret.total_price = 0;

                    if (ret.total_price >= 0)
                    {
                        retorns.Add(ret);
                    }
                }
                else
                {
                    if (obtiene_tagsDB != "2")
                    {
                        if (vendor_campania == "")
                        {
                            costo_despacho = conexion.despachoGratis(disEnvio.cliente, disEnvio.comuna, precio, "", "");
                        }
                        else
                        {
                            productosPromocion.campaign = vendor_campania;

                            ProductoPromocionResponse response = conexion.ObtieneDespachoGratisPromocionCampana(productosPromocion);

                            if (response.exist_in_campaign == true)
                            {
                                costo_despacho = conexion.despachoGratis(disEnvio.cliente, disEnvio.comuna, precio, productosPromocion.campaign, "Farmex");
                            }
                            else
                            {
                                costo_despacho = conexion.despachoGratis(disEnvio.cliente, disEnvio.comuna, precio, "", "");
                            }
                        }


                        if (costo_despacho == 1)
                        {
                            cost = logica.costoDirecto(jsonContentStr);
                            ret.total_price = cost.precio * 100;

                            if (ret.total_price > 0)
                            {
                                retorns.Add(ret);
                            }
                        }
                        else
                        {
                            ret.total_price = costo_despacho;

                            if (ret.total_price >= 0)
                            {
                                retorns.Add(ret);
                            }
                        }
                    }
                    else
                    {
                        cost = logica.costoDirecto(jsonContentStr);
                        ret.total_price = cost.precio * 100;

                        if (ret.total_price > 0)
                        {
                            retorns.Add(ret);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                retorns.Add(new Retorno(e.ToString()));
            }

            if (SKUValidado)
            {
                return Get(retorns);
            }
            else
            {
                List<Retorno> retornoNull = new List<Retorno>();
                return Get(retornoNull);
            }
        }



        [System.Web.Mvc.HttpGet]
        //GET api/values
        public IHttpActionResult Get(List<Retorno> retorns)
        {
            if (retorns == null)
            {
                conexion.guardarLog("retorns null", "retorns null", "-", "Cotizacion Interna");
                var rates = new { rates = "Error : entrada nula", status = 999 };
                return Ok(rates);
            }
            if (retorns.Count == 0)
            {
                try
                {
                    conexion.guardarLog("Problema con uno o muchos SKU no encontrados en nuestra base.", "salida", "-", "Cotizacion Interna");
                } 
                catch (Exception ex)
                {

                }
                var rates = new { rates = "Problema con uno o muchos SKU no encontrados en nuestra base.", status = 999 };
                return Ok(rates);
            }
            else
            {
                try
                {
                    try
                    {
                        conexion.guardarLog(retorns[0].total_price.ToString(), "precio salida", "-", "Cotizacion Interna");
                    }
                    catch (Exception ex)
                    {

                    }
                    var rates = new { rates = retorns };//, status = System.Net.HttpStatusCode.OK };
                    return Ok(rates);

                }
                catch (Exception ex)
                {
                    var rates = new { rates = ex.ToString(), status = System.Net.HttpStatusCode.InternalServerError };
                    return NotFound();
                }
            }


        }

        private JsonResult Json(object res, JsonRequestBehavior allowGet)
        {
            throw new NotImplementedException();
        }

        private string getTagsByID(string usuario, string contrasenia, string url_tienda, string lst)
        {
            string result = "";
            try
            {
                lst = lst.TrimEnd(',');
                var url = "https://" + usuario + ":" + contrasenia + "@" + url_tienda + "/admin/api/2022-04/products.json?ids=" + lst;
                WebClient client = new WebClient();
                string userName = usuario;
                string passWord = contrasenia;
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                string credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes(userName + ":" + passWord));
                client.Headers[HttpRequestHeader.Authorization] = "Basic " + credentials;
                result = client.DownloadString(url);
            }
            catch (Exception ex)
            {
                string err = ex.ToString();
            }
            return result;
        }

    }
}