﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.App_Code.DTO.CalculadoraCostosDespacho
{
    public class CalculadoraCostoDespachoDTO
    {
        public string comuna { get; set; }
        public double peso_efectivo { get; set; }
        public int id_cliente { get; set; }
    }
}