﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.App_Code.BE
{
    public class ProductosPromocion
    {
        public string campaign { get; set; }
        public List<Productos> products { get; set; }
    }
}