﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;
using CostosDeDespacho.App_Code.BE;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CostosDeDespacho.Controllers
{
    public class RegistroController : ApiController
    {

        BDconn conBD = new BDconn();


        [System.Web.Mvc.HttpPost]
        public async Task<IHttpActionResult> Post()
        {
            RespuestaSKU respuestaSKU = new RespuestaSKU();
            bool Autorizado = false;
            var requestContent = Request.Content;
            var jsonContent = await requestContent.ReadAsStringAsync();
            Boolean SKUValidado = true;
            String token = Request.Headers.Authorization.ToString();
            //List<DatosEnvio> data = new List<DatosEnvio>();
            //DatosEnvio disEnvio = new DatosEnvio();
            //List<Productos> products = new List<Productos>();
            JObject json = JObject.Parse(jsonContent);
            //disEnvio.comuna = json["rate"]["destination"]["city"].ToString();
            //disEnvio.cliente = json["rate"]["origin"]["company_name"].ToString();
            //foreach (JObject item in json["rate"]["items"])
            int status;
            string comuna, direccion, nombreDestinatario, cliente, phone, email, order_id, comentario_envio;
            string NomCli1 = "";
            string NomCli2 = "";
            string ApatCli = "";
            string AmatCli = "";
            comuna = json["rate"]["destination"]["city"].ToString();
            cliente = json["rate"]["origin"]["company_name"].ToString();
            nombreDestinatario = json["rate"]["destination"]["name"].ToString();

            try
            {
                string jsonStringLog = jsonContent.ToString();
                conBD.guardarLog(jsonStringLog, "jsonContent de entrada", cliente, "Registro Token");
            }
            catch (Exception ex)
            {

            }

            int cant_guias = 0;

            while (nombreDestinatario.Contains("  "))
            {
                nombreDestinatario = nombreDestinatario.Replace("  ", " ");
            }

            string[] nombre = nombreDestinatario.Split(' ');

            if (nombre.Length > 3)
            {
                NomCli1 = nombre[0];
                NomCli2 = nombre[1];
                ApatCli = nombre[2];
                AmatCli = nombre[3];
            }
            else if (nombre.Length == 3)
            {
                NomCli1 = nombre[0];
                NomCli2 = "";
                ApatCli = nombre[1];
                AmatCli = nombre[2];
            }
            else if (nombre.Length == 2)
            {
                NomCli1 = nombre[0];
                NomCli2 = "";
                ApatCli = nombre[1];
                AmatCli = "";
            }
            else if (nombre.Length == 1)
            {
                NomCli1 = nombre[0];
                NomCli2 = "";
                ApatCli = "";
                AmatCli = "";
            }

            direccion = json["rate"]["destination"]["address1"].ToString();
            phone = json["rate"]["destination"]["phone"].ToString();
            email = json["rate"]["destination"]["email"].ToString();
            order_id = json["rate"]["origin"]["order_id"].ToString();
            status = Int32.Parse(json["rate"]["origin"]["Status"].ToString());
            comentario_envio = json["rate"]["destination"]["address2"].ToString() == null || json["rate"]["destination"]["address2"].ToString() == "" ? "-" : json["rate"]["destination"]["address2"].ToString();

            Int32 tokenValidos = conBD.validarToken(token, json["rate"]["origin"]["company_name"].ToString());
            status = Int32.Parse(json["rate"]["origin"]["Status"].ToString());
            string codDespacho = "";

            if (tokenValidos == 0)
            {
                tokenValidos = tokenValidos + conBD.validarTokenPublico(token);
            }

            if (tokenValidos > 0)
            {
                Autorizado = true;
                List<Productos> productos = new List<Productos>();

                foreach (JObject item in json["rate"]["items"])
                {
                    int cant_x_sku = Int32.Parse(item["quantity"].ToString());
                    Productos product = new Productos();
                    //for (int i = 1; i <= cant_x_sku; i++)
                    //{
                        product.sku = item["sku"].ToString();
                        product.cantidad = Int32.Parse(item["quantity"].ToString());
                        //cant_guias = cant_guias + 1;
                        cant_guias = 1;
                        productos.Add(product);
                    //}
                }

                string CodCodmuna = conBD.GetCodComuna(comuna);
                while (CodCodmuna.Length < 3)
                {
                    CodCodmuna = "0" + CodCodmuna;
                }

                Int32 cod_cliente = conBD.Get_codCliente(cliente);

                string Correlativo = conBD.Get_Correlativo(cod_cliente);
                while (Correlativo.Length < 5)
                {
                    Correlativo = "0" + Correlativo;
                }

                string prefijo = conBD.GetPrefijoCliente(cod_cliente);
                string RutCli = prefijo + CodCodmuna + Correlativo;

                string lreferencias = "";

                for (int i = 0; i < cant_guias; i++)
                {
                    lreferencias = "" + lreferencias + RutCli + "," + "";
                }
                lreferencias = "[" + lreferencias.TrimEnd(',') + "]";

                Boolean validarComuna = true;
                validarComuna = conBD.validarComuna(comuna);
                if (validarComuna)
                {
                    string codDesp = conBD.ObtenerCodDespachoPorOrder(cliente, order_id);
                    if (codDesp == null || codDesp == "")
                    {
                        codDespacho = conBD.registrarDespacho(comuna, direccion, NomCli1, NomCli2, ApatCli, AmatCli, productos, cliente, phone, "-", email, order_id, status, lreferencias, comentario_envio);

                        if (codDespacho != "")
                        {
                            int contRef = 1;

                            for (int i = 1; i <= cant_guias; i++)
                            {
                                foreach (var itemDetalle in productos)
                                {
                                    string guia = RutCli + contRef;
                                    string url_etiqueta = RutCli + "_" + contRef + ".png";
                                    conBD.AlimentarDetalles(Int64.Parse(codDespacho), codDespacho, order_id, guia.Trim(), "-", DateTime.Now, 2589, 0, 36, Int64.Parse(codDespacho), "  ");
                                    conBD.insertarRegistro2(Int64.Parse(guia.Trim()), Int64.Parse(codDespacho), url_etiqueta, itemDetalle.sku, cliente, order_id);
                                    conBD.insertarOrdenWarehouse(cod_cliente, Int64.Parse(guia.Trim()), Int64.Parse(codDespacho), itemDetalle.sku, itemDetalle.cantidad, NomCli1, NomCli2, ApatCli, AmatCli, comuna);

                                    contRef = contRef + 1;
                                }
                                i = cant_guias;
                            }
                        }
                    }
                    else
                    {
                        codDespacho = "";
                    }
                }
                else    
                {
                    codDespacho = "ErrorComuna";
                }

                //List<string> skuNoValidos = new List<string>();
                //Boolean validacionEnCaliente;
                //foreach (Productos a in productos)
                //{
                //    validacionEnCaliente = conBD.validarSku(a.sku, cliente);
                //    if (!validacionEnCaliente)
                //    {
                //        SKUValidado = validacionEnCaliente;

                //        skuNoValidos.Add(a.sku);
                //    }

                //}
                //respuestaSKU.cliente = cliente;
                //respuestaSKU.sku_faltantes = skuNoValidos;
            }
            if (codDespacho == "")
            {
                return Get(false, "", Autorizado, SKUValidado, token, cliente, respuestaSKU, comuna);
            }
            else
            {
                return Get(true, codDespacho, Autorizado, SKUValidado, token, cliente, respuestaSKU, comuna);
            }
        }

        public static int ubicarIndice(string[] arrayBusqueda, string valorBuscado)
        {
            int pos = Array.IndexOf(arrayBusqueda, valorBuscado);
            return pos;
        }

        [System.Web.Mvc.HttpGet]
        //GET api/values
        public IHttpActionResult Get(Boolean resul, string CodDespacho, bool Autorizacion, Boolean SKUValidado, string token, string cliente, RespuestaSKU respuestasku, string comuna)
        {

            List<RetornoDespacho> respuesta = new List<RetornoDespacho>();
            RetornoDespacho ret = new RetornoDespacho();
            if (Autorizacion)
            {
                if (CodDespacho == "ErrorComuna")
                {
                    conBD.guardarLog("Error en comuna no valida. favor verifique los datos.", "salida", cliente, "Registro Token");
                    var despacho = new { despacho = "Error en comuna no valida. favor verifique los datos.", comuna_ingresada = comuna, status = -3 };
                    return Ok(despacho);
                }
                else
                {
                    if (SKUValidado)
                    {
                        if (resul)
                        {
                            ret.coddespacho = CodDespacho;
                            conBD.guardarLog(CodDespacho, "salida", cliente, "Registro Token");
                            ret.url_tracking = "http://app.shipex.cl/shipexapp/TrackingCustomer.aspx?n=" + CodDespacho;
                            //ret.url_tracking = "codigo de despacho " + CodDespacho;
                            ret.respuesta = "Peticion de despacho registrada";
                        }
                        else
                        {
                            conBD.guardarLog("No se pudo registrar el pedido, favor comuniquese con logistica.", "salida", cliente, "Registro Token");
                            ret.coddespacho = "null";
                            ret.url_tracking = "null";
                            ret.respuesta = "No se pudo registrar el pedido, favor comuniquese con logistica.";
                        }

                        respuesta.Add(ret);
                        try
                        {
                            var despacho = new { despacho = respuesta };//, status =  System.Net.HttpStatusCode.OK };
                            return Ok(despacho);

                        }
                        catch (Exception ex)
                        {
                            var despacho = new { despacho = ex.ToString(), status = System.Net.HttpStatusCode.InternalServerError };
                            return NotFound();
                        }

                    }
                    else
                    {
                        conBD.guardarLog("Problema con uno o muchos SKU no encontrados en nuestra base.", "salida", cliente, "Registro Token");
                        var despacho = new { despacho = "Problema con uno o muchos SKU no encontrados en nuestra base.", respuestasku.sku_faltantes, status = -2 };
                        return Ok(despacho);
                    }


                }
            }
            else
            {
                int estadoToken = conBD.validarEstadoToken(token, cliente);
                token_key respuesta_token = new token_key();
                respuesta_token.token = token;
                respuesta_token.cliente = cliente;

                if (estadoToken == 0)
                {
                    respuesta_token.respuesta = "Token fue caducado";
                    var despacho = new { despacho = respuesta_token, status = System.Net.HttpStatusCode.NonAuthoritativeInformation };
                    return Ok(despacho);
                }
                else if (estadoToken == 2)
                {
                    respuesta_token.respuesta = "Token no existe";
                    var despacho = new { despacho = respuesta_token, status = System.Net.HttpStatusCode.NonAuthoritativeInformation };
                    return Ok(despacho);
                }
                else
                {
                    var despacho = new { despacho = "Token no válido", status = System.Net.HttpStatusCode.NonAuthoritativeInformation };
                    return Ok(despacho);
                }


            }

        }




        private JsonResult Json(object res, JsonRequestBehavior allowGet)
        {
            throw new NotImplementedException();
        }
    }
}
