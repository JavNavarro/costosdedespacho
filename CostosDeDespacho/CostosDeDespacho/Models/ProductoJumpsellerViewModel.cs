﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.Models
{
    public class ProductoJumpsellerViewModel
    {
        public int id { get; set; }
        public int? variant_id { get; set; }
        public string sku { get; set; }
        public string name { get; set; }
        public int qty { get; set; }
        public float price { get; set; }
        public float tax { get; set; }
        public float discount { get; set; }
        public float weight { get; set; }
        public string image { get; set; }
    }
}