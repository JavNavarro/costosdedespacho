﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.App_Code.DTO.CalculadoraCostosDespacho
{
    public class RegionDTO
    {
        public int id_region { get; set; }
        public string nombre_region { get; set; }
    }
}