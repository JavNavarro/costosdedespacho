﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;
using CostosDeDespacho.App_Code.BE;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CostosDeDespacho.Controllers
{
    public class EditarCamposController : ApiController
    {

        BDconn conBD = new BDconn();


        [System.Web.Mvc.HttpPost]
        public async Task<IHttpActionResult> Post()
        {
            string direccion, nombreDestinatario, cliente, phone, email, order_id;
            Int32 status= 999;
            Int64 CodDespacho=0;
            bool Autorizado = false;
            var requestContent = Request.Content;
            var jsonContent = await requestContent.ReadAsStringAsync();
            String token = Request.Headers.Authorization.ToString();
            JObject json = JObject.Parse(jsonContent);
            try
            {
                cliente = json["rate"]["origin"]["company_name"].ToString();
                nombreDestinatario = json["rate"]["destination"]["name"].ToString();
                direccion = json["rate"]["destination"]["address1"].ToString();
                phone = json["rate"]["destination"]["phone"].ToString();
                email = json["rate"]["destination"]["email"].ToString();
                order_id = json["rate"]["origin"]["order_id"].ToString();
                status = Int32.Parse(json["rate"]["origin"]["Status"].ToString());
            }
            catch(Exception ex)
            {
                cliente = "";
                nombreDestinatario = "";
                direccion = "";
                phone = "";
                email = "";
                order_id = "";
            }
           
            Int32 tokenValidos = conBD.validarToken(token, json["rate"]["origin"]["company_name"].ToString());
            Boolean editado = false;
            if (tokenValidos == 0)
            {
                tokenValidos = tokenValidos + conBD.validarTokenPublico(token);
            }
            if (tokenValidos > 0)
            {
                Autorizado = true;
                CodDespacho =  conBD.obtenerCodDespacho(cliente, order_id);

                editado = conBD.editarDespacho(nombreDestinatario,  cliente, phone, email, order_id, direccion, CodDespacho, status);
               

            }



                return Get(Autorizado, editado, nombreDestinatario,cliente,phone,email,order_id,direccion, CodDespacho,token);
         
            



        }



        [System.Web.Mvc.HttpGet]
        //GET api/values
        public IHttpActionResult Get( bool Autorizacion, Boolean editado, string nombre, string cliente, string phone, string email, string order_id,string direccion,Int64 codDespacho,string token)
        {

            List<RetornoDespacho> respuesta = new List<RetornoDespacho>();
            RetornoDespacho ret = new RetornoDespacho();
            if (Autorizacion)
            {
                if (!editado)
                {
                    var Accion = new { Accion = "Error al editar. favor verifique los datos.", status = System.Net.HttpStatusCode.NotFound };
                    return Ok(Accion);
                }
                else
                {

                    DatosDespacho despachoEditado = new DatosDespacho();
                    despachoEditado.nombreDestinatario = nombre;
                    despachoEditado.orderId = order_id;
                    despachoEditado.direccion = direccion;
                    despachoEditado.cliente = cliente;
                    despachoEditado.codigoDespacho = codDespacho;
                  
                        try
                        {
                            var despacho = new { despacho = despachoEditado , status =  System.Net.HttpStatusCode.OK };
                            return Ok(despacho);

                        }
                        catch (Exception ex)
                        {
                            var despacho = new { despacho = ex.ToString(), status = System.Net.HttpStatusCode.InternalServerError };
                            return NotFound();
                        }



                }
            }
            else
            {
                int estadoToken = conBD.validarEstadoToken(token, cliente);
                token_key respuesta_token = new token_key();
                respuesta_token.token = token;
                respuesta_token.cliente = cliente;

                if (estadoToken == 0)
                {
                    respuesta_token.respuesta = "Token fue caducado";
                    var Accion = new { Accion = respuesta_token, status = System.Net.HttpStatusCode.NonAuthoritativeInformation };
                    return Ok(Accion);
                }
                else if (estadoToken == 2)
                {
                    respuesta_token.respuesta = "Token no existe";
                    var Accion = new { Accion = respuesta_token, status = System.Net.HttpStatusCode.NonAuthoritativeInformation };
                    return Ok(Accion);
                }
                else
                {
                    var Accion = new { Accion = "Token no válido", status = System.Net.HttpStatusCode.NonAuthoritativeInformation };
                    return Ok(Accion);
                }


            }

        }






        private JsonResult Json(object res, JsonRequestBehavior allowGet)
        {
            throw new NotImplementedException();
        }
    }
}
