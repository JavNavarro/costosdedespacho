﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.App_Code.DTO
{
    public class JumpsellerAPI
    {
        //      {
        //  "access_token":"80de978742714f8747036188cf6bab538e976adf72144d545e185ae444bc473d",
        //  "token_type":"bearer",
        //  "expires_in":3600,
        //  "refresh_token":"86454df3e89cd34bd006ac68a76f6f2af884822ed68ce417777a4cffaae64b61",
        //  "created_at":1502473092
        //}
        /// <summary>
        /// An access token that you can use requesting, for example, the Jumpseller API.
        /// </summary>
        public string access_token { get; set; }
        /// <summary>
        /// The way the above access token should be used. Always is "bearer".
        /// </summary>
        public string token_type { get; set; }
        /// <summary>
        /// The period of time in seconds until your access token expires.
        /// </summary>
        public int expires_in { get; set; }
        /// <summary>
        /// This a token used to refresh/replace your access token when it is expired.
        /// </summary>
        public string refresh_token { get; set; }
        /// <summary>
        /// The timestamp format of your
        /// </summary>
        public int created_at { get; set; }

        public int idStore { get; set; }
    }
}