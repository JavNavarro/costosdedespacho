﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.Models
{
    public class ClienteJumpsellerViewModel
    {
        public int id { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string phone_prefix { get; set; }
        public string ip { get; set; }
    }
}