﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.App_Code.CONSTANTES
{
    public static class JumpsellerOrders
    {
        //Ver documentacion en https://jumpseller.com/support/api/#tag/Orders/paths/~1orders~1status~1{status}.json/get
        public const string abandonado = "Abandoned";
        public const string cancelado = "Canceled";
        public const string pendientePago = "Pending Payment";
        public const string pagado = "Paid";
    }
}