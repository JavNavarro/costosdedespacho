﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.App_Code.DTO.Jumpseller
{
    public class ShippingMethod
    {
        public int id { get; set; }
        public string type { get; set; }
        public string name { get; set; }
        public string callback_url { get; set; }
        public string fetch_services_url { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string postal { get; set; }
    }
}

