﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.App_Code.DTO.ShipexAPI
{
    public class Item
    {
        public string name { get; set; }
        public string sku { get; set; }
        public int quantity { get; set; }
        public float grams { get; set; }
        public int price { get; set; }
        public string vendor { get; set; }
        public int requires_shipping { get; set; }
        public int taxable { get; set; }
        public string fullfillment_service { get; set; }
        public int product_id { get; set; }
        public int variant_id { get; set; }
    }
}