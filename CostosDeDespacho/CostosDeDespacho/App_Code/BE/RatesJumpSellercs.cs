﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.App_Code.BE
{
    public class RatesJumpSellercs
    {
        public string reference_id { set; get; }
        public List<Rate> rates { set; get; }
    }
}