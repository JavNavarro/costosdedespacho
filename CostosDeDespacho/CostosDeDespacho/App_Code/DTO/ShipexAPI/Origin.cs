﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.App_Code.DTO.ShipexAPI
{
    public class Origin
    {
        public string country { get; set; }
        public string postal_code { get; set; }
        public string province { get; set; }
        public string city { get; set; }
        public string name { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string MyProperty { get; set; }
        public string phone { get; set; }
        public string fax { get; set; }
        public string email { get; set; }
        public string address_type { get; set; }
        public string company_name { get; set; }
        public string order_id { get; set; }
        public int Status { get; set; }
    }
}