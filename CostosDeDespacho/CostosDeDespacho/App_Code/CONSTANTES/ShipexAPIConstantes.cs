﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.App_Code.CONSTANTES
{
    public static class ShipexAPIConstantes
    {
        public const int pedidoPagado = 0;
        public const int pedidoPreparado = 1;
        public const int pedidoCancelado = 2;
        public const int ingresado = 1;
        public const int confirmado = 2;
    }
}