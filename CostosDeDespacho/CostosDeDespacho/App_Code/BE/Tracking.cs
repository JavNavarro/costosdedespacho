﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.App_Code.BE
{
    public class Tracking
    {
        public DatosDespacho datos { set; get; }
        public List<EstadosDespacho> estados { set; get; }
    }
}