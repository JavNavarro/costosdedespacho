﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.App_Code.DTO.Jumpseller
{
    public class TokenInfo
    {
        public int resource_owner_id { get; set; }
        public List<string> scope { get; set; }
        public int expires_in { get; set; }
        public int created_at { get; set; }
        public string resource_owner_type { get; set; }
        public int store_id { get; set; }
    }
}