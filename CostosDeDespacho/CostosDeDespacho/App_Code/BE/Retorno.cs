﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.App_Code.BE
{
    public class Retorno
    {
        public Retorno() { }
        public Retorno(String error)
        {
            this.service_name = error;
        }

        public string service_name { get; set; }
        public string service_code { set; get; }
        public int total_price { set; get; }
        public string currency { set; get; }
        public string min_delivery_date { set; get; }
        public string max_delivery_date { set; get; }
        public string description { get; set; }

    }
}

