﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.App_Code.DTO.ShipexAPI
{
    /// <summary>
    /// Clase de envoltura para enviar a Shipex los datos del envio capturado por cualquier ecommerce
    /// Ver documentación en https://eshopex.atlassian.net/wiki/spaces/SHIP/pages/1499922433/Conectarse+API+registra+pedidos
    /// </summary>
    public class Rate
    {
        public Origin origin;
        public Destination destination;
        public List<Item> items { get; set; }
        public string currency { get; set; }
        public string locale { get; set; }
    }
}