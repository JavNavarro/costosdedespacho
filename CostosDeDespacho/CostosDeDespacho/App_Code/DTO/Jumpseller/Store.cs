﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.App_Code.DTO.Jumpseller
{
    public class Store
    {
        public string name { get; set; }
        public string code { get; set; }
        public string currency { get; set; }
        public string country { get; set; }
        public string email { get; set; }
    }
}