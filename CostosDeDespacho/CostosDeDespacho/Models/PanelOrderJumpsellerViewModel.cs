﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.Models
{
    public class PanelOrderJumpsellerViewModel
    {
        public string estadoEnvioShipex { get; set; }
        public long id { get; set; }
        public string estadoOrdenJumpseller { get; set; }
        public string nombreReceptor { get; set; }
        public string telefono { get; set; }
        public string email { get; set; }
        public string direccionEnvio { get; set; }
        public string comuna { get; set; }
        public string id_orden_jumpseller { get; set; }
        public string tracking { get; set; }
    }
}