﻿using CostosDeDespacho.App_Code.CONSTANTES;
using CostosDeDespacho.App_Code.DTO.ShipexAPI;
using CostosDeDespacho.App_Code.Utils;
using CostosDeDespacho.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;

namespace CostosDeDespacho.App_Code.BL
{
    public class ShipexAPI
    {
        public string crearDespachoShipex(string jsonstr, string token)
        {
            string urlRegistroDespacho = ConfigurationManager.AppSettings["api.shipex.registra.envio"];
            try
            {
                using (WebClient wc = new WebClient())
                {
                    wc.Headers[HttpRequestHeader.ContentType] = "application/json";
                    wc.Headers[HttpRequestHeader.Authorization] = token;
                    string jsonResult = wc.UploadString(new Uri(urlRegistroDespacho), "POST", jsonstr);
                    Log.Debug("Respuesta crear servicio Shipex " + jsonResult);
                    return jsonResult;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        /// <summary>
        /// Crea objeto para enviar a shipex api desde el view model de orden.
        /// </summary>
        /// <param name="objJumpsellerOrder">Object JSON Jumpseller</param>
        /// <returns>Rate</returns>
        public RateWrapper createObjectRateFromJumpseller(JUMPSELLER_ORDERS order, int status)
        {
            Rate objRate = new Rate();
            objRate.currency = "CLP";
            objRate.locale = "es";

            DTO.ShipexAPI.Destination objDestination = new DTO.ShipexAPI.Destination();
            objDestination.address1 = order.direccion_envio;
            objDestination.city = order.comuna;
            objDestination.country = "CL";
            objDestination.postal_code = "";
            objDestination.province = order.ciudad;
            objDestination.name = order.nombre_receptor;
            objDestination.phone = order.telefono;
            objDestination.email = order.email;
            objRate.destination = objDestination;
            objDestination.address2 = order.numero_calle;

            Origin objOrigin = new Origin();
            objOrigin.address1 = order.JUMPSELLER_STORES.url_tienda;
            objOrigin.city = "CL";
            objOrigin.company_name = order.JUMPSELLER_STORES.codigo_tienda;
            objOrigin.country = "CL";
            objOrigin.email = order.JUMPSELLER_STORES.email_tienda;
            objOrigin.name = order.JUMPSELLER_STORES.codigo_tienda;
            objOrigin.order_id = order.order_id.ToString();
            objOrigin.Status = status;
            objRate.origin = objOrigin;

            List<Item> lItems = new List<Item>();
            foreach (JUMPSELLER_PRODUCTS objProduct in order.JUMPSELLER_PRODUCTS)
            {
                Item objItem = new Item();
                objItem.name = objProduct.nombre_producto;
                objItem.sku = objProduct.sku;
                objItem.quantity = objProduct.cantidad != null ? Convert.ToInt32(objProduct.cantidad) : -1;
                objItem.grams = objProduct.peso_gramos != null ? (float)objProduct.peso_gramos : -1;
                objItem.product_id = (objProduct.id_producto != null && !objProduct.id_producto.Trim().Equals("")) ? Convert.ToInt32(objProduct.id_producto) : -1;
                objItem.variant_id = (objProduct.id_variante != null && !objProduct.id_variante.Trim().Equals("")) ? Convert.ToInt32(objProduct.id_variante) : -1;
                lItems.Add(objItem);
            }
            objRate.items = lItems;
            RateWrapper objRateWrapper = new RateWrapper();
            objRateWrapper.rate = objRate;
            return objRateWrapper;
        }
    }
}