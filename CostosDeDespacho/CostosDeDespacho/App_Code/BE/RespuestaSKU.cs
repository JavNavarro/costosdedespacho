﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.App_Code.BE
{
    public class RespuestaSKU
    {
        public string respuesta { set; get; }
        public List<String> sku_faltantes { set; get; }
        public string cliente { set; get; }
       
    }
}