﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.Models
{
    public class ShippingRate
    {
        public string service_name { get; set; }
        public string service_code { get; set; }
        public int total_price { get; set; } // En centavos
        public string currency { get; set; }
        public string min_delivery_date { set; get; }
        public string max_delivery_date { set; get; }
        public string description { get; set; }
    }
}