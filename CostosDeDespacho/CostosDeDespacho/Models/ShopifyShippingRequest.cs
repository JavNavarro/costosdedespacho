﻿using CostosDeDespacho.App_Code.DTO.ShipexAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.Models
{
    public class ShopifyShippingRequest
    {
        public RateShipping rate { get; set; }
    }

    public class RateShipping
    {
        public Destination destination { get; set; }
        public List<ShopifyItem> items { get; set; }
    }

}