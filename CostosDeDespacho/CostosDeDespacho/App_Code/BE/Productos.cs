﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho
{
    public class Productos
    {
        public string sku { set; get; }
        public int cantidad { set; get; }
        public int precio { get; set; }
        public Int64 product_id { get; set; }
        public Int64 variant_id { get; set; }
        public int price { get; set; } //casos para servicio de verificar promocion para costo despacho
        public decimal peso { get; set; }
    }
}