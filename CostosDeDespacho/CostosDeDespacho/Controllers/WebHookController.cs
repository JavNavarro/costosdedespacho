﻿using CostosDeDespacho.App_Code.BL;
using CostosDeDespacho.App_Code.CONSTANTES;
using CostosDeDespacho.App_Code.DTO.ShipexAPI;
using CostosDeDespacho.App_Code.Utils;
using CostosDeDespacho.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CostosDeDespacho.Controllers
{

    public class WebHookController : Controller
    {
        /// <summary>
        /// Procesamiento de orden de Webhook registrado para el evento pagado
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult processOrderJumpseller(OrderJumpsellerViewModel order, int estado = -1)
        {
            string storeCode = String.Empty;
            string @event = String.Empty;
            bool nuevaOrden = false;
            if (order != null)
            {
                try
                {
                    storeCode = Request.Headers.Get("Jumpseller-Store-Code");
                    Log.Debug("STORE CODE (CODIGO TIENDA): " + storeCode);

                    string jsonstr = JsonConvert.SerializeObject(order);
                    Log.Debug("WebhooksController.processOrderJumpseller");
                    Log.Debug("JSON ORDER PAID JUMPSELLER : " + jsonstr);
                    Log.Debug("Fin JSON ORDER PAID JUMPSELLER");

                    @event = Request.Headers.Get("Jumpseller-Event");
                    Log.Debug("-----------------------------");
                    Log.Debug("Nombre de enviardor : " + order.shipping_method_name.ToString());
                    Log.Debug("-----------------------------");
                    //Obtengo los nombres de shipping configurados en Shipex (Shipex normal, express, etc)
                    string nombreServicios = ConfigurationManager.AppSettings["app.jumpseller.nombre.servicios"].ToLower();
                    string[] aServicios = nombreServicios.ToLower().Split(',');
                    string enviador = order.shipping_method_name.ToLower();

                    if (storeCode != null && order.shipping_required && nombreServicios.Contains(enviador))
                    {
                        db_shipexEntities objQuery = new db_shipexEntities();
                        JUMPSELLER_STORES objStore = objQuery.JUMPSELLER_STORES.Where(x => x.codigo_tienda.Equals(storeCode)).FirstOrDefault();
                        if (objStore != null)
                        {
                            JUMPSELLER_ORDENES_WEBHOOK_LOG objLogWebhook = new JUMPSELLER_ORDENES_WEBHOOK_LOG();
                            objLogWebhook.jumpseller_store_id = objStore.id;
                            objLogWebhook.fecha_registro = DateTime.Now;
                            objLogWebhook.jsonrecepcionado = JsonConvert.SerializeObject(order);
                            objLogWebhook.nombre_evento = @event;

                            JUMPSELLER_ORDERS objOrders = objQuery.JUMPSELLER_ORDERS.Where(x => x.order_id == order.id && x.jumpseller_store_id == objStore.id).FirstOrDefault();
                            if (objOrders == null)
                            {
                                objOrders = new JUMPSELLER_ORDERS();
                                objOrders.fecha_registro = DateTime.Now;
                                nuevaOrden = true;
                                objOrders.id_api_estado = ShipexAPIConstantes.ingresado;
                                //Debo asociar la tienda en pedido no ingresado 
                                if (objOrders.JUMPSELLER_STORES == null)
                                {
                                    objOrders.JUMPSELLER_STORES = objQuery.JUMPSELLER_STORES.Where(x => x.codigo_tienda.Equals(storeCode)).FirstOrDefault();
                                }
                            }

                            objOrders.order_id = order.id;
                            objOrders.jumpseller_store_id = objStore.id;
                            objOrders.status = order.status;
                            objOrders.nombre_receptor = ((order.shipping_address.name != null ? order.shipping_address.name.Trim() : "") + " " + (order.shipping_address.surname != null ? order.shipping_address.surname.Trim() : "")).Trim();
                            objOrders.numero_calle = order.shipping_address.complement != null ? order.shipping_address.complement : "";
                            objOrders.region = order.shipping_address.region;
                            objOrders.email = order.customer.email;
                            objOrders.ciudad = order.shipping_address.city.Replace("á", "a").Replace("é", "e").Replace("í", "i").Replace("ó", "o").Replace("ú", "u").Replace("ñ", "n").Replace("Á", "A").Replace("É", "E").Replace("Í", "I").Replace("Ó", "O").Replace("Ú", "U").Replace("Ñ", "N").ToLower();
                            objOrders.nota = order.additional_information;
                            objOrders.latitude = order.shipping_address.latitude;
                            objOrders.longitud = order.shipping_address.longitude;
                            objOrders.fecha_creacion_orden_jumpseller = order.created_at;
                            objOrders.comuna = order.shipping_address.municipality.Replace("á", "a").Replace("é", "e").Replace("í", "i").Replace("ó", "o").Replace("ú", "u").Replace("ñ", "n").Replace("Á", "A").Replace("É", "E").Replace("Í", "I").Replace("Ó", "O").Replace("Ú", "U").Replace("Ñ", "N").ToLower();
                            objOrders.direccion_envio = order.shipping_address.address.Replace("á", "a").Replace("é", "e").Replace("í", "i").Replace("ó", "o").Replace("ú", "u").Replace("ñ", "n").Replace("Á", "A").Replace("É", "E").Replace("Í", "I").Replace("Ó", "O").Replace("Ú", "U").Replace("Ñ", "N").ToLower();
                            objOrders.telefono = order.customer.phone;
                            List<JUMPSELLER_PRODUCTS> lProductos = new List<JUMPSELLER_PRODUCTS>();
                            foreach (ProductoJumpsellerViewModel product in order.products)
                            {
                                JUMPSELLER_PRODUCTS objProduct = new JUMPSELLER_PRODUCTS();
                                objProduct.cantidad = product.qty;
                                objProduct.fecha_creacion = DateTime.Now;
                                objProduct.id_producto = product.id.ToString();
                                objProduct.id_variante = product.variant_id.ToString();
                                objProduct.order_id = objOrders.id;
                                objProduct.peso_gramos = product.weight;
                                objProduct.sku = product.sku;
                                objProduct.nombre_producto = product.name;
                                lProductos.Add(objProduct);
                            }
                            objOrders.JUMPSELLER_PRODUCTS = lProductos;

                            //Se envian datos a Shipex para generar el despacho
                            ShipexAPI objShipexAPI = new ShipexAPI();
                            int status = estado == -1 ? ShipexAPIConstantes.ingresado : estado;                     
                            CostosDeDespacho.App_Code.DTO.ShipexAPI.RateWrapper objRate = objShipexAPI.createObjectRateFromJumpseller(objOrders, status);
                            var body = JsonConvert.SerializeObject(objRate);
                            string tokenTienda = objStore.Api_Key.token;
                            string jsonResult = objShipexAPI.crearDespachoShipex(body, tokenTienda);

                            //Finalizo el registro asociado al log de entrada y salida
                            objLogWebhook.json_retorno_proceso = jsonResult;
                            objQuery.JUMPSELLER_ORDENES_WEBHOOK_LOG.Add(objLogWebhook);

                            DespachoAPIWrapper values = JsonConvert.DeserializeObject<DespachoAPIWrapper>(jsonResult);
                            /*
                             * Formato de retorno correcto:
                                {"despacho":[{"coddespacho":"201008652247","respuesta":"Peticion de despacho registrada","url_tracking":"http://app.shipex.cl/shipexapp/TrackingCustomer.aspx?n=201008652247"}]}
                             */
                            if (values.despacho != null && values.despacho.Count > 0)
                            {
                                DespachoAPI objDespachoItem = values.despacho[0];
                                if (objDespachoItem.coddespacho != null && !objDespachoItem.coddespacho.Equals(""))
                                {
                                    objOrders.url_tracking = objDespachoItem.url_tracking;
                                    objOrders.codigo_despacho_shipex = objDespachoItem.coddespacho;
                                    if (nuevaOrden)
                                    {
                                        objQuery.JUMPSELLER_ORDERS.Add(objOrders);
                                        objQuery.SaveChanges();
                                    }
                                    else
                                    {
                                        objQuery.Entry(objOrders).State = EntityState.Modified;
                                        objQuery.SaveChanges();
                                    }
                                }
                            }
                            return Json(new { result = values, status = System.Net.HttpStatusCode.OK }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                catch (Exception e)
                {
                    //Guardo el input para efectos de seguimiento posterior
                    db_shipexEntities objQuery = new db_shipexEntities();
                    JUMPSELLER_ORDENES_WEBHOOK_LOG objLogWebhook = new JUMPSELLER_ORDENES_WEBHOOK_LOG();
                    JUMPSELLER_STORES objStore = objQuery.JUMPSELLER_STORES.Where(x => x.codigo_tienda.Equals(storeCode)).FirstOrDefault();
                    objLogWebhook.jumpseller_store_id = objStore.id;
                    objLogWebhook.fecha_registro = DateTime.Now;
                    objLogWebhook.jsonrecepcionado = JsonConvert.SerializeObject(order);
                    objLogWebhook.nombre_evento = @event;
                    objQuery.JUMPSELLER_ORDENES_WEBHOOK_LOG.Add(objLogWebhook);
                    Log.Fatal("Error al procesar orden Webhook: " + order + " estado:" + estado.ToString());
                    var res = new { res = e.ToString(), status = System.Net.HttpStatusCode.InternalServerError };
                    return Json(res, JsonRequestBehavior.AllowGet);
                }
            }
            var resultado = new { res = "Orden procesada", status = System.Net.HttpStatusCode.OK };
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}