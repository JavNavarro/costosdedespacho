﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.Models
{
    public class ShopifyShippingResponse
    {
        public ShippingRate[] rates { get; set; }
    }
}