﻿using CostosDeDespacho.App_Code.BE;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;


namespace CostosDeDespacho.Controllers
{
    public class RatesJumpController : ApiController
    {
        BDconn conexion = new BDconn();
        private string myQueueItem;

        [System.Web.Mvc.HttpPost]
        public async Task<IHttpActionResult> Post()
        {
            var costo_despacho = await Get();
            return costo_despacho;
        }

        [System.Web.Mvc.HttpGet]
        public async Task<IHttpActionResult> Get()
        {
            List<App_Code.BE.Rate> rates1 = new List<App_Code.BE.Rate>();
            Boolean SKUValidado = true;
            try
            {
                var requestContent = Request.Content;
                var jsonContent = await requestContent.ReadAsStringAsync();
                List<DatosEnvio> data = new List<DatosEnvio>();
                DatosEnvio disEnvio = new DatosEnvio();
                List<Productos> products = new List<Productos>();
                JObject json = JObject.Parse(jsonContent);
                disEnvio.comuna = json["request"]["to"]["municipality_name"].ToString();
                disEnvio.cliente = json["request"]["from"]["name"].ToString();

                try
                {
                    string jsonStringLog = jsonContent.ToString();
                    conexion.guardarLog(jsonStringLog, "jsonContent de entrada", disEnvio.cliente, "RatesJump");
                }
                catch (Exception ex)
                {

                }
                foreach (JObject item in json["request"]["products"])
                {
                    Productos product = new Productos();
                    product.sku = item["sku"].ToString();
                    product.cantidad = Int32.Parse(item["quantity"].ToString());
                    products.Add(product);
                }
                disEnvio.products = products;

                Boolean validacionEnCaliente;
                foreach (Productos a in products)
                {
                    validacionEnCaliente = conexion.validarSku(a.sku, disEnvio.cliente);
                    if (!validacionEnCaliente)
                    {
                        SKUValidado = validacionEnCaliente;
                    }
                }
                data.Add(disEnvio);
                var jsonContentStr = JsonConvert.SerializeObject(data);
                App_Code.BL.CostoLogic logica = new App_Code.BL.CostoLogic();
                Costo cost = new Costo();
                cost = logica.costoDirecto(jsonContentStr);
                App_Code.BE.Rate ret = new App_Code.BE.Rate();
                ret.rate_id = "123";
                ret.service_code = "NRML";
                ret.service_name = "Shipex entrega";
                ret.total_price = (cost.precio).ToString();
                if (cost.precio > 0)
                {
                    rates1.Add(ret);
                }
            }
            catch (Exception e)
            {

            }
            if (!SKUValidado)
            {
                var rates = new { rates = "Problema con uno o muchos SKU no encontrados en nuestra base.", status = 999 };
                return Ok(rates);
            }
            else
            {
                if (rates1.Count == 0)
                {
                    try
                    {
                        conexion.guardarLog("Problema con uno o muchos SKU no encontrados en nuestra base.", "salida", "-", "RatesJump");
                    }
                    catch (Exception ex)
                    {

                    }
                    var rates = new { rates = "Despacho no disponible para su localidad.", status = 998 };
                    return Ok(rates);
                }
                else
                {
                    try
                    {
                        try
                        {
                            conexion.guardarLog(rates1[0].total_price.ToString(), "precio salida", "-", "RatesJump");
                        }
                        catch (Exception ex)
                        {

                        }
                        var rates = new { rates = rates1 };//, status = System.Net.HttpStatusCode.OK };
                        return Ok(rates);
                    }
                    catch (Exception ex)
                    {
                        var rates = new { rates = ex.ToString(), status = System.Net.HttpStatusCode.InternalServerError };
                        return NotFound();
                    }
                }
            }
        }

        private JsonResult Json(object res, JsonRequestBehavior allowGet)
        {
            throw new NotImplementedException();
        }
    }
}