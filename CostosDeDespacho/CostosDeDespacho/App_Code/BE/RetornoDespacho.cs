﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.App_Code.BE
{
    public class RetornoDespacho
    {
        public string coddespacho { set; get; }
        public string respuesta { set; get; }
        public string url_tracking { set; get; }
    }
}