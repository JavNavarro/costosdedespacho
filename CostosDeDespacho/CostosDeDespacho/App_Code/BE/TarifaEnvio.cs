﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.App_Code.BE
{
    public class TarifaEnvio
    {
        public decimal MinPeso { get; set; }
        public decimal MaxPeso { get; set; }
        public int Costo { get; set; } // Ahora es `int` en lugar de `decimal`

        public TarifaEnvio(decimal minPeso, decimal maxPeso, int costo)
        {
            this.MinPeso = minPeso;
            this.MaxPeso = maxPeso;
            this.Costo = costo;
        }
    }


}