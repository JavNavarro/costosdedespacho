﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;
using CostosDeDespacho.App_Code.BE;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CostosDeDespacho.Controllers
{
    public class TrackingController : ApiController
    {
        BDconn conBD = new BDconn();
        

        [System.Web.Mvc.HttpPost]
        public async Task<IHttpActionResult> Post()
        {
            Boolean Autorizado = false;
            var requestContent = Request.Content;
            var jsonContent = await requestContent.ReadAsStringAsync();
            string order_id,cliente;
            String token = Request.Headers.Authorization.ToString();
            
            JObject json = JObject.Parse(jsonContent);
            order_id = json["rate"]["origin"]["order_id"].ToString();
            cliente = json["rate"]["origin"]["company_name"].ToString();
            Int32 tokenValidos = conBD.validarToken(token, json["rate"]["origin"]["company_name"].ToString());
            if (tokenValidos == 0)
            {
                tokenValidos = tokenValidos + conBD.validarTokenPublico(token);
            }
            if (tokenValidos > 0)
            {
                Autorizado = true;
            }
            DatosDespacho despacho = conBD.getDespacho(order_id, cliente);
            List<EstadosDespacho> estados = conBD.traerEstadosDetracking(order_id, cliente);
            Tracking tr = new Tracking();
            tr.datos = despacho;
            tr.estados = estados;



            return Get(tr,Autorizado);
        }

        [System.Web.Mvc.HttpGet]
        //GET api/values
        public IHttpActionResult Get(Tracking tr, Boolean autorizado)
        {
            if (autorizado)
            {
                if (tr.estados.Count > 0)
                {
                    try
                    {
                        var tracking = new { tracking = tr , status =  System.Net.HttpStatusCode.OK };
                        return Ok(tracking);

                    }
                    catch (Exception ex)
                    {
                        var rates = new { rates = ex.ToString(), status = System.Net.HttpStatusCode.InternalServerError };
                        return NotFound();
                    }
                }
                else
                {
                    var tracking = new { tracking = "Datos no encontrados, favor valide su order_id.", status = System.Net.HttpStatusCode.NotFound };
                    return Ok(tracking);

                }
               
            }
            else
            {
                var tracking = new { tracking = "Token no válido", status = System.Net.HttpStatusCode.NonAuthoritativeInformation };
                return Ok(tracking);
            }
               
        }

        private JsonResult Json(object res, JsonRequestBehavior allowGet)
        {
            throw new NotImplementedException();
        }
    }
}
