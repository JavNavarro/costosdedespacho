﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;
using CostosDeDespacho.App_Code.BE;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CostosDeDespacho.Controllers
{
    public class EliminarCargaController : ApiController
    {
        BDconn conBD = new BDconn();


        [System.Web.Mvc.HttpPost]
        public async Task<IHttpActionResult> Post()
        {
            Boolean Autorizado = false;
            var requestContent = Request.Content;
            var jsonContent = await requestContent.ReadAsStringAsync();
            string order_id, cliente;
            String token = Request.Headers.Authorization.ToString();

            JObject json = JObject.Parse(jsonContent);
            order_id = json["rate"]["origin"]["order_id"].ToString();
            cliente = json["rate"]["origin"]["company_name"].ToString();
            Int32 tokenValidos = conBD.validarToken(token, json["rate"]["origin"]["company_name"].ToString());
            Boolean Eliminada = false;
            if (tokenValidos == 0)
            {
                tokenValidos = tokenValidos + conBD.validarTokenPublico(token);
            }
            if (tokenValidos > 0)
            {
                Autorizado = true;
                Eliminada = conBD.EliminarCarga(order_id, cliente);
            }
        
              



            return Get(Eliminada, Autorizado,token,cliente);
        }

        [System.Web.Mvc.HttpGet]
        //GET api/values
        public IHttpActionResult Get(Boolean Eliminada, Boolean autorizado,string token, string cliente)
        {
            if (autorizado)
            {
                
                    try
                    {
                    if (Eliminada)
                    {
                        var Accion = new { Accion = "Despacho eliminado correctamente.", status = System.Net.HttpStatusCode.OK };
                        return Ok(Accion);
                    }
                    else
                    {
                        var Accion = new { Accion = "No se pudo eliminar despacho, favor valide Order_id y vuelva a intentar.", status = System.Net.HttpStatusCode.NotFound };
                        return Ok(Accion);
                    }
                       

                    }
                    catch (Exception ex)
                    {
                        var Accion = new { Accion = ex.ToString(), status = System.Net.HttpStatusCode.InternalServerError };
                        return NotFound();
                    }
               

            }
            else
            {
                int estadoToken = conBD.validarEstadoToken(token, cliente);
                token_key respuesta_token = new token_key();
                respuesta_token.token = token;
                respuesta_token.cliente = cliente;

                if (estadoToken == 0)
                {
                    respuesta_token.respuesta = "Token fue caducado";
                    var Accion = new { Accion = respuesta_token, status = System.Net.HttpStatusCode.NonAuthoritativeInformation };
                    return Ok(Accion);
                }
                else if (estadoToken == 2)
                {
                    respuesta_token.respuesta = "Token no existe";
                    var Accion = new { Accion = respuesta_token, status = System.Net.HttpStatusCode.NonAuthoritativeInformation };
                    return Ok(Accion);
                }
                else
                {
                    var Accion = new { Accion = "Token no válido", status = System.Net.HttpStatusCode.NonAuthoritativeInformation };
                    return Ok(Accion);
                }


            }

        }



        private JsonResult Json(object res, JsonRequestBehavior allowGet)
        {
            throw new NotImplementedException();
        }
    }
}
