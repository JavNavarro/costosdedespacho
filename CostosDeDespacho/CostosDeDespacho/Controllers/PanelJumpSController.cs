﻿using CostosDeDespacho.App_Code.BL;
using CostosDeDespacho.App_Code.CONSTANTES;
using CostosDeDespacho.App_Code.DTO;
using CostosDeDespacho.App_Code.DTO.Jumpseller;
using CostosDeDespacho.App_Code.Utils;
using CostosDeDespacho.Models;
using iTextSharp.text.pdf;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;


namespace CostosDeDespacho.Controllers
{
    public class PanelJumpSController : Controller
    {
        //private static readonly HttpClient client = new HttpClient();
        private const string keySessionCredencialesAPI = "objJumpsellerApi";
        private const string keySessionJumpsellerStore = "objJumpsellerStore";

        private JUMPSELLER_STORES objJumpsellerStore;

        [HttpGet]
        public ActionResult setTokenShipex(string store_id)
        {
            try
            {
                Log.Debug("setTokenShipex store_id" + store_id);
                ViewBag.idJumpsellerStore = store_id;
                db_shipexEntities context = new db_shipexEntities();
                int storeId = Convert.ToInt32(store_id);
                JUMPSELLER_STORES objJumpsellerStore = context.JUMPSELLER_STORES.Where(x => x.id_tienda_jumpseller == storeId && x.activo == true).FirstOrDefault();
                if (objJumpsellerStore.Api_Key != null)
                {
                    ViewBag.api_token = objJumpsellerStore.Api_Key.token.ToString();
                }
            }
            catch (Exception e)
            {
                Log.Fatal("Error setTokenShipex GET " + e.ToString());
            }
            return View();
        }

        [HttpPost]
        public ActionResult setTokenShipex(string idJumpsellerStore, string token)
        {
            ViewBag.idJumpsellerStore = idJumpsellerStore;
            int idStore = Convert.ToInt32(idJumpsellerStore);
            JumpsellerAPI objJumpsellerApi = this.getJumpsellerAPIObj(idStore);
            db_shipexEntities context = new db_shipexEntities();
            int storeId = Convert.ToInt32(idJumpsellerStore);
            JUMPSELLER_STORES objJumpsellerStore = context.JUMPSELLER_STORES.Where(x => x.id_tienda_jumpseller == storeId && x.activo == true).FirstOrDefault();
            Api_Key objApiKey = context.Api_Key.Where(x => x.token.Equals(token)).FirstOrDefault();
            if (objApiKey != null)
            {
                JumpsellerOperacionesAPI objOperacionesAPIJumpseller = new JumpsellerOperacionesAPI(objJumpsellerApi.token_type, objJumpsellerApi.access_token);
                ShippingMethodWrapper objShippingMethod = objOperacionesAPIJumpseller.createShippingMethod(token);
                if (objShippingMethod != null && objShippingMethod.shipping_method != null)
                {
                    //posterior a crear el shipping en la tienda debo asociar la tienda con el token
                    db_shipexEntities contextSave = new db_shipexEntities();
                    JUMPSELLER_STORES objSearchStore = contextSave.JUMPSELLER_STORES.Where(x => x.id == objJumpsellerStore.id).FirstOrDefault();
                    objSearchStore.fecha_ingreso_token = DateTime.Now;
                    objSearchStore.shipex_token_ingresado = true;
                    objSearchStore.activo = true;
                    objSearchStore.id_shipping_method = objShippingMethod.shipping_method.id;
                    objSearchStore.API_KEY_id_token = objApiKey.id;
                    contextSave.Entry(objSearchStore).State = EntityState.Modified;

                    //Registro de webhook
                    string urlProcesaOrdenWebhook = ConfigurationManager.AppSettings["app.jumpseller.url.processOrderWebhook"];
                    string eventName = ConfigurationManager.AppSettings["api.jumpseller.hook.order.paid"];
                    bool resCreacion = objOperacionesAPIJumpseller.createWebhook(eventName, urlProcesaOrdenWebhook);
                    //Fin registro de webhook

                    contextSave.SaveChanges();
                    this.setValorSession(keySessionJumpsellerStore, objSearchStore);
                    return RedirectToAction("panelOrdenes", new { store_id = objSearchStore.id_tienda_jumpseller });
                }
            }
            else
            {
                return View();
            }
            return View();
        }

        [HttpPost]
        public ActionResult panelOrdenes(string idOrden, string fecDesde, string fecHasta, string idJumpsellerStore)
        {

            //Caso sin filtro, solo se redirecicona al controlador por get
            ViewBag.idJumpsellerStore = idJumpsellerStore;
            if (fecDesde.Equals("") && fecHasta.Equals("") && idOrden.Trim().Equals(""))
            {
                Log.Debug("redireccionando ..." + idJumpsellerStore);
                return RedirectToAction("panelOrdenes", new { store_id = idJumpsellerStore });
            }

            List<PanelOrderJumpsellerViewModel> lOrderJumsellerVM = new List<PanelOrderJumpsellerViewModel>();
            try
            {
                int idStore = Convert.ToInt32(idJumpsellerStore);
                db_shipexEntities objConexion = new db_shipexEntities();
                JUMPSELLER_STORES objJumpsellerStore = objConexion.JUMPSELLER_STORES.Where(x => x.id_tienda_jumpseller == idStore && x.activo == true).FirstOrDefault();
                int nOrden;
                bool resCambioOrden = false;
                JUMPSELLER_ORDERS objOrdenBusquedaId;

                bool conFechas = fecDesde != null && !fecDesde.Trim().Equals("") && fecHasta != null && !fecHasta.Trim().Equals("") ? true : false;
                bool conOrden = idOrden != null && !idOrden.Trim().Equals("") ? true : false;
                //Solo busco por orden
                if (conOrden == true && conFechas == false)
                {
                    resCambioOrden = Int32.TryParse(idOrden.Trim(), out nOrden);
                    objOrdenBusquedaId = objConexion.JUMPSELLER_ORDERS.Where(x => x.order_id == nOrden && x.jumpseller_store_id == objJumpsellerStore.id).FirstOrDefault();
                    PanelOrderJumpsellerViewModel objOrderJumpsellerVM = new PanelOrderJumpsellerViewModel();
                    objOrderJumpsellerVM.comuna = objOrdenBusquedaId.comuna;
                    objOrderJumpsellerVM.direccionEnvio = objOrdenBusquedaId.direccion_envio;
                    objOrderJumpsellerVM.email = objOrdenBusquedaId.email;
                    objOrderJumpsellerVM.estadoEnvioShipex = objOrdenBusquedaId.SHIPEX_API_ESTADOS.estado;
                    objOrderJumpsellerVM.estadoOrdenJumpseller = objOrdenBusquedaId.status;
                    objOrderJumpsellerVM.id = objOrdenBusquedaId.id;
                    objOrderJumpsellerVM.id_orden_jumpseller = objOrdenBusquedaId.order_id.ToString();
                    objOrderJumpsellerVM.nombreReceptor = objOrdenBusquedaId.nombre_receptor;
                    objOrderJumpsellerVM.telefono = objOrdenBusquedaId.telefono;
                    lOrderJumsellerVM.Add(objOrderJumpsellerVM);
                    ViewBag.idOrden = idOrden;
                }

                //Busco solo por fechas
                if (conOrden == false && conFechas == true)
                {
                    String procAlmacenado = "exec FILTRAR_ORDENES_JUMPSELLER '@fechaInicio', '@fechaFin', '@idStore'";
                    procAlmacenado = procAlmacenado.Replace("@fechaInicio", fecDesde);
                    procAlmacenado = procAlmacenado.Replace("@fechaFin", fecHasta);
                    procAlmacenado = procAlmacenado.Replace("@idStore", objJumpsellerStore.id.ToString());
                    PanelOrderJumpsellerViewModel objOrderJumpsellerVM = objConexion.Database.SqlQuery<PanelOrderJumpsellerViewModel>(procAlmacenado).FirstOrDefault();
                    lOrderJumsellerVM.Add(objOrderJumpsellerVM);
                }
            }
            catch (Exception ex)
            {
                Log.Fatal("Error panelOrdenes post " + ex.ToString());
            }

            return View(lOrderJumsellerVM);
        }

        [HttpGet]
        public ActionResult panelOrdenes(string store_id)
        {
            Log.Debug("Panel para store_id " + store_id);
            int storeId;
            JUMPSELLER_STORES objJumpsellerStore = null;
            JumpsellerAPI objJumpsellerApi = null;
            db_shipexEntities context = new db_shipexEntities();
            if (Int32.TryParse(store_id, out storeId))
            {
                Log.Debug("Cargando datos para la store_id " + store_id);
                objJumpsellerStore = context.JUMPSELLER_STORES.Where(x => x.id_tienda_jumpseller == storeId && x.activo == true).FirstOrDefault();
                if (objJumpsellerStore != null)
                {
                    JUMPSELLER_CREDENCIALES_APP objCredenciales = context.JUMPSELLER_CREDENCIALES_APP.Where(x => x.store_id == objJumpsellerStore.id).FirstOrDefault();
                    Log.Debug("Tiene datos?" + (objCredenciales != null));
                    if (objCredenciales != null)
                    {
                        objJumpsellerApi = new JumpsellerAPI();
                        objJumpsellerApi.access_token = objCredenciales.access_token;
                        objJumpsellerApi.created_at = (int)objCredenciales.created_at;
                        objJumpsellerApi.refresh_token = objCredenciales.refresh_token;
                        objJumpsellerApi.token_type = objCredenciales.token_type;
                    }
                    else
                    {
                        //No encontro credenciales registradas, se envia al proceso de autorización
                        Log.Debug("Redirección a proceso de autorización. store_id: " + store_id + " no se encuentra registradas las credenciales");
                        return RedirectToAction("ProcesarOauth");
                    }
                }
                else
                {
                    //No encontro la tienda registrada, se envia al proceso de autorización
                    Log.Debug("Redirección a proceso de autorización. store_id: " + store_id + " no se encuentra registrada la tienda");
                    return RedirectToAction("ProcesarOauth");
                }
                Log.Debug("Fin carga de datos para la store_id " + store_id);
            }

            //Valido el uso del token para generar el metodo de envío, en caso de no existir, se envía a pag de solicitud
            //objOperacionesAPIJumpseller.createShippingMethod();
            JumpsellerOperacionesAPI objOperacionesAPIJumpseller = new JumpsellerOperacionesAPI(objJumpsellerApi.token_type, objJumpsellerApi.access_token);
            if (this.actualizarToken(objJumpsellerApi))
            {
                JUMPSELLER_STORES objStore = objOperacionesAPIJumpseller.getInfoStore();
                if (objStore != null)
                {
                    if (objStore.shipex_token_ingresado == null || objStore.shipex_token_ingresado == false)
                    {
                        //Redirecciono para que se genere el ingreso de token
                        Log.Debug("redireccionando para solicitar token store_id: " + store_id);
                        return RedirectToAction("setTokenShipex", new { store_id = store_id });
                    }
                    else
                    {
                        //Obtengo los primeros 50 pedidos
                        List<JUMPSELLER_ORDERS> lOrdenes = context.JUMPSELLER_ORDERS.Where(x => x.jumpseller_store_id == objStore.id).OrderByDescending(w => w.fecha_registro).Take(50).ToList();
                        List<PanelOrderJumpsellerViewModel> lOrdenJumpseller = new List<PanelOrderJumpsellerViewModel>();
                        foreach (JUMPSELLER_ORDERS objOrderJumpseller in lOrdenes)
                        {
                            PanelOrderJumpsellerViewModel objOrderJumpVM = new PanelOrderJumpsellerViewModel();
                            objOrderJumpVM.id_orden_jumpseller = objOrderJumpseller.order_id.ToString();
                            objOrderJumpVM.direccionEnvio = objOrderJumpseller.direccion_envio;
                            objOrderJumpVM.email = objOrderJumpseller.email;
                            objOrderJumpVM.comuna = objOrderJumpseller.comuna;
                            objOrderJumpVM.nombreReceptor = objOrderJumpseller.nombre_receptor;
                            objOrderJumpVM.telefono = objOrderJumpseller.telefono;
                            objOrderJumpVM.estadoOrdenJumpseller = objOrderJumpseller.status;
                            objOrderJumpVM.estadoEnvioShipex = objOrderJumpseller.SHIPEX_API_ESTADOS.estado;
                            objOrderJumpVM.id = objOrderJumpseller.id;
                            objOrderJumpVM.tracking = objOrderJumpseller.codigo_despacho_shipex == null || objOrderJumpseller.codigo_despacho_shipex == "null" ? "-" : objOrderJumpseller.codigo_despacho_shipex;
                            lOrdenJumpseller.Add(objOrderJumpVM);
                        }
                        //Se pasa a la vista el store id para la busqueda (la sesion la pierde por ser iframe)
                        ViewBag.idJumpsellerStore = store_id;
                        return View(lOrdenJumpseller);
                    }
                }
            }
            //en caso de fallar la validacion de actualizacion de token se envia al proceso de autorizacion
            return RedirectToAction("ProcesarOauth");
        }

        /// <summary>
        /// Valida el ingreso de la tienda, en caso de no estar registrada realiza el proceso de autorización
        /// </summary>
        /// <param name="store_id">Id de tienda</param>
        /// <returns></returns>
        public ActionResult validaTienda(int store_id)
        {
            try
            {
                db_shipexEntities context = new db_shipexEntities();
                JUMPSELLER_STORES objStore = context.JUMPSELLER_STORES.Where(x => x.id_tienda_jumpseller == store_id).FirstOrDefault();
                if (objStore != null && objStore.activo == true && (objStore.actualizar_permisos == false || objStore.actualizar_permisos == null))
                {
                    //Seteo los valores a utilizar mas adelante en la pap 
                    JUMPSELLER_CREDENCIALES_APP objJumpsellerCredencialesApp = objStore.JUMPSELLER_CREDENCIALES_APP.FirstOrDefault();
                    JumpsellerAPI objJumpsellerApi = new JumpsellerAPI();

                    objJumpsellerApi.access_token = objJumpsellerCredencialesApp.access_token;
                    objJumpsellerApi.created_at = (int)objJumpsellerCredencialesApp.created_at;
                    objJumpsellerApi.refresh_token = objJumpsellerCredencialesApp.refresh_token;
                    objJumpsellerApi.token_type = objJumpsellerCredencialesApp.token_type;
                    this.setValorSession(keySessionJumpsellerStore, objStore);
                    this.setValorSession(keySessionCredencialesAPI, objJumpsellerApi);
                    return RedirectToAction("panelOrdenes", new { store_id = store_id });
                }
                else
                {
                    //Como no esta registrada la tienda, se reenvía al proceso de autorización
                    return RedirectToAction("ProcesarOauth");
                }
            }
            catch (Exception e)
            {
                Log.Fatal("Problema al validar la tienda id store " + store_id);
                Log.Fatal(e.ToString());
                Log.Fatal("Fin problema al validar la tienda");
            }
            return View();
        }

        /// <summary>
        /// Metodo de autorización OAuth 2, el cual es necesario para autenticarse sin necesidad de "pasar" por usuario y password
        /// para mayor información del flujo ver https://jumpseller.com/support/oauth-2/
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ProcesarOauth(String code)
        {
            Log.Debug("Ingreso a Procesar autorizacion");
            /*
             * El flujo OAuth se explica en los siguientes pasos:
             * 1. Solicitar el codigo para autorizar con los parametros necesarios para leer, escribir según el objeto de sistema (Pedidos, productos, etc), esto se retorna si el usuario da aceptar a los permisos
             * 2. Solicitar/refrescar el token según el codigo solicitado en el punto 1 
             */
            if (code == null)
            {
                //Paso 1. Solicito el codigo
                Log.Debug("Paso 1. Solicitud de codigo");

                try
                {
                    //https://accounts.jumpseller.com/oauth/authorize?client_id=[[client_id]]&redirect_uri=&[[redirect_uri]]&response_type=[[response_type]]&scope=[[scope]]

                    string url = ConfigurationManager.AppSettings["oauth.authorize.url"];
                    string clientId = ConfigurationManager.AppSettings["oauth.authorize.client_id"];
                    string redirectUri = ConfigurationManager.AppSettings["oauth.authorize.redirect_uri"];
                    string responseType = ConfigurationManager.AppSettings["oauth.authorize.response_type"];
                    string scope = ConfigurationManager.AppSettings["oauth.authorize.scope"];
                    url = url.Replace("[[client_id]]", clientId);
                    url = url.Replace("[[redirect_uri]]", redirectUri);
                    url = url.Replace("[[response_type]]", responseType);
                    url = url.Replace("[[scope]]", scope);
                    Log.Debug("Url sol. codigo: " + url);
                    Response.Redirect(url);
                }
                catch (Exception e)
                {
                    Response.Write(e.ToString());
                }
            }
            else
            {
                Log.Debug("Paso 2 revision credenciales");
                Log.Debug("Cod. " + code);
                bool reinstalacionApp = false;
                //Paso 2. Solicito el token, siempre y cuando no tenga las credenciales en bd, caso contrario simplemente cargo los valores
                JumpsellerAPI objJumpsellerApi = new JumpsellerAPI();

                JUMPSELLER_CREDENCIALES_APP objJumpsellerCredencialesApp = null;
                string url = ConfigurationManager.AppSettings["oauth.url.token"];
                string urlBaseParameters = ConfigurationManager.AppSettings["oauth.url.parameters"];
                string clientId = ConfigurationManager.AppSettings["oauth.authorize.client_id"];
                string clientSecret = ConfigurationManager.AppSettings["oauth.client_secret"];
                string grantType = ConfigurationManager.AppSettings["oauth.grant_type"];
                string redirectUrl = ConfigurationManager.AppSettings["oauth.authorize.redirect_uri"];
                urlBaseParameters = urlBaseParameters.Replace("[[client_id]]", clientId);
                urlBaseParameters = urlBaseParameters.Replace("[[client_secret]]", clientSecret);
                urlBaseParameters = urlBaseParameters.Replace("[[grant_type]]", grantType);
                urlBaseParameters = urlBaseParameters.Replace("[[code]]", code);
                urlBaseParameters = urlBaseParameters.Replace("[[redirect_uri]]", redirectUrl);
                using (WebClient wc = new WebClient())
                {
                    using (db_shipexEntities objShipex = new db_shipexEntities())
                    {
                        Log.Debug("Llamada servicio para autorizar");
                        try
                        {
                            string jsonResult = "";
                            wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                            jsonResult = wc.UploadString(new Uri(url), "POST", urlBaseParameters);
                            if (jsonResult != null && !String.IsNullOrEmpty(jsonResult.Trim()))
                            {
                                Log.Debug("Llamada realizada oathtoken.  respuesta: " + jsonResult);

                                objJumpsellerApi = JsonConvert.DeserializeObject<JumpsellerAPI>(jsonResult);
                                Log.Debug("Obteniendo info de token con parametros. token_type" + objJumpsellerApi.token_type + " --- access_token " + objJumpsellerApi.access_token);
                                JumpsellerOperacionesAPI objAPIJumpseller = new JumpsellerOperacionesAPI(objJumpsellerApi.token_type, objJumpsellerApi.access_token);
                                TokenInfo objTokenInfo = objAPIJumpseller.obtenerInfoTiendaPorToken();
                                Log.Debug("store_id obtenido:" + objTokenInfo.store_id.ToString());

                                if (objTokenInfo != null)
                                {
                                    Log.Debug("Instalando la app");

                                    //caso reinstalacion de app
                                    objJumpsellerStore = objShipex.JUMPSELLER_STORES.Where(x => x.id_tienda_jumpseller == objTokenInfo.store_id).FirstOrDefault();
                                    if (objJumpsellerStore != null)
                                    {
                                        objJumpsellerCredencialesApp = objShipex.JUMPSELLER_CREDENCIALES_APP.Where(x => x.store_id == objJumpsellerStore.id).FirstOrDefault();
                                    }
                                    if (objJumpsellerCredencialesApp != null && objJumpsellerStore != null)
                                    {
                                        //elimino el shipping existente
                                        Log.Debug("caso reinstalacion");
                                       // int idShippingMethod = (int)objJumpsellerStore.id_shipping_method;
                                        //    objAPIJumpseller.deteleShippingMethod(idShippingMethod);
                                        objAPIJumpseller.getShippingMethods();
                                        objJumpsellerCredencialesApp.access_token = objJumpsellerApi.access_token;
                                        objJumpsellerCredencialesApp.created_at = objJumpsellerApi.created_at;
                                        objJumpsellerCredencialesApp.refresh_token = objJumpsellerApi.refresh_token;
                                        objJumpsellerCredencialesApp.token_type = objJumpsellerApi.token_type;
                                        objJumpsellerCredencialesApp.fecha_modificacion = DateTime.Now;
                                        objShipex.Entry(objJumpsellerCredencialesApp).State = EntityState.Modified;
                                        objShipex.SaveChanges();
                                        Log.Debug("Fin reinstalacion");
                                    }
                                    else
                                    {
                                        Log.Debug("Instalando app desde cero");

                                        //Creo la tienda en los registros de Shipex
                                        JumpsellerOperacionesAPI objOperacionesAPIJumpseller = new JumpsellerOperacionesAPI(objJumpsellerApi.token_type, objJumpsellerApi.access_token);
                                        objJumpsellerStore = objOperacionesAPIJumpseller.saveInfoStore();
                                        if (objJumpsellerStore != null)
                                        {
                                            Log.Debug("Id table.jumpseller_store " + objJumpsellerStore.id);
                                            objJumpsellerCredencialesApp = new JUMPSELLER_CREDENCIALES_APP();
                                            objJumpsellerCredencialesApp.access_token = objJumpsellerApi.access_token;
                                            objJumpsellerCredencialesApp.created_at = objJumpsellerApi.created_at;
                                            objJumpsellerCredencialesApp.refresh_token = objJumpsellerApi.refresh_token;
                                            objJumpsellerCredencialesApp.token_type = objJumpsellerApi.token_type;
                                            objJumpsellerCredencialesApp.fecha_creacion = DateTime.Now;
                                            objJumpsellerCredencialesApp.nombre_app = ConfigurationManager.AppSettings["app.jumpseller.name"];
                                            //Se asocia la tienda a los tokens
                                            objJumpsellerCredencialesApp.store_id = objJumpsellerStore.id;
                                            objShipex.Entry(objJumpsellerCredencialesApp).State = EntityState.Added;
                                            objShipex.SaveChanges();

                                            //Debo volver a buscar debido a que viene con otro contexto el objeto
                                            objJumpsellerStore = objShipex.JUMPSELLER_STORES.Where(x => x.id == objJumpsellerStore.id).FirstOrDefault();

                                            objJumpsellerStore.id_tienda_jumpseller = objTokenInfo.store_id;
                                            objShipex.Entry(objJumpsellerStore).State = EntityState.Modified;
                                            objShipex.SaveChanges();

                                            Log.Debug("Fin instalacion app desde cero");
                                        }
                                        else
                                        {
                                            Log.Fatal("no se obtiene info de tienda jumpseller");
                                        }
                                    }
                                }
                                Log.Debug("Redireccionando a panel con store_id: " + objTokenInfo.store_id);
                                return RedirectToAction("panelOrdenes", new { store_id = objTokenInfo.store_id });
                            }
                        }
                        catch (Exception ex)
                        {
                            Log.Error("Error ProcesarOauth (code = " + code + " Excepción: " + ex.ToString());
                            Response.Write("Error al ProcesarOauth: " + ex.ToString());
                        }
                    }

                }
            }
            //TODO: Se debe "crear una vista que solicite el token que shipex debe asociar, para posteriormente generar el registro que esta activa la cuenta
            return View();
        }

        [HttpGet]
        public ActionResult Confirmar(long idTableOrden, int id_store)
        {
            string result = String.Empty;
            try
            {
                db_shipexEntities context = new db_shipexEntities();
                JUMPSELLER_STORES objStore = context.JUMPSELLER_STORES.Where(x => x.id_tienda_jumpseller == id_store && x.activo == true).FirstOrDefault();
                if (objStore != null && objStore.activo == true && (objStore.actualizar_permisos == false || objStore.actualizar_permisos == null))
                {
                    //Seteo los valores a utilizar mas adelante en la pap 
                    JUMPSELLER_CREDENCIALES_APP objJumpsellerCredencialesApp = objStore.JUMPSELLER_CREDENCIALES_APP.FirstOrDefault();
                    JumpsellerAPI objJumpsellerApi = new JumpsellerAPI();

                    objJumpsellerApi.access_token = objJumpsellerCredencialesApp.access_token;
                    objJumpsellerApi.created_at = (int)objJumpsellerCredencialesApp.created_at;
                    objJumpsellerApi.refresh_token = objJumpsellerCredencialesApp.refresh_token;
                    objJumpsellerApi.token_type = objJumpsellerCredencialesApp.token_type;


                    JUMPSELLER_ORDERS objOrder = context.JUMPSELLER_ORDERS.Where(x => x.id == idTableOrden).FirstOrDefault();
                    objOrder.id_api_estado = ShipexAPIConstantes.confirmado;
                    context.Entry(objOrder).State = EntityState.Modified;
                    if (actualizarToken(objJumpsellerApi))
                    {
                        JumpsellerOperacionesAPI objShipexAPI = new JumpsellerOperacionesAPI(objJumpsellerApi.token_type, objJumpsellerApi.access_token);
                        FulfillmentWrapper objFullfillmentWR = new FulfillmentWrapper();
                        Fulfillment objFulfillment = new Fulfillment();

                        objFulfillment.shipment_status = "requested";
                        objFulfillment.order_id = objOrder.order_id.ToString();
                        objFulfillment.type = "manual";
                        objFulfillment.tracking_number = objOrder.codigo_despacho_shipex;
                        objFulfillment.tracking_company = "other";
                        objFulfillment.external_id = objOrder.codigo_despacho_shipex;
                        objFulfillment.service_type = "delivery";
                        objFullfillmentWR.fulfillment = objFulfillment;

                        bool resFulFillment = objShipexAPI.createFulfillment(objFullfillmentWR);
                        OrderJumpsellerWrapper objJumpsellerOrderAPI = objShipexAPI.getOrderByOrderId(objOrder.order_id.ToString());
                        OrderJumpsellerViewModel orderAPJumpseller = objJumpsellerOrderAPI.order;
                        bool resUpdateOrder = objShipexAPI.updateOrder(orderAPJumpseller.id.ToString(), orderAPJumpseller.status, objFulfillment.shipment_status, objFulfillment.tracking_number, objFulfillment.tracking_company, objOrder.url_tracking, "");

                        if (resFulFillment && resUpdateOrder)
                        {
                            result = "Confirmación creada correctamente";
                            context.SaveChanges();
                        }
                        else
                        {
                            result = "Problemas al confirmar";
                        }
                        return RedirectToAction("panelOrdenes", new { store_id = id_store });
                    }
                }
                return RedirectToAction("panelOrdenes", new { store_id = id_store });
            }
            catch (Exception e)
            {
                result = e.ToString();
                return RedirectToAction("panelOrdenes", new { store_id = id_store });
            }
        }

        private bool actualizarToken(JumpsellerAPI objJumpSeller)
        {
            DateTime fechaObtencionToken = TimeHandler.UnixTimeStampToDateTime(objJumpSeller.created_at);
            int cantidadHoras = (DateTime.Now - fechaObtencionToken).Hours;
            int tiempoActualizacionToken = Convert.ToInt32(ConfigurationManager.AppSettings["oauth.horas.actualizacionToken"]);
            if (cantidadHoras >= tiempoActualizacionToken)
            {
                //Se realiza la peticion de actualizar token
                try
                {
                    string url = ConfigurationManager.AppSettings["oauth.url.token"];
                    string urlBaseParameters = ConfigurationManager.AppSettings["oauth.url.parameters.refresh"];
                    string clientId = ConfigurationManager.AppSettings["oauth.authorize.client_id"];
                    string clientSecret = ConfigurationManager.AppSettings["oauth.client_secret"];
                    string grantType = ConfigurationManager.AppSettings["oauth.grant_type.refresh"];
                    urlBaseParameters = urlBaseParameters.Replace("[[client_id]]", clientId);
                    urlBaseParameters = urlBaseParameters.Replace("[[client_secret]]", clientSecret);
                    urlBaseParameters = urlBaseParameters.Replace("[[grant_type]]", grantType);
                    urlBaseParameters = urlBaseParameters.Replace("[[refresh_token]]", objJumpSeller.refresh_token);
                    using (WebClient wc = new WebClient())
                    {
                        wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                        string jsonResult = wc.UploadString(url, urlBaseParameters);
                        if (jsonResult != null && !String.IsNullOrEmpty(jsonResult.Trim()))
                        {
                            JumpsellerAPI newDataToken = JsonConvert.DeserializeObject<JumpsellerAPI>(jsonResult);
                            db_shipexEntities objShipex = new db_shipexEntities();
                            JUMPSELLER_CREDENCIALES_APP objJumpsellerCredencialesApp = objShipex.JUMPSELLER_CREDENCIALES_APP.Where(x => x.access_token.Equals(objJumpSeller.access_token)).FirstOrDefault();
                            objJumpsellerCredencialesApp.access_token = newDataToken.access_token;
                            objJumpsellerCredencialesApp.created_at = newDataToken.created_at;
                            objJumpsellerCredencialesApp.refresh_token = newDataToken.refresh_token;
                            objJumpsellerCredencialesApp.token_type = newDataToken.token_type;
                            objShipex.Entry(objJumpsellerCredencialesApp).State = EntityState.Modified;
                            objShipex.SaveChanges();
                            this.setValorSession(keySessionCredencialesAPI, newDataToken);
                        }
                    }
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            return true;
        }

        private bool setValorSession(string keySession, object value)
        {
            try
            {
                Session[keySession] = value;
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private JumpsellerAPI getJumpsellerAPIObj(int storeId)
        {
            db_shipexEntities context = new db_shipexEntities();
            JumpsellerAPI objJumpsellerApi = null;
            objJumpsellerStore = context.JUMPSELLER_STORES.Where(x => x.id_tienda_jumpseller == storeId && x.activo == true).FirstOrDefault();
            JUMPSELLER_CREDENCIALES_APP objCredenciales = context.JUMPSELLER_CREDENCIALES_APP.Where(x => x.store_id == objJumpsellerStore.id).FirstOrDefault();
            if (objCredenciales != null)
            {
                objJumpsellerApi = new JumpsellerAPI();
                objJumpsellerApi.access_token = objCredenciales.access_token;
                objJumpsellerApi.created_at = (int)objCredenciales.created_at;
                objJumpsellerApi.refresh_token = objCredenciales.refresh_token;
                objJumpsellerApi.token_type = objCredenciales.token_type;
            }
            return objJumpsellerApi;
        }

        [HttpPost]
        public ActionResult GenerarPDF_Masivo(List<JUMPSELLER_ORDERS> _trackingJS)
        {
            try
            {
                db_shipexEntities context = new db_shipexEntities();
                var fName = "etiqueta_" + DateTime.Now.ToString("dd-MM-yy-HH-mm-ss") + ".pdf";
                iTextSharp.text.Document doc = new iTextSharp.text.Document();
                //string ruta = "C:/Webdev/warehouseshipex/warehouseShipex/Etiquetas/" + fName; //producción
                string ruta = Server.MapPath("~/Etiquetas/" + fName);

                iTextSharp.text.pdf.PdfWriter ESCRITOR = iTextSharp.text.pdf.PdfWriter.GetInstance(doc, new FileStream(ruta, FileMode.CreateNew));
                iTextSharp.text.Rectangle tamanoPag = new iTextSharp.text.Rectangle(0, 0, 799, 1199);

                try
                {
                    doc.SetPageSize(tamanoPag);
                }
                catch (Exception ex)
                {

                }
                doc.Open();
                doc.SetPageSize(tamanoPag);

                long CodDespacho;
                long CodDespacho2;
                string refCli;
                string nombreCliente;
                string direccion;
                string comuna;
                string ciudad;
                string strGuiasTotal;
                string Telefono;
                string[] listGuias;
                string comentario;
                int guiaIndex;
                int cantGuias;

                DataTable dt = new DataTable();

                System.Drawing.Image img;
                iTextSharp.text.Image imagen2;

                long strGuiaThis;

                foreach (var item in _trackingJS)
                {
                    Int64 tracking_shipex = Convert.ToInt64(item.codigo_despacho_shipex);
                    var strGuias = context.Registro.FirstOrDefault(re => re.Tracking == tracking_shipex);
                    string strCliente = strGuias.postaBlue;

                    if (strGuias != null)
                    {
                        strGuiasTotal = strGuias.Referencia.Replace('"', ' ').Replace(',', '-').Replace('[', ' ').Replace(']', ' ').ToString();
                        listGuias = strGuiasTotal.Split('-');
                        cantGuias = listGuias.Count();
                        guiaIndex = 0;

                        foreach (var b in listGuias)
                        {
                            guiaIndex += 1;
                            strGuiaThis = Int64.Parse(b.Trim());

                            var dTable = context.Awb_Servicio.FirstOrDefault(aw => aw.awb == strGuiaThis);

                            if (dTable != null)
                            {
                                CodDespacho = Int64.Parse(dTable.coddespacho.ToString()); //tracking shipex
                                CodDespacho2 = Int64.Parse(dTable.codDespachoMandante.ToString()); //mandante
                                comuna = strGuias.comuna;
                                direccion = strGuias.calle;
                                nombreCliente = strGuias.NomCli1 + ' ' + strGuias.NomCli2 + ' ' + strGuias.ApatCli + ' ' + strGuias.AmatCli;
                                refCli = strGuias.ReferenciaCliente;
                                ciudad = strGuias.ciudad;
                                Telefono = strGuias.telefono;
                                comentario = strGuias.comentario;

                                string paquetesCant = "BULTO: " + guiaIndex.ToString() + "/" + cantGuias.ToString();

                                img = ConvertHtmlToImage(CodDespacho.ToString(), strGuiaThis.ToString(), refCli.ToString(), comuna, comentario, nombreCliente, direccion, Telefono, paquetesCant, strCliente);

                                imagen2 = iTextSharp.text.Image.GetInstance(img, System.Drawing.Imaging.ImageFormat.Bmp);

                                doc.Add(imagen2);
                                doc.NewPage();
                            }
                            else
                            {
                                long guiaSum = long.Parse(strGuiaThis.ToString() + guiaIndex.ToString());
                                dTable = context.Awb_Servicio.FirstOrDefault(aw => aw.awb == guiaSum);

                                if (dTable != null)
                                {
                                    strGuiaThis = guiaSum;

                                    CodDespacho = Int64.Parse(dTable.coddespacho.ToString()); //tracking shipex
                                    CodDespacho2 = Int64.Parse(dTable.codDespachoMandante.ToString()); //mandante
                                    comuna = strGuias.comuna;
                                    direccion = strGuias.calle;
                                    nombreCliente = strGuias.NomCli1 + ' ' + strGuias.NomCli2 + ' ' + strGuias.ApatCli + ' ' + strGuias.AmatCli;
                                    refCli = strGuias.ReferenciaCliente;
                                    ciudad = strGuias.ciudad;
                                    Telefono = strGuias.telefono;
                                    comentario = strGuias.comentario;

                                    string paquetesCant = "BULTO: " + guiaIndex.ToString() + "/" + cantGuias.ToString();

                                    img = ConvertHtmlToImage(CodDespacho.ToString(), strGuiaThis.ToString(), refCli.ToString(), comuna, comentario, nombreCliente, direccion, Telefono, paquetesCant, strCliente);

                                    imagen2 = iTextSharp.text.Image.GetInstance(img, System.Drawing.Imaging.ImageFormat.Bmp);

                                    doc.Add(imagen2);
                                    doc.NewPage();
                                }
                            }
                        }
                    }
                }

                doc.Close();
                Session["etiqueta"] = fName;
                return Json(new { success = true, fName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var res = new { res = ex.ToString(), status = System.Net.HttpStatusCode.InternalServerError };
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            
        }

        private Image ConvertHtmlToImage(object codDespacho, string strGuiaThis, string refCli, string comuna, string comentario, string nombreCliente, string direccion, string telefono, string paquetesCant, string strCliente)
        {
            Bitmap m_Bitmap = new Bitmap(799, 1199); // 886  591
            PointF point = new PointF(0, 50);
            Point point2 = new Point(0, 0);
            Size maxSize = new System.Drawing.Size(799, 1199); // 591
            Size SizeBarCodTR = new System.Drawing.Size(1, 50);
            Size SizeBarCodRef = new System.Drawing.Size(1, 60);
            Size SizeBarCodGuia = new System.Drawing.Size(5, 45);
            SolidBrush br = new SolidBrush(Color.White);
            Graphics g = Graphics.FromImage(m_Bitmap);
            System.Drawing.Font f = new System.Drawing.Font("arial", 16, FontStyle.Bold);
            System.Drawing.Font f2 = new System.Drawing.Font("arial", 22, FontStyle.Bold);
            System.Drawing.Font f3 = new System.Drawing.Font("arial", 30, FontStyle.Bold);
            System.Drawing.Font f33 = new System.Drawing.Font("arial", 36, FontStyle.Bold);
            System.Drawing.Font f32 = new System.Drawing.Font("arial", 34, FontStyle.Bold);
            System.Drawing.Font f4 = new System.Drawing.Font("arial", 26, FontStyle.Bold);
            System.Drawing.Font f5 = new System.Drawing.Font("arial", 24, FontStyle.Bold);

            iTextSharp.text.Rectangle recCod = new iTextSharp.text.Rectangle(0, 135, 700, 2); // = New Rectangle(0, 0, 100, 10)
            g.FillRectangle(br, 0, 0, 799, 1199); // 886 591
            Barcode128 bcTrack39 = new Barcode128();
            bcTrack39.Code = Convert.ToInt64(codDespacho).ToString();
            bcTrack39.X = 8;
            bcTrack39.N = 8;
            bcTrack39.BarHeight = 45;

            Barcode128 bcGuia = new Barcode128();
            bcGuia.Code = Convert.ToInt64(strGuiaThis).ToString();
            bcGuia.X = 8;
            bcGuia.N = 8;
            bcGuia.BarHeight = 55;

            Barcode128 bcref = new Barcode128();
            bcref.Code = refCli;
            bcref.X = 3;
            bcref.N = 3;
            bcref.BarHeight = 30;

            // Ref Cliente
            System.Drawing.Image ImageBarCodRef = bcref.CreateDrawingImage(System.Drawing.Color.Black, System.Drawing.Color.White);
            g.DrawString(refCli, f4, Brushes.Black, 185, 230);
            g.DrawString("N° REFERENCIA", f, Brushes.Black, 185, 145);
            g.DrawImage(ImageBarCodRef, 180, 165, 300, 60);

            // tracking
            System.Drawing.Image imageBarCodTr = bcTrack39.CreateDrawingImage(System.Drawing.Color.Black, System.Drawing.Color.White);
            g.DrawImage(imageBarCodTr, 100, 665, 515, 70);
            g.DrawString("TRACKING SHIPEX", f, Brushes.Black, 110, 640);
            g.DrawString(codDespacho.ToString(), f4, Brushes.Black, 120, 740);

            // guia
            System.Drawing.Image ImageBarCodGuia = bcGuia.CreateDrawingImage(System.Drawing.Color.Black, System.Drawing.Color.White);
            g.DrawString(strGuiaThis, f4, Brushes.Black, 100, 1030);
            g.DrawImage(ImageBarCodGuia, 20, 1070, 450, 70);
            // g.DrawString(cod, f, Brushes.Black, 330, 105)
            //System.Drawing.Image imgShipex = System.Drawing.Image.FromFile("C:/Webdev/warehouseshipex/warehouseShipex/Archivos/logo_shipex.jpg"); //produccion
            System.Drawing.Image imgShipex = System.Drawing.Image.FromFile(Server.MapPath("~/Archivos/logo_shipex.jpg")); //desarrollo
            g.DrawImage(imgShipex, 610, 1030);

            try
            {
                //System.Drawing.Image imgCliente = System.Drawing.Image.FromFile("C:/Webdev/warehouseshipex/warehouseShipex/Archivos/logo_" + strCliente + ".jpg"); //produccion
                System.Drawing.Image imgCliente = System.Drawing.Image.FromFile(Server.MapPath("~/Archivos/logo_" + strCliente + ".jpg")); //desarrollo
                Int32 xImg, YImg;
                YImg = imgCliente.Height;
                xImg = imgCliente.Width;
                if (YImg > 50)
                {
                    YImg = 50;
                }
                if (xImg > 100)
                {
                    xImg = 100;
                }
                try
                {
                    g.DrawImage(imgCliente, 0, 20, 2 * xImg, 2 * YImg);
                }
                catch (Exception EX)
                {
                    g.DrawImage(imgCliente, 0, 20);
                }
            }
            catch (Exception ex)
            {

            }
            System.Drawing.Rectangle rec = new System.Drawing.Rectangle(0, 0, 799, 140);
            try
            {
                if (strCliente.Length < 18 && strCliente.Length > 8)
                {
                    g.DrawString(strCliente.ToUpper(), f32, Brushes.Black, 290, 20);
                }
                else if (strCliente.Length < 8)
                {
                    g.DrawString(strCliente.ToUpper(), f33, Brushes.Black, 290, 20);
                }
                else
                {
                    g.DrawString(strCliente.ToUpper(), f3, Brushes.Black, 290, 20);
                }
            }
            catch (Exception)
            {
                g.DrawString(strCliente.ToUpper(), f3, Brushes.Black, 290, 20);
            }

            Pen pen = new Pen(Brushes.Black, 3);
            //g.DrawRectangle(pen, rec):

            g.DrawLine(pen, 0, 290, 799, 290);
            g.DrawLine(pen, 0, 140, 799, 140);
            g.DrawLine(pen, 0, 630, 799, 630);
            g.DrawLine(pen, 0, 1020, 799, 1020);
            g.DrawLine(pen, 0, 830, 799, 830);

            g.DrawString("Información Destinatario", f2, Brushes.Black, 10, 292);
            g.DrawString("Nombre: " + nombreCliente, f2, Brushes.Black, 0, 329);

            if (direccion.Length > 35)
            {
                g.DrawString("Dirección: " + direccion.Substring(0, 35), f2, Brushes.Black, 0, 358);

                try
                {
                    g.DrawString(direccion.Substring(35, direccion.Length - 35), f2, Brushes.Black, 0, 387);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            else
            {
                g.DrawString("Dirección: " + direccion, f2, Brushes.Black, 0, 358);
            }

            g.DrawString("Comuna: " + comuna, f2, Brushes.Black, 0, 416);
            g.DrawString("Teléfono: " + telefono, f2, Brushes.Black, 0, 445);

            if (comentario.Length > 40)
            {
                g.DrawString("Observación:" + comentario.Substring(0, 27), f2, Brushes.Black, 0, 484);
                if (comentario.Substring(28, comentario.Length - 28).Length > 50)
                {
                    try
                    {
                        g.DrawString(comentario.Substring(28, 68), f2, Brushes.Black, 0, 509);
                        g.DrawString(comentario.Substring(69, comentario.Length - 69), f2, Brushes.Black, 0, 542);
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }
                else
                {
                    try
                    {
                        g.DrawString(comentario.Substring(41, comentario.Length - 42), f2, Brushes.Black, 0, 509);
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }
            }
            else
            {
                g.DrawString("Observación:" + comentario, f2, Brushes.Black, 0, 484);
            }

            g.DrawString("DESPACHO PRIORITY", f2, Brushes.Red, 0, 840);
            g.DrawString(paquetesCant, f4, Brushes.Black, 350, 840);
            g.DrawString("CARRIER: ", f2, Brushes.Black, 0, 875);


            return m_Bitmap;
        }

        public ActionResult DownloadInvoice(string fName)
        {
            string FilePath = Server.MapPath("~/Etiquetas/" + fName); //desarrollo
            //string FilePath =  "C:/Users/Shipex/Desktop/eshipex/warehouseshipex/warehouseShipex/Etiquetas/" + fName;
            WebClient User = new WebClient();
            Byte[] FileBuffer = User.DownloadData(FilePath);
            if (FileBuffer != null)
            {
                return File(FileBuffer, "application/pdf", fName);
            }

            return Content("El archivo no fue  encontrado", "text/plain");

        }
    }
}