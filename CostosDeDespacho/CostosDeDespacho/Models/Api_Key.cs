//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CostosDeDespacho.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Api_Key
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Api_Key()
        {
            this.JUMPSELLER_STORES = new HashSet<JUMPSELLER_STORES>();
        }
    
        public int id { get; set; }
        public string token { get; set; }
        public int cliente_id { get; set; }
        public System.DateTime fechaCreacion { get; set; }
        public Nullable<System.DateTime> fechaActualizacion { get; set; }
        public Nullable<int> activo { get; set; }
        public string clave_cliente_ecommerce { get; set; }
        public string clave_secreta_cliente_ecommerce { get; set; }
        public string url_tienda { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JUMPSELLER_STORES> JUMPSELLER_STORES { get; set; }
    }
}
