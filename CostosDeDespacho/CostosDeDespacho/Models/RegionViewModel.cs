﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.Models
{
    public class RegionViewModel
    {
        public int region_id { get; set; }
        public string region_nombre { get; set; }
    }
}