﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.App_Code.BE
{
    public class Rate
    {
        public string rate_id { set; get; }
        public string service_name { set; get; }
        public string service_code { set; get; }
        public string total_price { set; get; }
    }
}