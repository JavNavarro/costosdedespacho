﻿using CostosDeDespacho.App_Code.BE;
using CostosDeDespacho.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace CostosDeDespacho.Controllers
{
    [System.Web.Http.RoutePrefix("api/shipping")]
    public class CalcularCotizacionChileParcelsController : ApiController
    {
        BDconn conexion = new BDconn();
        private string myQueueItem;
        db_shipexEntities context = new db_shipexEntities();

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("rates-chileparcels")]
        public IHttpActionResult GetRates([FromBody] ShopifyShippingRequest request)
        {
            try
            {
                List<DatosEnvio> data = new List<DatosEnvio>();
                DatosEnvio disEnvio = new DatosEnvio();

                // Accede a los datos de destino directamente
                var destination = request.rate.destination;

                // Accede a la lista de productos
                List<Productos> products = new List<Productos>();

                foreach (var item in request.rate.items)
                {
                    Productos product = new Productos
                    {
                        sku = item.sku,
                        cantidad = item.quantity,
                        precio = item.price * item.quantity,
                        price = item.price / 100,
                        product_id = item.product_id == null || item.product_id == 0 ? 0 : item.product_id,
                        variant_id = item.variant_id == null || item.variant_id == 0 ? 0 : item.variant_id
                    };

                    products.Add(product);
                }


                Int32 existe_comuna = conexion.buscarComunaChileParcels(destination.city);

                if (existe_comuna == 0)
                {
                    return Ok(new { rates = new object[] { } }); // No hay tarifas
                }

                disEnvio.cliente = "Farmex";
                disEnvio.comuna = destination.city;
                disEnvio.products = products;

                data.Add(disEnvio);
                var jsonContentStr = JsonConvert.SerializeObject(data);

                App_Code.BL.CostoLogic logica = new App_Code.BL.CostoLogic();
                Costo cost = logica.costoDirectoChileParcels(jsonContentStr);


                if (cost.precio > 0)
                {
                    var response = new ShopifyShippingResponse
                    {
                        rates = new[]
                        {
                            new ShippingRate
                            {
                                service_name = "Shipex - Entrega mismo día",
                                service_code = "FAST",
                                total_price = cost.precio * 100, // En centavos (10.00 USD)
                                currency = "CLP",
                                description = "Los pedidos realizados después de las 11 a.m serán procesados como realizados el día hábil siguiente"
                            }
                        }
                    };
                    return Ok(response);
                }
                else
                {
                    return Ok(new { rates = new object[] { } });
                }
            }
            catch (Exception ex)
            {
                return Ok(new { rates = new object[] { } });
                //return InternalServerError(ex);
            }
        }
    }

}