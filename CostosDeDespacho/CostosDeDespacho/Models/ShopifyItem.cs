﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostosDeDespacho.Models
{
    public class ShopifyItem
    {
        public string sku { get; set; }
        public int quantity { get; set; }
        public int price { get; set; }
        public long product_id { get; set; }
        public long variant_id { get; set; }
    }
}