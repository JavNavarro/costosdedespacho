//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CostosDeDespacho.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class sku_promocion_despacho
    {
        public long id { get; set; }
        public string sku_promocion { get; set; }
        public string Tags_products { get; set; }
        public Nullable<int> id_cliente { get; set; }
    
        public virtual ClientesEmpresa ClientesEmpresa { get; set; }
    }
}
